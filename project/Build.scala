import com.joescii.SbtJasminePlugin._
import com.typesafe.sbt.SbtSite.site
import com.typesafe.sbt.site.SphinxSupport._
import com.typesafe.sbt.packager.linux.LinuxPackageMapping
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport._
import org.clapper.sbt.editsource.EditSourcePlugin
import sbt._
import com.typesafe.sbt.packager.Keys._
import sbt.Keys._
import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.packager.archetypes.ServerLoader.SystemV
import sbtbuildinfo.{BuildInfoKey,BuildInfoPlugin}
import sbtbuildinfo.BuildInfoKeys._
import play.Play.autoImport._


object ApplicationBuild extends Build with com.typesafe.sbt.packager.linux.LinuxMappingDSL {

  val appName = "xuc"
  val appVersion = "2.47.1-rc1"
  val appOrganisation = "xivo"

  lazy val xucstats = ProjectRef(file("../xucstats"),"xucstats")
  lazy val xucami = ProjectRef(file("../xucami"),"xucami")
  lazy val integrationTest = config("integrationTest") extend(Test)


  val main = Project(appName, file("."))
    .enablePlugins(play.PlayScala, BuildInfoPlugin)
    .settings(
      name := appName,
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      resolvers ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      publishArtifact in(Compile, packageDoc) := false,
      publishArtifact in packageDoc := false
    )
    .settings(Jasmine.settings: _*)
    .settings(CustomDebianSettings.settings: _*)
    .settings(CustomDebianSettings.debianMappings: _*)
    .settings(
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/test-reports","-l","xuc.tags.WSApiSpecTest"),
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/test-reports","-l","xuc.tags.IntegrationTest"),
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u", "target/test-reports"),
      javaOptions in Test += "-Dlogger.application=WARN",
      javaOptions in Test += "-Dlogger.play=WARN",
      javaOptions in Test += "-Dconfig.file=test/resources/application.conf",
      testOptions in Test += Tests.Setup(() => { System.setProperty("XUC_VERSION", appVersion) }),
      testOptions in integrationTest := Seq(Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/integrationtest-reports","-n","xuc.tags.IntegrationTest")),
      testOptions in wsApiTest := Seq(Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/test-reports","-n","xuc.tags.WSApiSpecTest"))
    )
    .settings(
      Site.settings: _*
    )
    .settings(
      DockerSettings.settings: _*
    )
    .settings(
      Editsources.settings: _*
    )
    .settings(
      setVersionVarTask := { System.setProperty("XUC_VERSION", appVersion) },
      edit in EditSource <<= (edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource),
      packageBin in Compile <<= (packageBin in Compile) dependsOn (edit in EditSource),
      run in Compile <<= (run in Compile) dependsOn setVersionVarTask
    )
    .settings(
      BuildInfoCustomSettings.settings: _*
    )
    .configs(wsApiTest)
    .settings(inConfig(wsApiTest)(Defaults.testTasks): _*)
    .configs(integrationTest)
    .settings(inConfig(integrationTest)(Defaults.testTasks): _*)
    //.dependsOn(xucami)
    //.aggregate(xucami)

  lazy val wsApiTest = config("wsapi") extend(Test)

  lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")

  object BuildInfoCustomSettings {
      val settings = Seq(
        buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
        buildInfoPackage := "xucserver.info"
      )
  }

  object Site {
    val settings = site.settings ++ site.sphinxSupport() ++ Seq(
      enableOutput in generatePdf in Sphinx := true
    )
  }

  object Jasmine {
    val settings = jasmineSettings ++ Seq(
      appJsDir <+= baseDirectory / "app/assets/javascripts",
      appJsLibDir <+= baseDirectory / "public/javascripts/",
      jasmineTestDir <+= baseDirectory / "test/assets/",
      jasmineConfFile <+= baseDirectory / "test/assets/test.dependencies.js",
      (test in Test) <<= (test in Test) dependsOn (jasmine),
      jasmineEdition := 2
    )
  }

  object DockerSettings {
    val settings = Seq(
      maintainer in Docker := "Jean-Yves LEBLEU <jylebleu@avencall.com>",
      dockerBaseImage := "azul/zulu-openjdk:8u66",
      dockerExposedPorts := Seq(9000),
      dockerExposedVolumes := Seq("/conf"),
      dockerRepository := Some("xivoxc"),
      dockerEntrypoint := Seq("bin/xuc_docker")
    )
  }

  object Editsources {
    val settings = Seq(
      flatten in EditSource := true,
      targetDirectory in EditSource <<= baseDirectory / "conf",
      variables in EditSource <+= version {version => ("SBT_EDIT_APP_VERSION", appVersion)},
      (sources in EditSource) <++= baseDirectory map { bd =>
        (bd / "src/res" * "xuc.version").get
      }
    )
  }

  object CustomDebianSettings {
    val settings = Seq(
      daemonUser in Linux := "xuc",
      serverLoading in Debian := SystemV,
      maintainer in Debian := "Jean-Yves LEBLEU <jylebleu@avencall.com>",
      packageSummary := "Xuc framework",
      packageDescription := """supervision, agent application and samples for Xivo""",
      debianChangelog := Some(file("debian/changelog"))
    )

    val debianMappings = Seq (
      linuxPackageMappings in Debian <+= (baseDirectory) map { bd =>
        (packageMapping((bd / "etc/init.d/xuc_init") -> "/etc/init.d/xuc")
          withUser "root" withGroup "root" withPerms "0755")
      },

      linuxPackageMappings in Debian <+= (baseDirectory) map { bd =>
        (packageMapping((bd / "conf/application.conf") -> "/usr/share/xuc/conf/xuc.conf")
          withUser "xuc" withGroup "xuc" withPerms "0755")
      },

      linuxPackageMappings in Debian <+= (baseDirectory) map { bd =>
        (packageMapping((bd / "conf/logger.xml") -> "/usr/share/xuc/conf/xuc_logger.xml")
          withUser "xuc" withGroup "xuc" withPerms "0755")
      },

      linuxPackageMappings <<= (linuxPackageMappings) map { mappings =>
        for(LinuxPackageMapping(filesAndNames, meta, zipped) <- mappings) yield {
          val newFilesAndNames = for {
            (file, installPath) <- filesAndNames
            if !installPath.contains("/application.conf")
            if !installPath.contains("/logger.xml")
            if !installPath.contains("/messages")
            if !installPath.contains("/messages.fr")
            if !installPath.contains("/play.plugins")
          } yield file -> installPath
          LinuxPackageMapping(newFilesAndNames, meta, zipped)
        }
      }
    )
  }
}
