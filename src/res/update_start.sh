#!/bin/sh

docker pull xivoxc/xuc:latest;
docker stop xuc;
docker rm xuc;
docker run $(cat /etc/docker/xuc/docker-run.conf)
