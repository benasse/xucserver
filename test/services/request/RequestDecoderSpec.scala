package services.request

import java.util.UUID

import models.CallbackStatus
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}
import play.api.libs.json.Json
import services.BrowserMessage
import services.config.ConfigDispatcher._
import services.request.PhoneRequest._

class RequestDecoderSpec extends WordSpec with Matchers with BeforeAndAfterEach {

  var decoder: RequestDecoder = null

  override def beforeEach(): Unit = {
    decoder = new RequestDecoder()
  }

  "A decoder" should {
    "decode not known message to old browser request format" in {
      val browserMsg = play.api.libs.json.Json.parse(s"""{"${XucRequest.ClassProp}":"${XucRequest.WebClass}", "${XucRequest.Cmd}":"olderformat"}""")

      val expectedMsg = BrowserMessage(play.libs.Json.parse(browserMsg.toString()))

      var decoded = decoder.decode(browserMsg)

      decoded should be (expectedMsg)

    }
    "reject invalid message" in {
      val pingMsg = Json.toJson("invalid")

      val decoded = decoder.decode(pingMsg)

      decoded should be (InvalidRequest(XucRequest.errors.invalid, pingMsg.toString()))

    }
    "decode ping message" in {
      val pingMsg = Json.obj("claz"->"ping")

      val decoded = decoder.decode(pingMsg)

      decoded should be (Ping)

    }

    "reject web class message without command" in {
      val invalidWeb = Json.obj("claz" -> XucRequest.WebClass)

      decoder.decode(invalidWeb) shouldBe an [InvalidRequest]
    }

    "decode moveAgentsInGroup command" in {
      val moveAgentInGroup = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"moveAgentsInGroup",
                                        "groupId" -> 27,
                                        "fromQueueId" -> 59,
                                        "fromPenalty" -> 0,
                                        "toQueueId" -> 157,
                                        "toPenalty" ->2
                                      )
      val decoded = decoder.decode(moveAgentInGroup)

      decoded should be (MoveAgentInGroup(27,59,0,157,2))
    }

    "decode addAgentsInGroup command" in {
      val addAgentInGroup = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"addAgentsInGroup",
        "groupId" -> 23,
        "fromQueueId" -> 42,
        "fromPenalty" -> 1,
        "toQueueId" -> 56,
        "toPenalty" ->2
      )

      val decoded = decoder.decode(addAgentInGroup)

      decoded should be (AddAgentInGroup(23,42,1,56,2))

    }
    "decode removeAgentGroupFromQueueGroup command" in {
      val groupId = 131
      val (queueId, penalty) = (235,2)

      val removeAgentGroupFromQueueGroup = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"removeAgentGroupFromQueueGroup",
        "groupId" -> groupId,
        "queueId" -> queueId,
        "penalty" -> penalty
      )

      val decoded = decoder.decode(removeAgentGroupFromQueueGroup)

      decoded should be (RemoveAgentGroupFromQueueGroup(groupId, queueId, penalty))

    }
    "decode addAgentsNotInQueueFromGroupTo command" in {
      val groupId = 123
      val (queueId, penalty) = (342,5)

      val addAgentsNotInQueueFromGroupTo = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"addAgentsNotInQueueFromGroupTo",
        "groupId" -> groupId,
        "queueId" -> queueId,
        "penalty" -> penalty
      )
      val decoded = decoder.decode(addAgentsNotInQueueFromGroupTo)

      decoded should be (AddAgentsNotInQueueFromGroupTo(groupId, queueId, penalty))

    }
    "decode agent listen command" in {
      val agentId = 675

      val agentListen = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"listenAgent",
        "agentid" -> agentId
      )

      val decoded = decoder.decode(agentListen)

      decoded should be (AgentListen(agentId))
    }
    "decode subscribe to agent events" in  {
      val decoder = new RequestDecoder()

      val subscribetoagents = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"subscribeToAgentEvents")

      val decoded = decoder.decode(subscribetoagents)

      decoded should be (SubscribeToAgentEvents)

    }
    "decode get config request" in {
      import services.config.ConfigDispatcher.ObjectType._

      val getConfig = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"getConfig",
        "objectType" -> "queue"
      )

      val decoded = decoder.decode(getConfig)

      decoded should be (GetConfig(TypeQueue))
    }
    "decode get list request" in {
      import services.config.ConfigDispatcher.ObjectType._

      val getList = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"getList",
        "objectType" -> "queuemember"
      )

      val decoded = decoder.decode(getList)

      decoded should be (GetList(TypeQueueMember))
    }
    "decode get agent state request" in {
      val getAgentStates = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"getAgentStates")

      val decoded = decoder.decode(getAgentStates)

      decoded should be (GetAgentStates)

    }
    "decode get agent directory request" in {
      val getAgentDirectory = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"getAgentDirectory")

      val decoded = decoder.decode(getAgentDirectory)

      decoded should be (GetAgentDirectory)

    }

    "decode subscribeToQueueStats request" in {
      val subscribeToQueueStats = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"subscribeToQueueStats")

      val decoded = decoder.decode(subscribeToQueueStats)

      decoded should be (SubscribeToQueueStats)
    }

    "decode monitorPause request" in {
      val monitorPause = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"monitorPause","agentid" -> 10)

      val decoded = decoder.decode(monitorPause)

      decoded should be (MonitorPause(10))
    }

    "decode monitorUnpause request" in {
      val monitorUnpause = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"monitorUnpause","agentid" -> 11)

      val decoded = decoder.decode(monitorUnpause)

      decoded should be (MonitorUnpause(11))
    }

    "decode agent login request" in {
      val agentLoginRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"agentLogin","agentid" -> 55)

      val decoded = decoder.decode(agentLoginRequest)

      decoded should be (AgentLoginRequest(Some(55),None))
    }

    "decode invite conference room request" in {
      val inviteConfRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"inviteConferenceRoom","userId" -> 55)

      val decoded = decoder.decode(inviteConfRequest)

      decoded should be (InviteConferenceRoom(55))
    }

    "decode na forward request" in {
      val naForwardRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"naFwd","state" -> true, "destination"->"1102")

      val decoded = decoder.decode(naForwardRequest)

      decoded should be(NaForward("1102", true))
    }

    "decode unc forward request" in {
      val uncForwardRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"uncFwd","state" -> false, "destination"->"2256")

      val decoded = decoder.decode(uncForwardRequest)

      decoded should be(UncForward("2256", false))
    }

    "decode busy forward request" in {
      val busyForwardRequest = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"busyFwd","state" -> true, "destination"->"2233")

      val decoded = decoder.decode(busyForwardRequest)

      decoded should be(BusyForward("2233", true))
    }

    "decode subscribe to agents stats" in {
      val subscribeToAgentStats = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"subscribeToAgentStats")

      val decoded = decoder.decode(subscribeToAgentStats)

      decoded should be (SubscribeToAgentStats)

    }

    "decode subscribe to queue calls" in {
      val queueId = 5
      val subscribeToQueueCalls = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"subscribeToQueueCalls", "queueId" -> queueId)

      decoder.decode(subscribeToQueueCalls) should be (SubscribeToQueueCalls(queueId))
    }

    "decode unsubscribe to queue calls" in {
      val queueId = 5
      val unSubscribeToQueueCalls = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"unSubscribeToQueueCalls", "queueId" -> queueId)

      decoder.decode(unSubscribeToQueueCalls) should be (UnSubscribeToQueueCalls(queueId))
    }

    "decode get agent call history" in {
      val size = 7
      val getCallHistory = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"getAgentCallHistory", "size" -> size)

      decoder.decode(getCallHistory) should be (GetAgentCallHistory(size))
    }

    "decode get user call history" in {
      val size = 7
      val getCallHistory = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"getUserCallHistory", "size" -> size)

      decoder.decode(getCallHistory) should be (GetUserCallHistory(size))
    }

    "decode set agent group" in {
      val setAgentGroup = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"setAgentGroup", "groupId" -> 7, "agentId" -> 23)

      decoder.decode(setAgentGroup) shouldEqual SetAgentGroup(23, 7)
    }

    "decode directory look up" in {
      val directoryLookUp = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"directoryLookUp", "term" -> "test")

      decoder.decode(directoryLookUp) shouldEqual DirectoryLookUp("test")
    }

    "decode get favorites" in {
      val directoryLookUp = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"getFavorites")

      decoder.decode(directoryLookUp) shouldEqual GetFavorites
    }

    "decode set favorite" in {
      val setFavorite = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"addFavorite", "contactId" -> "123", "source" -> "xivou")

      decoder.decode(setFavorite) shouldEqual AddFavorite("123", "xivou")
    }

    "decode remove favorite" in {
      val removeFavorite = Json.obj("claz"->XucRequest.WebClass,XucRequest.Cmd->"removeFavorite", "contactId" -> "123", "source" -> "xivou")

      decoder.decode(removeFavorite) shouldEqual RemoveFavorite("123", "xivou")
    }

    "decode GetCallbackLists" in {
      val getCallbacks = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "getCallbackLists")

      decoder.decode(getCallbacks) shouldEqual GetCallbackLists
    }

    "decode takeCallback" in {
      val uuid = UUID.randomUUID()
      val takeCallback = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "takeCallback", "uuid" -> uuid.toString)

      decoder.decode(takeCallback) shouldEqual TakeCallback(uuid.toString)
    }

    "decode releaseCallback" in {
      val uuid = UUID.randomUUID()
      val releaseCallback = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "releaseCallback", "uuid" -> uuid.toString)

      decoder.decode(releaseCallback) shouldEqual ReleaseCallback(uuid.toString)
    }

    "decode startCallback" in {
      val uuid = UUID.randomUUID()
      val startCallback = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "startCallback", "uuid" -> uuid.toString, "number" -> "233124")

      decoder.decode(startCallback) shouldEqual StartCallback(uuid.toString, "233124")
    }

    "decode dial with variables" in {
      val dial = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "dial", "destination" -> "1000",
        "variables" -> Json.obj("var1" -> "val1", "var2" -> "val2"))
      decoder.decode(dial) shouldEqual Dial("1000", Map("var1" -> "val1", "var2" -> "val2"))
    }

    "decode dial without variables" in {
      val dial = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "dial", "destination" -> "1000")
      decoder.decode(dial) shouldEqual Dial("1000")
    }

    "decode updateCallbackTicket" in {
      val uuid = UUID.randomUUID()
      val updateCallbackTicket = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "updateCallbackTicket",
        "uuid" -> uuid.toString, "status" -> "Answered", "comment" -> "A small comment")

      decoder.decode(updateCallbackTicket) shouldEqual UpdateCallbackTicket(uuid.toString, Some(CallbackStatus.Answered), Some("A small comment"))
    }

    "decode answer" in {
      val answer = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "answer")
      decoder.decode(answer) shouldEqual Answer
    }

    "decode hangup" in {
      val hangup = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "hangup")
      decoder.decode(hangup) shouldEqual Hangup
    }

    "decode attendedTransfer" in {
      val attendedTransfer = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "attendedTransfer", "destination" -> "1000")
      decoder.decode(attendedTransfer) shouldEqual AttendedTransfer("1000")
    }

    "decode completeTransfer" in {
      val completeTransfer = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "completeTransfer")
      decoder.decode(completeTransfer) shouldEqual CompleteTransfer
    }

    "decode cancelTransfer" in {
      val answer = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "cancelTransfer")
      decoder.decode(answer) shouldEqual CancelTransfer
    }

    "decode conference" in {
      val answer = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "conference")
      decoder.decode(answer) shouldEqual Conference
    }

    "decode hold" in {
      val hold = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "hold")
      decoder.decode(hold) shouldEqual Hold
    }

    "decode setAgentQueue" in {
      val setAgentQueue = Json.obj("claz" -> XucRequest.WebClass, XucRequest.Cmd -> "setAgentQueue","agentId"->33,"queueId"->54,"penalty"->2)
      decoder.decode(setAgentQueue) shouldEqual SetAgentQueue(33,54,2)
    }
  }
}
