package services.directory

import models._
import org.scalatest.{Matchers, WordSpec}
import org.xivo.cti.message.{PhoneConfigUpdate, PhoneStatusUpdate}
import org.xivo.cti.model.PhoneHintStatus
import services.config.ConfigRepository
import xivo.models.{XivoDevice, Line}

import scala.collection.mutable

/**
 * Created by jirka on 03/09/15.
 */
class DirectoryTransformerLogicSpec extends WordSpec with Matchers {

  class Helper() {
    def fixture(phoneNumber: String, lineId: Int = 34) = new {
      val LineId = lineId
      val userId = 52
      val line = Line(LineId,"default","sip", "ojhf",
        Some(XivoDevice("ecbe7520c44c481cbf7bbc196294f27c", Some("10.50.2.104"), "Snom")), None, "ip")
      val phoneStatus = {
        val phoneStatus = new PhoneStatusUpdate
        phoneStatus.setLineId(LineId)
        phoneStatus.setHintStatus(PhoneHintStatus.AVAILABLE.getHintStatus().toString)
        phoneStatus
      }
      val phoneConfig = {
        val phoneConfig = new PhoneConfigUpdate
        phoneConfig.setId(LineId)
        phoneConfig.setNumber(phoneNumber)
        phoneConfig.setUserId(userId)
        phoneConfig
      }
      val xivoDirectoryResult = {
        DirSearchResult(
          List("Name", "Number", "Mobile", "Other number", "Favorite"),
          List("name", "name", "number", "number", "favorite"),
          List(
            ResultItem(
              DefaultDisplayViewItem("peter pan", Some("456"), Some("87"), Some("7"), None, false),
              Relations(None, Some(LineId), Some(userId), Some("xivo-id1"), None),
              "xivo-directory"
            )
          )
        )
      }
    }

    val repo = new ConfigRepository()
    val logic = new DirectoryTransformerLogic(repo)
    val testFixture = fixture("44500")
  }

  "DirectoryTransformerLogic" should {
    "update and transform xivo directory entries" in new Helper() {
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)
      repo.onPhoneStatusUpdate(testFixture.phoneStatus)

      val richDirectoryResult = logic.addStatusesDirResult(testFixture.xivoDirectoryResult)
      val richEntry = richDirectoryResult.getEntries.head

      richEntry.status should be(PhoneHintStatus.AVAILABLE)
      richEntry.favorite should be(Some(false))
      richDirectoryResult.headers shouldNot contain("Favorite")
      richDirectoryResult.headers.size shouldEqual 4
      richDirectoryResult.getEntries().head shouldEqual
        RichEntry(
          PhoneHintStatus.AVAILABLE,
          mutable.Buffer("peter pan", "456", "87", "7", false, ""),
          None, Some("xivo-directory"), Some(false)
        )
    }

    "update xivo directory entries with UNEXISTING when line not found" in new Helper(){
      val richDirectoryResult = logic.addStatusesDirResult(testFixture.xivoDirectoryResult)
      val richEntry = richDirectoryResult.getEntries.head

      richEntry.status should be(PhoneHintStatus.UNEXISTING)
      richDirectoryResult.headers.size shouldEqual 4
    }

    "update xivo directory entries with UNEXISTING when status not found" in new Helper(){
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)

      val richDirectoryResult = logic.addStatusesDirResult(testFixture.xivoDirectoryResult)
      val richEntry = richDirectoryResult.getEntries.head

      richEntry.status should be(PhoneHintStatus.UNEXISTING)
      richDirectoryResult.headers.size shouldEqual 4
    }

    "update and transform xivo favorites entries" in new Helper() {
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)
      repo.onPhoneStatusUpdate(testFixture.phoneStatus)

      val favorites = logic.addStatusesFavorites(testFixture.xivoDirectoryResult)
      val richEntry = favorites.getEntries.head

      richEntry.status should be(PhoneHintStatus.AVAILABLE)
      richEntry.favorite should be(Some(false))
      favorites.headers shouldNot contain("Favorite")
      favorites.headers.size shouldEqual 4
    }

  }
}
