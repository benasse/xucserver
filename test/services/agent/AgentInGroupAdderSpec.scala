package services.agent

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import services.ActorFactory
import services.agent.AgentInGroupAction.AgentsDestination
import services.config.ConfigDispatcher._
import services.request.SetAgentQueue
import xivo.models.Agent


class AgentInGroupAdderSpec extends TestKitSpec("agentingroupadder") {


   class Helper {
     val configDispatcher = TestProbe()

     trait TestActorFactory extends  ActorFactory {
       override val configDispatcherURI = configDispatcher.ref.path.toString
     }

     def actor(groupId: Long, queueId: Long, penalty : Int) = {
       val a = TestActorRef(Props(new AgentInGroupAdder(groupId, queueId, penalty) with TestActorFactory))
       (a,a.underlyingActor)
     }
   }



   "an agent group adder" should {

     "should set agent in new queue upon reception of agents" in new Helper {

       val (groupId, fromQueueId, fromPenalty) = (7,44,8)
       val (toQueueId, toPenalty) = (22,2)
       val (ref,_) = actor(groupId,fromQueueId,fromPenalty)

       val agents = List(Agent(1,"John","Malt","33784","default",groupId))

       ref ! AgentsDestination(toQueueId, toPenalty)

       ref !  AgentList(agents)

       configDispatcher.expectMsgAllOf(RequestConfig(ref,GetAgents(groupId, fromQueueId, fromPenalty)),
                                         ConfigChangeRequest(ref,SetAgentQueue(1,toQueueId,toPenalty))
                                         )

     }
   }
 }
