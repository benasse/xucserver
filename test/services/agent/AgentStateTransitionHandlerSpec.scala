package services.agent

import akka.testkit.TestFSMRef
import akkatest.TestKitSpec
import org.joda.time.DateTime
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import services.AgentStateFSM._
import services.{AgentStateFSM, XucEventBus}
import xivo.events.AgentState
import xivo.events.AgentState.CallDirection._
import xivo.events.AgentState.{AgentOnCall, AgentReady}
import xivo.xucami.models.MonitorState
import xivo.xucami.models.MonitorState.MonitorState

class AgentStateTransitionHandlerSpec extends TestKitSpec("AgentStateTransitionHandlerSpec") with MockitoSugar {

  class LineSubscriberHelper {
    val eventBus = mock[XucEventBus]

    trait TestTransitionHandler  extends AgentStateTHandler {
      this: AgentStateFSM =>
      override lazy val lineEventSubs = new Lsubs()
      override lazy val eventPublisher: EventPublisher = new EventPublisher()
      override lazy val transitionPublisher: TransitionPublisher = new TransitionPublisher()
    }

    class TestAgentFsm(eventBus:XucEventBus, state:MAgentStates) extends AgentStateFSM(eventBus) {
      this: AgentStateTHandler =>
      val dummyAgentState = AgentReady(16, new DateTime, "444444", List())

      when(state) {
        case Event((to:MAgentStates, phoneNumber:String),_) => goto(to) using(AgentStateContext(state=dummyAgentState, phoneNumber = "3000"))
      }
    }

    def testFsm(state:MAgentStates) =  {
      val fsm = TestFSMRef(new TestAgentFsm(eventBus, state) with TestTransitionHandler)
      fsm.setState(state)
      fsm
    }
  }
  "a line event subscriber" should {
    "Unsubscribe from line events on any transition to agentloggout" in new LineSubscriberHelper  {
      val testAsm = testFsm(MAgentReady)

      testAsm ! (MAgentLoggedOut,"")

      verify(eventBus).unsubscribe(testAsm)
    }
    "Subscribe to line events on any transition from logout" in new LineSubscriberHelper {
      val testAsm = testFsm(MAgentLoggedOut)

      testAsm ! (MAgentReady,"3000")

      verify(eventBus).subscribe(testAsm, XucEventBus.lineEventTopic(3000))
    }
    "Subscribe to line events on any transition from initstate" in new LineSubscriberHelper {
      val testAsm = testFsm(MInitState)

      testAsm ! (MAgentPaused,"3000")

      verify(eventBus).subscribe(testAsm, XucEventBus.lineEventTopic(3000))
    }
  }
  class EventPublisherHelper {
    val eventBus = mock[XucEventBus]
    val anyAgentState = AgentReady(16, new DateTime, "444444", List())

    trait TestTransitionHandler  extends AgentStateTHandler {
      this: AgentStateFSM =>
      override lazy val lineEventSubs = new Lsubs()
      override lazy val eventPublisher: EventPublisher = new EventPublisher()
      override lazy val transitionPublisher: TransitionPublisher = new TransitionPublisher()
    }

    class TestAgentFsm(eventBus:XucEventBus, state:MAgentStates) extends AgentStateFSM(eventBus) {
      this: AgentStateTHandler =>

      when(state) {
        case Event(to:MAgentStates,_) => goto(to) using(AgentStateContext(state=anyAgentState, phoneNumber=""))
        case Event((to:MAgentStates, state:AgentState, phoneNumber:String, cause: String),_) => goto(to) using(AgentStateContext(state=state, phoneNumber = phoneNumber, userStatus = cause))
        case Event((to:MAgentStates, state:AgentState, phoneNumber:String, cause: String, mState:MonitorState),_) => goto(to) using(AgentStateContext(state=state, phoneNumber = phoneNumber, userStatus = cause, monitorState = mState))
      }
    }

    def testFsm(state:MAgentStates) =  {
      val fsm = TestFSMRef(new TestAgentFsm(eventBus, state) with TestTransitionHandler)
      fsm.setState(state)
      fsm
    }
  }
  "An event publisher" should {
    "not publish from dialing to dialing on pause" in new EventPublisherHelper {
      val testAsm = testFsm(MAgentDialing)

      testAsm ! (MAgentDialingOnPause)

      verify(eventBus, never()).publish(any[AgentState])
    }
    "not publish from dialing on pause to dialing" in new EventPublisherHelper {
      val testAsm = testFsm(MAgentDialingOnPause)

      testAsm ! (MAgentDialing)

      verify(eventBus, never()).publish(any[AgentState])
    }
    "not publish from ringing to ringing on pause"  in new EventPublisherHelper {
      val testAsm = testFsm(MAgentRinging)

      testAsm ! (MAgentRingingOnPause)

      verify(eventBus, never()).publish(any[AgentState])

    }
    "not publish from ringing on pause to ringing"  in new EventPublisherHelper {
      val testAsm = testFsm(MAgentRingingOnPause)

      testAsm ! (MAgentRinging)

      verify(eventBus, never()).publish(any[AgentState])

    }
    "not publish from call to call on pause"  in new EventPublisherHelper {
      val testAsm = testFsm(MAgentOnCall)

      testAsm ! (MAgentOnCallOnPause)

      verify(eventBus, never()).publish(any[AgentState])

    }
    "not publish from call on pause to call"  in new EventPublisherHelper {
      val testAsm = testFsm(MAgentOnCallOnPause)

      testAsm ! (MAgentOnCall)

      verify(eventBus, never()).publish(any[AgentState])

    }
    "publish agent state with cause and phone number from context" in new EventPublisherHelper {
      val testAsm = testFsm(MAgentPaused)
      val readyEvent = AgentReady(16, new DateTime, "444444", List())

      testAsm ! (MAgentDialing, readyEvent, "3000", "out")

      verify(eventBus).publish(readyEvent withPhoneNb("3000") withCause("out"))

    }
    "on call publish agent state with cause, phone number, and monitor state from context" in new EventPublisherHelper {
      val testAsm = testFsm(MAgentReady)
      val oncallEvent = AgentOnCall(16, new DateTime,  acd=false, Incoming, "444444", List(), onPause=false)

      testAsm ! (MAgentOnCall, oncallEvent, "5000", "play", MonitorState.ACTIVE)

      verify(eventBus).publish(oncallEvent withPhoneNb("5000") withCause("play") withMonitorState(MonitorState.ACTIVE))

    }
  }

  class TransitionPublisherHelper {
    val eventBus = mock[XucEventBus]
    val anyAgentState = AgentReady(16, new DateTime, "444444", List())

    trait TestTransitionHandler  extends AgentStateTHandler {
      this: AgentStateFSM =>
      override lazy val lineEventSubs = new Lsubs()
      override lazy val eventPublisher: EventPublisher = new EventPublisher()
      override lazy val transitionPublisher: TransitionPublisher = new TransitionPublisher()
    }

    class TestAgentFsm(eventBus:XucEventBus, state:MAgentStates) extends AgentStateFSM(eventBus) {
      this: AgentStateTHandler =>

      when(state) {
        case Event((to:MAgentStates, state:AgentState, phoneNumber:String, cause: String),_) => goto(to) using(AgentStateContext(state=state, phoneNumber = phoneNumber, userStatus = cause))
      }
    }

    def testFsm(state:MAgentStates) =  {
      val fsm = TestFSMRef(new TestAgentFsm(eventBus, state) with TestTransitionHandler)
      fsm.setState(state)
      fsm
    }
  }
  "A transition publisher" should {
    "publish agent state with cause and phone number from context" in new EventPublisherHelper {
      val testAsm = testFsm(MAgentPaused)
      val readyEvent = AgentReady(16, new DateTime, "444444", List())

      testAsm ! readyEvent

      verify(eventBus).publish(
        AgentTransition(
          MAgentPaused,
          MEmptyContext,
          MAgentReady,
          AgentStateContext(readyEvent, "", MonitorState.UNKNOWN, "", "444444")
        ), 16
      )

    }
  }
}
