package services.agent

import akka.actor.{ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import services.ActorFactory
import services.agent.AgentInGroupAction.{RemoveAgents, AgentsDestination}
import services.request._

class AgentConfigSpec extends TestKitSpec("AgentConfig") with MockitoSugar {

  class Helper {

    val moveAgentInGroups = TestProbe()
    val addAgentInGroups = TestProbe()
    val removeAgentGroupFromQueueGroup = TestProbe()
    val addAgentsNotInQueueFromGroupTo = TestProbe()
    val agentGroupSetter = TestProbe()

    val mockMove = mock[(Long,Long,Int)=> ActorRef]
    val mockEmpty = mock[() => ActorRef]

    trait TestActorFactory extends ActorFactory {
      override def getMoveAgentInGroup(groupId : Long, queueId: Long, penalty : Int) = mockMove(groupId, queueId, penalty)
      override def getAddAgentInGroup(groupId : Long, queueId: Long, penalty : Int) = mockMove(groupId, queueId, penalty)
      override def getRemoveAgentGroupFromQueueGroup(groupId: Long, queueId: Long, penalty: Int) = mockMove(groupId, queueId, penalty)
      override def getAddAgentsNotInQueueFromGroupTo(groupId: Long, queueId: Long, penalty: Int) = mockMove(groupId, queueId, penalty)
      override def getAgentGroupSetter() = mockEmpty()
    }

    def actor = {
      val a = TestActorRef[AgentConfig](Props(new AgentConfig with TestActorFactory))
      (a, a.underlyingActor)
    }
  }

  "Agent config" should {
    "delegate move agent from a group in queue/penaly to a queue/penalty to an actor" in new Helper {
      val (ref, agentConfig) = actor

      val moveRequest = MoveAgentInGroup(groupId = 3,fromQueueId = 45, fromPenalty = 2, toQueueId = 89, toPenalty = 1)

      stub(agentConfig.getMoveAgentInGroup(3,45,2)).toReturn(moveAgentInGroups.ref)

      ref ! BaseRequest(TestProbe().ref,moveRequest)

      moveAgentInGroups.expectMsg(AgentsDestination(89,1))

    }
    "delegate add agent from a group in queue/penalty to a queue/penalty to an actor" in new Helper {
      val (groupId, fromQueueId, fromPenalty) = (7,44,8)
      val (toQueueId, toPenalty) = (22,2)

      val (ref, agentConfig) = actor

      val addRequest = AddAgentInGroup(groupId,fromQueueId,fromPenalty, toQueueId, toPenalty)

      stub(agentConfig.getAddAgentInGroup(groupId,fromQueueId,fromPenalty)).toReturn(addAgentInGroups.ref)

      ref ! BaseRequest(TestProbe().ref,addRequest)

      addAgentInGroups.expectMsg(AgentsDestination(toQueueId,toPenalty))
    }
    "delegate remove agent group from a queue penalty to an actor" in new Helper {
      val (groupId, queueId, penalty) = (7,44,8)

      val (ref, agentConfig) = actor

      val removeRequest = RemoveAgentGroupFromQueueGroup(groupId, queueId, penalty)

      stub(agentConfig.getRemoveAgentGroupFromQueueGroup(groupId,queueId,penalty)).toReturn(removeAgentGroupFromQueueGroup.ref)

      ref ! BaseRequest(TestProbe().ref,removeRequest)

      removeAgentGroupFromQueueGroup.expectMsg(RemoveAgents)

    }
    "delegate add agents from a group to a queue penalty to an actor" in new Helper {
      val (groupId, toQueueId, toPenalty) = (904,211,2)

      val (ref, agentConfig) = actor

      val addRequest = AddAgentsNotInQueueFromGroupTo(groupId, toQueueId, toPenalty)

      stub(agentConfig.getAddAgentsNotInQueueFromGroupTo(groupId,toQueueId,toPenalty)).toReturn(addAgentsNotInQueueFromGroupTo.ref)

      ref ! BaseRequest(TestProbe().ref,addRequest)

      addAgentsNotInQueueFromGroupTo.expectMsg(AgentsDestination(toQueueId,toPenalty))
    }

    "delegate set agent group to an actor" in new Helper {
      val (groupId, agentId) = (12, 25)
      val (ref, agentConfig) = actor
      stub(agentConfig.getAgentGroupSetter()).toReturn(agentGroupSetter.ref)

      ref ! BaseRequest(TestProbe().ref, SetAgentGroup(groupId, agentId))

      agentGroupSetter.expectMsg(SetAgentGroup(groupId, agentId))
    }
  }

}
