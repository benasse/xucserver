package services.config

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.{XivoUser, XivoUserDao}
import org.joda.time.DateTime
import org.mockito.Mockito.{stub, verify, _}
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message._
import services.XucEventBus.XucEvent
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import services.config.ConfigDispatcher.ObjectType
import services.config.ConfigManager.InitConfig
import services.{XucEventBus, XucStatsEventBus}
import xivo.events.AgentState.{AgentLoggedOut, AgentLogin}
import xivo.models._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class ConfigInitializerSpec extends TestKitSpec("ConfigInitializerSpec") with MockitoSugar with ScalaFutures {

  class Helper {
    val configRepository = mock[ConfigRepository]
    val lineManager = TestProbe()
    val eventBus = mock[XucEventBus]
    val statsEventBus = mock[XucStatsEventBus]
    val factory = new MessageFactory
    val myFactory = mock[MessageFactory]
    val agentQueueMemberFactory  = mock[AgentQueueMemberFactory]
    val xivoUserDao = mock[XivoUserDao]
    val agentLoginStatusDao = mock[AgentLoginStatusDao]
    val parent = TestProbe()
    val agentManager = TestProbe()

    trait XivoUserDaoTest extends XivoUserDao {
      def getUsers() = xivoUserDao.getUsers()
      def getUser(userId: Long) = xivoUserDao.getUser(userId)
    }

    trait AgentLoginStatusDaoTest extends AgentLoginStatusDao {
      override def getLoginStatus(id:Agent.Id):Option[AgentLoginStatus] = agentLoginStatusDao.getLoginStatus(id)
    }

    trait MUpdateTest extends MetricUpdate {
      def updateOrCreateMetric(queueId:Int, statName: String, statValue: Double) = mock[(Int,String,Double) => Unit]
    }
    def actor = {
      val a = TestActorRef[ConfigDispatcher with ConfigInitializer](Props(
        new ConfigDispatcher(configRepository, lineManager.ref, agentManager.ref, eventBus, statsEventBus, agentQueueMemberFactory)
          with ConfigInitializer with XivoUserDaoTest with AgentLoginStatusDaoTest with MUpdateTest ), parent.ref, "ConfigInitializer")
      a.underlyingActor.requests = List()
      (a, a.underlyingActor)
    }
  }
  "Config initializer" should {
    "send request to link on init config" in new Helper {
      val (ref, cInit) = actor
      val ctiLink = TestProbe()
      stub(xivoUserDao.getUsers()).toReturn(Future(List()))
      val requests = List(factory.createGetAgents(), factory.createGetPhonesList())
      cInit.requestsToSend = requests

      ref ! InitConfig(ctiLink.ref)

      requests.foreach(r => ctiLink.expectMsg(r))
    }
    "load queue members on init config" in new Helper {
      val (ref, cInit) = actor
      val ctiLink = TestProbe()
      stub(xivoUserDao.getUsers()).toReturn(Future(List()))

      ref ! InitConfig(ctiLink.ref)

      verify(configRepository).loadAgentQueueMembers()

    }

    "load users on InitConfig" in new Helper {
      val (ref, cInit) = actor
      val ctiLink = TestProbe()

      val userList = Future(List(XivoUser(1,"Jack",Some("Band"),Some("user1"),Some("0000"))))
      stub(xivoUserDao.getUsers()).toReturn(userList)

      ref ! InitConfig(ctiLink.ref)

      whenReady(userList) { userList =>
        verify(xivoUserDao).getUsers()
        verify(configRepository).updateUser(XivoUser(1,"Jack",Some("Band"),Some("user1"),Some("0000")))
      }

    }
    "send subsequent requests to link" in new Helper {
      val (ref, cInit) = actor
      val ctiLink = TestProbe()
      stub(xivoUserDao.getUsers()).toReturn(Future(List()))
      val requests = List(factory.createGetAgents())
      cInit.link = ctiLink.ref
      cInit.requestsToSend = requests

      ref ! InitConfig(ctiLink.ref)

      val subseqrequests = List(factory.createGetAgentConfig("12"))
      cInit.requests = subseqrequests

      (requests ++ subseqrequests).foreach(r => ctiLink.expectMsg(r))
    }


    "request user configuration on user ids" in new Helper {
      val (ref, cInit) = actor
      cInit.requests = List()
      val userIds = new UserIdsList
      val userConfigRequest = factory.createGetUserConfig("57")
      stub(configRepository.onUserIds(userIds)).toReturn(List(userConfigRequest))

      ref ! userIds

      verify(configRepository).onUserIds(userIds)
      cInit.requests.head shouldBe (userConfigRequest)
    }
    "request phones configuration on phone ids" in new Helper {
      val (ref, cInit) = actor
      cInit.requests = List()
      val phoneIds = new PhoneIdsList
      val phoneConfigRequest = factory.createGetPhoneConfig("4")
      stub(configRepository.onPhoneIds(phoneIds)).toReturn(List(phoneConfigRequest))

      ref ! phoneIds

      verify(configRepository).onPhoneIds(phoneIds)
      cInit.requests.head shouldBe (phoneConfigRequest)
    }

    "request queues configuration on queue ids" in new Helper {
      val (ref, cInit) = actor
      cInit.requests = List()
      val queueIds = new QueueIds
      val queueConfigRequest = factory.createGetQueueConfig("72")
      stub(configRepository.onQueueIds(queueIds)).toReturn(List(queueConfigRequest))

      ref ! queueIds

      verify(configRepository).onQueueIds(queueIds)
      cInit.requests.head shouldBe (queueConfigRequest)
    }
    "request queue member configuration on queue member ids" in new Helper {
      val (ref, cInit) = actor
      val queueMemberIds = new QueueMemberIds
      val queueMemberConfigRequest = factory.createGetQueueMemberConfig("65")
      stub(configRepository.onQueueMemberIds(queueMemberIds)).toReturn(List(queueMemberConfigRequest))

      ref ! queueMemberIds

      verify(configRepository).onQueueMemberIds(queueMemberIds)
      cInit.requests.head shouldBe (queueMemberConfigRequest)

    }
    "not crash on receiving other id list" in new Helper {
      val (ref, cInit) = actor
      cInit.requests = List()
      val agentIds= new AgentIds
      cInit.receive(agentIds)
    }
    "update config repository on queue configuration received" in new Helper {
      val (ref, _) = actor
      val queueConfig = new QueueConfigUpdate

      ref ! queueConfig

      verify(configRepository).updateQueueConfig(queueConfig)
    }
    """on user config message received
        get user username
        update config repository with username
        update config repository with line
        update config repository
        request user status""" in new Helper {
      val (ref, configInitializer) = actor
      val link = TestProbe()
      configInitializer.link = link.ref
      configInitializer.messageFactory = myFactory

      val userConfigUpdate = new UserConfigUpdate
      userConfigUpdate.setUserId(232)
      userConfigUpdate.addLineId(35)
      val userStatusRequest = factory.createGetUserStatus("232")
      stub(myFactory.createGetUserStatus("232")).toReturn(userStatusRequest)
      val fuser= Future(Some(XivoUser(232,"Alf",Some("Foul"),Some("afoul"),Some("0000"))))
      stub(xivoUserDao.getUser(232)).toReturn(fuser)

      ref ! userConfigUpdate

      whenReady(fuser, Timeout(20 seconds)) { fuser =>
        verify(configRepository).updateUser(XivoUser(232,"Alf",Some("Foul"),Some("afoul"),Some("0000")))
      }
      verify(xivoUserDao).getUser(232)
      verify(configRepository).onUserConfigUpdate(userConfigUpdate)
      verify(myFactory).createGetUserStatus("232")
      verify(configRepository).loadUserLine(232,35)

      configInitializer.requests should contain(userStatusRequest)

    }

    """ on user config update
        load agent configuration if agent id is not 0
        and send self message with new agent loaded
        """ in new Helper {
      val (ref, configInit) = actor
      stub(xivoUserDao.getUser(232)).toReturn(Future(None))

      val agentId = 45
      val agent = Agent(agentId, "Marc", "Aurèle", "3345","default")
      val userConfigUpdate = new UserConfigUpdate
      userConfigUpdate.setUserId(232)
      userConfigUpdate.setAgentId(45)

      stub(configRepository.getAgent(agentId)).toReturn(Some(agent))
      ref ! userConfigUpdate

      verify(configRepository).loadAgent(agentId)
      verify(configRepository).getAgent(agentId)


    }

    """ on agent received
        - get login status from db and send to agent manager if exists
        - request status
        - request queue member config update
        - publish agent on bus""" in new Helper {
      val (ref, cInit) = actor
      cInit.messageFactory = myFactory
      val ctiLink = TestProbe()
      cInit.link = ctiLink.ref
      val agentId = 45
      val agentNumber = "56778"
      val agent = Agent(agentId,"Bob","Dash", agentNumber,"default")

      val statusRequest = factory.createGetAgentStatus(agentId.toString)
      val qmemberConfigRequest = factory.createGetQueueMemberConfig(s"Agent/$agentNumber,queue")

      stub(myFactory.createGetAgentStatus(agentId.toString)).toReturn(statusRequest)
      stub(configRepository.getAgentQueueMemberRequest(agentNumber)).toReturn(List(qmemberConfigRequest))

      val agentLoginStatus = new AgentLoginStatus(52, "1200", new DateTime())
      stub(agentLoginStatusDao.getLoginStatus(agentId)).toReturn(Some(agentLoginStatus))

      ref ! agent

      verify(myFactory).createGetAgentStatus(agentId.toString)
      ctiLink.expectMsgAllOf(statusRequest,qmemberConfigRequest)

      agentManager.expectMsg(AgentLogin(agentLoginStatus))
      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeAgent), agent))

    }

    """ on agent received
        - if no login status from db send agent logout to agent manager
        - request queue member
        - request queue member config update
        - publish agent on bus""" in new Helper {

      val (ref, cInit) = actor
      cInit.messageFactory = myFactory
      val ctiLink = TestProbe()
      cInit.link = ctiLink.ref
      val agentId = 45
      val agentNumber = "56778"
      val agent = Agent(agentId,"Bob","Dash", agentNumber,"default")

      val statusRequest = factory.createGetAgentStatus(agentId.toString)
      val qmemberConfigRequest = factory.createGetQueueMemberConfig(s"Agent/$agentNumber,queue")

      stub(myFactory.createGetAgentStatus(agentId.toString)).toReturn(statusRequest)
      stub(configRepository.getAgentQueueMemberRequest(agentNumber)).toReturn(List(qmemberConfigRequest))

      stub(agentLoginStatusDao.getLoginStatus(agentId)).toReturn(None)

      ref ! agent

      verify(myFactory).createGetAgentStatus(agentId.toString)
      ctiLink.expectMsgAllOf(statusRequest,qmemberConfigRequest)

      agentManager.expectMsg(AgentLoggedOut(agentId, new DateTime(),"", List()))
      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeAgent), agent))

    }

    """on queue member config update received
       - get from database
       - send the result to self
        if queue member changed (not exists in repo)
        update config repository
        publish updated queue member""" in new Helper {
      val (ref, cInit) = actor

      val queueMember = new QueueMemberConfigUpdate
      queueMember.setAgentNumber("2345")
      queueMember.setQueueName("blue")

      val agqmember = AgentQueueMember(agentId = 93, queueId = 54, penalty = 2)

      stub(agentQueueMemberFactory.getQueueMember("2345", "blue")).toReturn(Some(agqmember))
      stub(configRepository.queueMemberExists(agqmember)).toReturn(false)


      ref ! queueMember


      verify(agentQueueMemberFactory).getQueueMember("2345", "blue")
      verify(configRepository).updateOrAddQueueMembers(agqmember)
      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeQueueMember), agqmember))
    }
    """on queue member added received
       - get from database
       - send the result to self
        if queue member changed (not exists in repo)
        update config repository
        publish updated queue member""" in new Helper {
      val (ref, cInit) = actor

      val queueMember = new QueueMemberAdded
      queueMember.setAgentNumber("2345")
      queueMember.setQueueName("blue")

      val agqmember = AgentQueueMember(agentId = 93, queueId = 54, penalty = 2)

      stub(agentQueueMemberFactory.getQueueMember("2345", "blue")).toReturn(Some(agqmember))
      stub(configRepository.queueMemberExists(agqmember)).toReturn(false)


      ref ! queueMember


      verify(agentQueueMemberFactory).getQueueMember("2345", "blue")
      verify(configRepository).updateOrAddQueueMembers(agqmember)
      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeQueueMember), agqmember))
    }
    """ Do not publish queue member if already received
        with same penalty
    """ in new Helper {
      val (ref, cInit) = actor
      val queueMember = new QueueMemberConfigUpdate
      queueMember.setAgentNumber("2345")
      queueMember.setQueueName("blue")
      queueMember.setPenalty(0)

      val agqmember = AgentQueueMember(agentId = 93, queueId = 54, penalty = 2)

      stub(agentQueueMemberFactory.getQueueMember("2345", "blue")).toReturn(Some(agqmember))
      stub(configRepository.queueMemberExists(agqmember)).toReturn(true)

      ref ! queueMember

      verify(agentQueueMemberFactory).getQueueMember("2345", "blue")
      verify(eventBus,never()).publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeQueueMember), AgentQueueMember(93, 54, 5)))
      verify(configRepository).queueMemberExists(agqmember)

    }
    """ on remove queue member received
        update config repository
        publish transformed queue member with id to event bus
        """ in new Helper {
      val (ref, cInit) = actor
      val remQueueMember = new QueueMemberRemoved
      val removedagQMember = AgentQueueMember(93, 54, -1)
      stub(configRepository.getAgentQueueMember(remQueueMember)).toReturn(Some(removedagQMember))

      ref ! remQueueMember

      verify(configRepository).removeQueueMember(remQueueMember)
      verify(eventBus).publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeQueueMember),removedagQMember))

    }
  }
}