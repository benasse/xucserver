package services.config

import java.util.Date

import models._
import org.joda.time.DateTime
import org.json.JSONObject
import org.mockito.Mockito.{stub, verify}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{AgentConfigUpdate, AgentIds, DirectoryResult, PhoneConfigUpdate, PhoneIdsList, PhoneStatusUpdate, QueueConfigUpdate, QueueIds, QueueMemberConfigUpdate, QueueMemberIds, QueueMemberRemoved, UserConfigUpdate, UserIdsList, UserStatusUpdate}
import org.xivo.cti.model.{DirectoryEntry, Meetme, MeetmeMember, PhoneHintStatus}
import services.agent.{AgentStatistic, StatPeriod, Statistic}
import services.config.ConfigDispatcher.{LineConfigQueryById, LineConfigQueryByNb}
import xivo.events.AgentState.CallDirection._
import xivo.events.AgentState.{AgentLoggedOut, AgentLogin, AgentOnCall, AgentReady}
import xivo.models._
import xivo.xucami.models.QueueCall

import scala.collection.JavaConversions._

class ConfigRepositorySpec extends WordSpec with Matchers with MockitoSugar with BeforeAndAfterEach{

  var configRepository: ConfigRepository = _
  var factory: MessageFactory = _
  var lineFactory: LineFactory = _
  var agentFactory: AgentFactory = _
  var agentQueueMemberFactory: AgentQueueMemberFactory = _

  override def beforeEach = {
    factory = mock[MessageFactory]
    lineFactory = mock[LineFactory]
    agentFactory = mock[AgentFactory]
    agentQueueMemberFactory = mock[AgentQueueMemberFactory]
    configRepository = new ConfigRepository(factory, lineFactory, agentFactory, agentQueueMemberFactory)
  }

  def fixture(phoneNumber: String, lineId: Int = 34) = new {
    val LineId = lineId
    val userId = 52
    val line = Line(LineId,"default","sip", "ojhf",  Some(XivoDevice("ecbe7520c44c481cbf7bbc196294f27c", Some("10.50.2.104"), "Snom")), None, "ip")
    val PhoneStatus = PhoneHintStatus.AVAILABLE.getHintStatus().toString
    val phoneStatus = {
      val phoneStatus = new PhoneStatusUpdate
      phoneStatus.setLineId(LineId)
      phoneStatus.setHintStatus(PhoneStatus)
      phoneStatus
    }
    val phoneConfig = {
      val phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(LineId)
      phoneConfig.setNumber(phoneNumber)
      phoneConfig.setUserId(userId)
      phoneConfig
    }
    val directoryResult = {
      val directoryResult = new DirectoryResult
      var entry = new DirectoryEntry
      entry.addField("John")
      entry.addField("Heart")
      entry.addField(phoneNumber)
      entry.addField("0678906543")
      directoryResult.addEntry(entry)
      directoryResult.addHeader("FirstName")
      directoryResult.addHeader("LastName")
      directoryResult.addHeader("Phone")
      directoryResult.addHeader("Mobile")
      directoryResult
    }
    val emptyPhoneStatus = {
      val phoneStatus = new PhoneStatusUpdate
      phoneStatus.setLineId(LineId)
      phoneStatus.setHintStatus("")
      phoneStatus
    }
  }
  "A line repository" should {
    "update user line entry" in {
      val testFixture = fixture("44500")

      stub(lineFactory.get(987)).toReturn(Some(Line(987,"default","sip","kjjkh",None, None, "ip")))

      configRepository.loadUserLine(56,987)

      verify(lineFactory).get(987)
      configRepository.getLineForUser(56) should be(Some(Line(987,"default","sip","kjjkh",None, None, "ip")))

    }
  }

  "A config repository" should {
    "update internal line config repo" in {
        val testFixture = fixture("44500")
        configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
        configRepository.linePhoneNbs.filter(_._2 == "44500").keys.toList should be(List(testFixture.phoneConfig.getId))

    }
    "replace line configuration" in {
      val testFixture = fixture("44500")
      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
      testFixture.phoneConfig.setId(58)
      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
      configRepository.linePhoneNbs.filter(_._2 == "44500").keys.toList should be(List(testFixture.phoneConfig.getId))

    }
    "update directory entries" in {
      val testFixture = fixture("44500")

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
      configRepository.onPhoneStatusUpdate(testFixture.phoneStatus)

      val richDirectoryResult = configRepository.addStatuses(testFixture.directoryResult)
      val richEntry = richDirectoryResult.getEntries.head
      richEntry.status should be(PhoneHintStatus.AVAILABLE)

      richDirectoryResult.headers.size should be(4)
    }

    "update phone status with username" in {

      configRepository.updateUser(XivoUser(32,"jack",Some("Bal"),Some("jack"),Some("pwd")))
      var phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(45)
      phoneConfig.setNumber("1000")
      phoneConfig.setUserId(32)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      var phoneStatus = new PhoneStatusUpdate
      phoneStatus.setLineId(45)
      phoneStatus.setHintStatus("32")
      configRepository.onPhoneStatusUpdate(phoneStatus)

      val userPhoneStatus = configRepository.getUserPhoneStatus(phoneStatus).get

      userPhoneStatus.username should be("jack")
    }
    "not get any user phone status if hint is empty" in {
      configRepository.updateUser(XivoUser(32,"jack",Some("Bal"),Some("jack"),Some("pwd")))
      var phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(43)
      phoneConfig.setNumber("1000")
      phoneConfig.setUserId(32)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      var phoneStatus = new PhoneStatusUpdate
      phoneStatus.setLineId(43)
      phoneStatus.setHintStatus("")
      configRepository.onPhoneStatusUpdate(phoneStatus)

      configRepository.getUserPhoneStatus(phoneStatus) should be(None)
    }

    "get all user phone statuses" in {
      configRepository.updateUser(XivoUser(32,"jack",Some("Bal"),Some("jack"),Some("pwd")))
      var phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(45)
      phoneConfig.setNumber("1000")
      phoneConfig.setUserId(32)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      var phoneStatus = new PhoneStatusUpdate
      phoneStatus.setLineId(45)
      phoneStatus.setHintStatus("8")
      configRepository.onPhoneStatusUpdate(phoneStatus)

      val uph = UserPhoneStatus("jack", PhoneHintStatus.getHintStatus(Integer.valueOf(phoneStatus.getHintStatus())))

      val uphs = configRepository.getAllUserPhoneStatuses

      uphs should contain(uph)
    }

    "not update status entry when phone status is empty" in {
      val testFixture = fixture("44500")

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)

      configRepository.onPhoneStatusUpdate(testFixture.phoneStatus)
      configRepository.onPhoneStatusUpdate(testFixture.emptyPhoneStatus)

      val richDirectoryResult = configRepository.addStatuses(testFixture.directoryResult)
      var richEntry = richDirectoryResult.getEntries.head
      richEntry.status should be(PhoneHintStatus.AVAILABLE)

    }

    "requests phoneConfig and phoneStatus for each phoneId upon reception of phoneList" in {

      val expectedRequest = new JSONObject()
      stub(factory.createGetPhoneConfig("1")).toReturn(expectedRequest)
      stub(factory.createGetPhoneConfig("3")).toReturn(expectedRequest)
      stub(factory.createGetPhoneStatus("1")).toReturn(expectedRequest)
      stub(factory.createGetPhoneStatus("3")).toReturn(expectedRequest)

      val phoneIdsList = new PhoneIdsList()
      phoneIdsList.add(1)
      phoneIdsList.add(3)

      val result = configRepository.onPhoneIds(phoneIdsList)

      result.size should be(4)

      result.foreach(request => {
        request should be(expectedRequest)
      })
      verify(factory).createGetPhoneConfig("1")
      verify(factory).createGetPhoneConfig("3")
      verify(factory).createGetPhoneStatus("1")
      verify(factory).createGetPhoneStatus("3")
    }

    "requests user configuration for each user id received" in {

      val expectedRequest = new JSONObject()
      stub(factory.createGetUserConfig("11")).toReturn(expectedRequest)
      stub(factory.createGetUserConfig("31")).toReturn(expectedRequest)

      val userIds = new UserIdsList
      userIds.add(11)
      userIds.add(31)

      val result = configRepository.onUserIds(userIds)

      result.size should be(2)
      verify(factory).createGetUserConfig("11")
      verify(factory).createGetUserConfig("31")
    }

    "get a user from id" in {
      val user = XivoUser(45,"Bob",Some("Health"),Some("bhealth"),Some("pwd"))
      configRepository.updateUser(user)

      configRepository.getUser(45) should be(Some(user))
    }
    "get a user from username" in {
      val user = XivoUser(45,"Bob",Some("Health"),Some("bhealth"),Some("pwd"))
      configRepository.updateUser(user)

      configRepository.getUser("bhealth") should be(Some(user))
    }
    "get a user from username and password" in {
      val user = XivoUser(45,"Bob",Some("Health"),Some("bhealth"),Some("pwd"))
      configRepository.updateUser( XivoUser(45,"Bob",Some("Health"),Some("bhealth"),Some("pwd")))
      configRepository.updateUser( XivoUser(46,"John",Some("Als"),Some("jals"),Some("pwdals")))

      configRepository.getUser("bhealth","pwd") should be(Some(user))
      configRepository.getUser("bhealth","xcvds") should be(None)

    }

    "requests queue configuration for each queue id received" in {

      val expectedRequest = new JSONObject()
      stub(factory.createGetQueueConfig("11")).toReturn(expectedRequest)
      stub(factory.createGetQueueConfig("31")).toReturn(expectedRequest)

      val queueIds = new QueueIds
      queueIds.add(11)
      queueIds.add(31)

      val result = configRepository.onQueueIds(queueIds)

      result.size should be(2)
      verify(factory).createGetQueueConfig("11")
      verify(factory).createGetQueueConfig("31")
    }

    "requests agent status for each agent id received" in {

      val expectedRequest = new JSONObject()
      stub(factory.createGetAgentStatus("27")).toReturn(expectedRequest)
      stub(factory.createGetAgentStatus("41")).toReturn(expectedRequest)

      val agentIds = new AgentIds
      agentIds.add(27)
      agentIds.add(41)

      val result = configRepository.requestStatusOnAgentIds(agentIds)

      result.size should be(2)
      verify(factory).createGetAgentStatus("27")
      verify(factory).createGetAgentStatus("41")
    }

    "request agent from db on id" in {
      val agentId = 45
      val albert = Agent(agentId, "Albert", "Jul", "4567","default")

      stub(agentFactory.getById(agentId)).toReturn(Some(albert))

      configRepository.loadAgent(agentId)

      configRepository.getAgent(agentId) should be(Some(albert))
    }

     "creates agent queue member config update from agent number" in {
      val queueMember = new QueueMemberConfigUpdate
      queueMember.setId("Agent/7654,queueone")
      queueMember.setAgentNumber("7654")
      queueMember.setQueueName("queueone")
      putAgentInQueue(agentId = 25, queueId = 32, penalty= 5)
      addAgent(Agent(25, "Alain", "Ficelle","7654","default",32))
      addQueue(32,"queueone")

      val expectedRequest = new JSONObject()

      stub(factory.createGetQueueMemberConfig("Agent/7654,queueone")).toReturn(expectedRequest)

      configRepository.getAgentQueueMemberRequest("7654") should be(List(expectedRequest))

      verify(factory).createGetQueueMemberConfig("Agent/7654,queueone")

    }
    "request queue member configuration on each queue member id received" in {
      val expectedRequest = new JSONObject()
      stub(factory.createGetQueueMemberConfig("Agent/3456,queueone")).toReturn(expectedRequest)
      stub(factory.createGetQueueMemberConfig("Agent/3400,queuetwo")).toReturn(expectedRequest)

      val qmIds = new QueueMemberIds
      qmIds.add("Agent/3456,queueone")
      qmIds.add("Agent/3400,queuetwo")

      val result = configRepository.onQueueMemberIds(qmIds)

      result.size should be(2)
      verify(factory).createGetQueueMemberConfig("Agent/3456,queueone")
      verify(factory).createGetQueueMemberConfig("Agent/3400,queuetwo")
    }
    "sould update of add queue member" in {
      putAgentInQueue(45,32,1)

      configRepository.updateOrAddQueueMembers(AgentQueueMember(45,32,7))

      val agQMembers = configRepository.getAgentQueueMembers
      agQMembers.length should be(1)
      agQMembers should contain(AgentQueueMember(45, 32, 7))

    }
    "should check if queue member exists" in {
      putAgentInQueue(32,12,3)

      configRepository.queueMemberExists(AgentQueueMember(32,12,3))
    }
    "translate queue member removed to agent queue member with ids and negative penalty" in {
      addAgent(Agent(512, "Jack", "Pearl", "3692", "sales"))
      addQueue(43, "queueone")
      val qmr = new QueueMemberRemoved
      qmr.setQueueName("queueone")
      qmr.setAgentNumber("3692")
      configRepository.removeQueueMember(qmr)

      configRepository.getAgentQueueMember(qmr) should be(Some(AgentQueueMember(512, 43, -1)))
    }

    "remove queue member on queue member removed" in {
      addAgent(Agent(4, "Jack", "Pearl", "3692", "sales"))
      addAgent(Agent(5, "Ken", "Quod", "3693", "sales"))
      addQueue(34, "queueone")
      addQueue(37, "queuetwo")
      putAgentInQueue(4, 34, 14)
      putAgentInQueue(5, 34, 14)
      putAgentInQueue(4, 37, 7)

      val qmRemoved = new QueueMemberRemoved
      qmRemoved.setAgentNumber("3692")
      qmRemoved.setQueueName("queueone")
      configRepository.removeQueueMember(qmRemoved)

      val agQMembers = configRepository.getAgentQueueMembers

      agQMembers should not contain (AgentQueueMember(4, 34, 14))
      agQMembers should contain(AgentQueueMember(4,37,7))
      agQMembers should contain(AgentQueueMember(5,34,14))
    }
    "update phone status with username on request" in {

      val userId = 54321
      val lineId = 5678
      configRepository.updateUser(XivoUser(userId,"joe",Some("bal"),Some("joe"), Some("pwd")))
      var phoneConfig = new PhoneConfigUpdate
      phoneConfig.setId(lineId)
      phoneConfig.setNumber("1000")
      phoneConfig.setUserId(54321)
      configRepository.onPhoneConfigUpdate(phoneConfig)

      val status = new PhoneStatusUpdate
      status.setLineId(lineId)
      status.setHintStatus("99")
      val userPhoneStatus: UserPhoneStatus = configRepository.getUserPhoneStatus(status).get

      userPhoneStatus.username should be("joe")
    }

    "on user status send back user status with agent id" in {
      val userConfig = new UserConfigUpdate
      userConfig.setAgentId(25)
      userConfig.setUserId(77)
      val userStatus = new UserStatusUpdate
      userStatus.setUserId(77)
      userStatus.setStatus("away")

      configRepository.onUserConfigUpdate(userConfig)

      val agentUserStatus = configRepository.getAgentUserStatus(userStatus).get

      agentUserStatus.agentId should be(25)
      agentUserStatus.userStatus should be("away")
    }
    "can retreive user status" in {
      val userStatus = new UserStatusUpdate
      userStatus.setUserId(84)
      userStatus.setStatus("boing")

      configRepository.getAgentUserStatus(userStatus)

      configRepository.getUserStatus(84) should be(Some(userStatus))

    }
    "can retreive phone status" in {
      val testFixture = fixture("1256", 78)

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)
      val phoneStatus = new PhoneStatusUpdate
      phoneStatus.setLineId(78)
      phoneStatus.setHintStatus("4")

      configRepository.onPhoneStatusUpdate(phoneStatus)

      configRepository.getPhoneStatus(78).get.getHintStatus() should be("4")
      configRepository.getPhoneStatus(78).get.getLineId should be(78)

    }
    "not update any phone status when no username updated" in {

      val status = new PhoneStatusUpdate
      status.setLineId(32)
      status.setHintStatus("6")
      val userPhoneStatus = configRepository.getUserPhoneStatus(status)

      userPhoneStatus should be(None)
    }

    "get line config on config request by number when found" in {
      val phoneNumberToFind = "44500"

      val testFixture = fixture(phoneNumberToFind)
      stub(lineFactory.get(testFixture.LineId)).toReturn(Some(testFixture.line))

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)

      val lineConfig = LineConfig(testFixture.LineId.toString, phoneNumberToFind, Some(testFixture.line))

      configRepository.getLineConfig(LineConfigQueryByNb(phoneNumberToFind)) should be(Some(lineConfig))
    }

    "get None on config request by number when not found" in {
      val testFixture = fixture("44500")

      configRepository.onPhoneConfigUpdate(testFixture.phoneConfig)

      configRepository.getLineConfig(LineConfigQueryByNb("777")) should be(None)
    }

    "get line config on config request by id when found" in {
      val tf = fixture("2000", 57)
      val line = Line(tf.LineId,"default", "sip", "ojhf", Some(XivoDevice("ecbe7520c44c481cbf7bbc196294f27c",
        Some("10.50.2.104"), "Snom")), None, "ip")
      stub(lineFactory.get(tf.LineId)).toReturn(Some(line))
      configRepository.onPhoneConfigUpdate(tf.phoneConfig)

      val lineConfig = LineConfig(tf.LineId.toString, "2000", Some(line))

      configRepository.getLineConfig(LineConfigQueryById(tf.LineId)) should be(Some(lineConfig))
    }

    "send agent directory on request" in {
      val agentConfig = new AgentConfigUpdate
      agentConfig.setId(34)
      agentConfig.setNumber("2000")
      agentConfig.setFirstName("John")
      agentConfig.setLastName("Doe")

      val agent = Agent(agentConfig)
      val agentState = AgentReady(34, new DateTime(), "4460", List())

      configRepository.addAgent(agent)
      configRepository.onAgentState(agentState)

      val ad = configRepository.getAgentDirectory

      ad should be(List(AgentDirectoryEntry(agent, agentState)))
    }

    "not send agent on directory if not status received" in {
      configRepository = new ConfigRepository(factory, lineFactory)
      val agentConfig = new AgentConfigUpdate
      agentConfig.setId(34)
      agentConfig.setNumber("2000")
      agentConfig.setFirstName("John")
      agentConfig.setLastName("Doe")

      val agent = Agent(agentConfig)
      configRepository.addAgent(agent)
      val ad = configRepository.getAgentDirectory

      ad should be(List())

    }

    "send agent states on request" in {
      val agentStateA = AgentReady(34, new DateTime(), "3200", List())
      val agentStateB = AgentOnCall(22, new DateTime(), true, Incoming, "3211", List(), onPause=false)

      configRepository.onAgentState(agentStateA)
      configRepository.onAgentState(agentStateB)

      val as = configRepository.getAgentStates

      as should be(List(agentStateA, agentStateB))
    }
    "retreive agent state" in {
      val agentStateA = AgentReady(34, new DateTime(), "3200", List())
      val agentStateB = AgentOnCall(22, new DateTime(), true, Incoming, "3211", List(), onPause=false)

      configRepository.onAgentState(agentStateA)
      configRepository.onAgentState(agentStateB)

      val ags = configRepository.getAgentState(22)
      ags should be(Some(agentStateB))
    }
    "get agent id logged on a phone number" in {
      val loggedId = 354
      val agentStateA = AgentReady(loggedId, new DateTime(), "3200", List())
      val agentStateB = AgentOnCall(22, new DateTime(), true, Incoming, "3211", List(), onPause=false)

      configRepository.onAgentState(agentStateA)
      configRepository.onAgentState(agentStateB)

      val agentId = configRepository.getAgentLoggedOnPhoneNumber("3200")

      agentId should be(Some(loggedId))
    }
    "do not get agent id for a phone number if agent is logged out" in {
      val loggedOutId = 354
      val agentStateA = AgentLoggedOut(loggedOutId, new DateTime(), "3200", List())

      configRepository.onAgentState(agentStateA)

      val agentId = configRepository.getAgentLoggedOnPhoneNumber("3200")

      agentId should be(None)
    }
    "do not get agent id if phone number is empty" in {
      val loggedId = 354
      val agentStateA = AgentReady(loggedId, new DateTime(), "", List())

      configRepository.onAgentState(agentStateA)

      val agentId = configRepository.getAgentLoggedOnPhoneNumber("")

      agentId should be(None)
    }
    "update user line and agent line on agent login received" in {
      val agentLogin = AgentLogin(54, new DateTime(), "3300", List())
      configRepository.linePhoneNbs = Map(874.toLong->"3300")
      configRepository.userAgents = Map(77L->54)

      stub(lineFactory.get(874)).toReturn(Some(Line(874,"default","sip","sdlkf",None, None, "ip")))
      configRepository.onAgentState(agentLogin)

      configRepository.getLineForAgent(54) should be(Some(Line(874,"default","sip","sdlkf",None, None, "ip")))
      configRepository.getLineForUser(77) should be(Some(Line(874,"default","sip","sdlkf",None, None, "ip")))
    }

    "saves queue configuration" in {
      val qConfig = new QueueConfigUpdate
      qConfig.setId(34)
      configRepository.updateQueueConfig(qConfig)
      configRepository.queues.get(34) shouldBe (Some(qConfig))
    }

    "return outbound queue number" in {
      addQueue(34,"out_grp1","4000")
      addQueue(35,"otherqueue","4010")
      addQueue(36,"out_grp2","4020")

      configRepository.getOutboundQueueNumber(List(35,36)) should be(Some("4020"))
    }

    "send back agents for groupId, queueId, penaly" in {
      val Alain = Agent(4, "Alain", "Chabot", "5236", "sales", 5)
      val Bernard = Agent(5, "Bernard", "Dante", "5237", "sales", 5)
      val Caroline = Agent(6, "Caroline", "Eniac", "5238", "marketing", 1)
      addAgent(Alain)
      addAgent(Bernard)
      addAgent(Caroline)

      putAgentInQueue(4,1,0)
      putAgentInQueue(5,1,2)
      putAgentInQueue(6,2,0)

      val agents = configRepository.getAgents(groupId=5, queueId = 1, penalty = 0)

      agents should contain(Alain)
      agents should contain noneOf(Bernard,Caroline)
    }

    "send back agents for groupId, not in queueId" in {
      import AgentBuilderP._
      val Daniel = addInConfig("Daniel", "Fan", "5239") inGroup(780) inQueue(100) build
      val Eric = addInConfig("Eric", "Gore", "5240")    inGroup(780) inQueue(225) build
      val Fanny = addInConfig("Fany", "Hotel", "5241")  inGroup(121) inQueue(230) build

      putAgent(Daniel) inQueue(115)

      val agents = configRepository.getAgentsNotInQueue(groupId=780, queueId = 100)

      agents should contain(Eric)
      agents should contain noneOf(Daniel,Fanny)
    }

    "load queue members" in {
      stub(agentQueueMemberFactory.all()).toReturn(List(AgentQueueMember(52,88,2)))

      configRepository.loadAgentQueueMembers()

      configRepository.agentQueueMembers should contain(AgentQueueMember(52,88,2))
    }

    "send meetmes on request" in {
      val meetmeA = new Meetme("test", "4000", true, new Date(), List[MeetmeMember]())
      val meetmeB = new Meetme("test2", "4002", true, new Date(), List[MeetmeMember]())

      configRepository.onMeetmeUpdate(List(meetmeA, meetmeB))

      configRepository.getMeetmeList should be(List(meetmeA, meetmeB))
    }

    "add agent statistic" in {
      val agStat = AgentStatistic(34,List(Statistic("stat1", StatPeriod(1))))

      configRepository.updateAgentStatistic(agStat)

      configRepository.agentStatistics(34) should be(agStat)
    }

    "add a new agent statistic on update" in {
      val agStat1 = AgentStatistic(34,List(Statistic("stat1", StatPeriod(1))))
      val agStat2 = AgentStatistic(34,List(Statistic("stat2", StatPeriod(34))))

      configRepository.updateAgentStatistic(agStat1)
      configRepository.updateAgentStatistic(agStat2)

      configRepository.agentStatistics(34).statistics should contain theSameElementsAs(List(Statistic("stat1", StatPeriod(1)),Statistic("stat2", StatPeriod(34))))

    }
    "update agent statistic" in {

      val agStat1 = AgentStatistic(34,List(Statistic("stat1", StatPeriod(100))))
      val agStat2 = AgentStatistic(34,List(Statistic("stat1", StatPeriod(2500))))

      configRepository.updateAgentStatistic(agStat1)
      configRepository.updateAgentStatistic(agStat2)

      configRepository.agentStatistics(34) should be(agStat2)
    }

    "return a list of statistics" in {
      val agStat1 = AgentStatistic(34,List(Statistic("stat1", StatPeriod(1))))
      val agStat2 = AgentStatistic(52,List(Statistic("stat2", StatPeriod(34))))

      configRepository.updateAgentStatistic(agStat1)
      configRepository.updateAgentStatistic(agStat2)

      configRepository.getAgentStatistics should be(List(agStat1, agStat2))
    }

    "add a call to a queue" in {
      val queue = "foo"
      val uniqueId = "123456.789"
      val queueCall = QueueCall(2, "foo", "3345678", new DateTime())

      configRepository.onQueueCallReceived(queue, uniqueId, queueCall)

      configRepository.queueCalls shouldEqual Map(queue -> Map(uniqueId -> queueCall))
    }

    "add a call to the existing ones if the queue is already registered" in {
      val queue = "foo"
      val uniqueId1 = "123456.789"
      val uniqueId2 = "123456.999"
      val queueCall1 = QueueCall(2, "foo", "3345678", new DateTime())
      val queueCall2 = QueueCall(3, "bar", "44445678", new DateTime())

      configRepository.onQueueCallReceived(queue, uniqueId1, queueCall1)
      configRepository.onQueueCallReceived(queue, uniqueId2, queueCall2)

      configRepository.queueCalls shouldEqual Map(queue -> Map(uniqueId1 -> queueCall1, uniqueId2 -> queueCall2))
    }

    "remove a call from a queue" in {
      val queue = "foo"
      val uniqueId1 = "123456.789"
      val uniqueId2 = "123456.999"
      val queueCall1 = QueueCall(2, "foo", "3345678", new DateTime())
      val queueCall2 = QueueCall(3, "bar", "44445678", new DateTime())

      configRepository.queueCalls = Map(queue -> Map(uniqueId1 -> queueCall1, uniqueId2 -> queueCall2))
      configRepository.onQueueCallFinished(queue, uniqueId2)

      configRepository.queueCalls shouldEqual Map(queue -> Map(uniqueId1 -> queueCall1))
    }

    "not fail if the uniqueId is missing" in {
      val queue = "foo"
      configRepository.queueCalls = Map(queue -> Map())

      configRepository.onQueueCallFinished(queue, "123456.789")
      configRepository.queueCalls shouldEqual Map(queue -> Map())
    }

    "not fail if the queue is missing" in {
      configRepository.onQueueCallFinished("foo", "123456.789")
      configRepository.queueCalls shouldEqual Map()
    }

    "retrieve the calls on a queue" in {
      val queueName = "foo"
      val queueId = 10L
      val queueConfig = mock[QueueConfigUpdate]
      stub(queueConfig.getName).toReturn(queueName)
      val queueCall = QueueCall(1, "bar", "334578", new DateTime())

      configRepository.queues = Map(queueId -> queueConfig)
      configRepository.queueCalls = Map(queueName -> Map("123456.89" -> queueCall))

      configRepository.getQueueCalls(queueId) shouldEqual QueueCallList(queueId, List(queueCall))
    }

    "retrieve the calls on a queue by name" in {
      val queueName = "foo"
      val queueId = 10L
      val queueConfig = mock[QueueConfigUpdate]
      stub(queueConfig.getId).toReturn(queueId.toInt)
      stub(queueConfig.getName).toReturn(queueName)
      val queueCall = QueueCall(1, "bar", "334578", new DateTime())

      configRepository.queues = Map(queueId -> queueConfig)
      configRepository.queueCalls = Map(queueName -> Map("123456.89" -> queueCall))

      configRepository.getQueueCalls(queueName) shouldEqual Some(QueueCallList(queueId, List(queueCall)))
    }

    "get the agent number by cti username" in {
      val ctiUserName = "toto"
      val agent = Agent(54, "John", "Doe", "1000", "default")
      val userId = 21L
      configRepository.users = Map(userId -> XivoUser(userId, "Toto", Some("Doe"), Some(ctiUserName), None))
      configRepository.userAgents = Map(userId -> agent.id)
      configRepository.agents = Map(agent.id -> agent)

      configRepository.agentFromUsername(ctiUserName) shouldEqual Some(agent)
    }

    "get the device interface by cti username" in {
      val ctiUserName = "toto"
      val lineName = "aasfeg"
      val userId = 21L
      configRepository.users = Map(userId -> XivoUser(userId, "Toto", Some("Doe"), Some(ctiUserName), None))
      configRepository.userLines = Map(userId -> Line(12, "default", "sip", lineName, None, None, "ip"))

      configRepository.interfaceFromUsername(ctiUserName) shouldEqual Some(s"SIP/$lineName")
    }

    "return the groupId of the agent" in {
      val agent1 = Agent(1, "Toto", "Doe", "1000", "default", 10)
      val agent2 = Agent(2, "John", "Luke", "1001", "default", 20)
      configRepository.agents = Map(agent1.id -> agent1,
                                    agent2.id -> agent2)

      configRepository.groupIdForAgent(agent1.id) shouldEqual Some(10)
    }

    "return the phone number of the user" in {
      val user = XivoUser(12, "John", Some("Doe"), Some("j.doe"), None)
      val line = Line(112, "default", "SIP", "ajkoas", None, None, "ip")
      val nb = "44580"
      configRepository.updateUser(user)
      configRepository.updateUserLine(user.id, line)
      configRepository.updateLinePhoneNb(line.id, nb)

      configRepository.phoneNumberForUser(user.username.get) shouldEqual Some(nb)
    }

    "find a user by phone number" in {
      val user = XivoUser(12, "John", Some("Doe"), Some("j.doe"), None)
      val line = Line(112, "default", "SIP", "ajkoas", None, None, "ip")
      val nb = "44580"
      configRepository.updateUser(user)
      configRepository.updateLineUser(line.id, user.id)
      configRepository.updateLinePhoneNb(line.id, nb)

      configRepository.userNameFromPhoneNb(nb) shouldEqual user.username
    }

    "get the queues for an agent" in {
      val agentId = 12
      configRepository.updateOrAddQueueMembers(AgentQueueMember(agentId, 3, 0))
      configRepository.updateOrAddQueueMembers(AgentQueueMember(agentId, 4, 0))
      configRepository.updateOrAddQueueMembers(AgentQueueMember(agentId + 1, 5, 0))

      configRepository.getQueuesForAgent(agentId) should contain allOf(3, 4)
    }
  }
  private def addAgent(ag: Agent) = configRepository.addAgent(ag)

  private def addQueue(id: Int, name: String, number:String="") = {
    val qcf = new QueueConfigUpdate
    qcf.setId(id)
    qcf.setName(name)
    qcf.setNumber(number)
    configRepository.updateQueueConfig(qcf)
  }

  private def putAgentInQueue(agentId: Agent.Id, queueId : Long, penalty : Int): Unit = {
    configRepository.agentQueueMembers = AgentQueueMember(agentId, queueId, penalty) :: configRepository.agentQueueMembers
  }

  object AgentBuilderP {
    var agentId: Agent.Id = 0
    val context = "default"
    class AgentBuilder(firstName: String, lastName : String, number: String, groupId: Option[Long], queueId:Option[Long], penalty:Option[Int]) {
      def inGroup(id:Long)= new AgentBuilder(firstName, lastName, number, Some(id), queueId, penalty)
      def inQueue(id:Long) = new AgentBuilder(firstName, lastName, number, groupId, Some(id), penalty)
      def withPenalty(pen:Int) = new AgentBuilder(firstName, lastName, number, groupId, queueId, Some(pen))

      def build() = {
        agentId = agentId +1
        val agent = Agent(agentId, firstName, lastName, number, context, groupId.getOrElse(0))
        configRepository.addAgent(agent)
        configRepository.agentQueueMembers = AgentQueueMember(agent.id, queueId.get, penalty.getOrElse(0)) :: configRepository.agentQueueMembers
        agent
      }
    }

    def addInConfig(firstName: String, lastName : String, number: String) = new AgentBuilder(firstName, lastName, number, None, None, None)

    class AgentB(agent: Agent) {
      def inQueue(queueId: Long) = configRepository.agentQueueMembers = AgentQueueMember(agent.id, queueId, 0) :: configRepository.agentQueueMembers
    }
    def putAgent(agent : Agent) = new AgentB(agent)
  }
}
