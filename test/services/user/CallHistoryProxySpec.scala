package services.user

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.joda.time.{DateTime, Period}
import org.scalatest.mock.MockitoSugar
import services.ActorFactory
import services.request.{BaseRequest, UserCallHistoryRequest}
import xivo.models.{CallStatus, CallDetail, CallHistory}

class CallHistoryProxySpec extends TestKitSpec("CallHistoryProxySpec") with MockitoSugar {

  val callHistoryManager = TestProbe()

  trait TestActorFactory extends ActorFactory {
    override val callHistoryManagerURI = callHistoryManager.ref.path.toString
  }

  class Helper {
    def actor = {
      val a = TestActorRef[CallHistoryProxy](Props(new CallHistoryProxy with TestActorFactory))
      (a, a.underlyingActor)
    }
  }

  "A CallHistoryProxy" should {
    "forward UserCallHistoryRequest to the CallHistoryManager" in new Helper {
      val (ref, proxy) = actor
      ref ! UserCallHistoryRequest(7, "jdoe")
      callHistoryManager.expectMsg(BaseRequest(ref, UserCallHistoryRequest(7, "jdoe")))
      proxy.requester.isDefined shouldBe true
    }

    "send the CallHistory back to the requester" in new Helper {
      val (ref, proxy) = actor
      val requester = TestProbe()
      proxy.requester = Some(requester.ref)
      val history = CallHistory(List(CallDetail(new DateTime, Some(new Period), "4000", Some("4001"), CallStatus.Answered)))

      ref ! history

      requester.expectMsg(history)
    }
  }

}
