package services.user

import akka.actor.{ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.XucUser
import org.json.JSONObject
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerSuite
import org.xivo.cti.MessageFactory
import services.request.{BusyForward, NaForward, UncForward, BaseRequest}
import services.{ActorFactory, CtiRouter, XucEventBus, XucStatsEventBus}
import xivo.websocket.WsBus
import org.mockito.Mockito.{never, stub, verify}

class UserActionSpec extends TestKitSpec("CtiUserAction") with MockitoSugar  with OneAppPerSuite {
  val testCtiLink = TestProbe()
  val messageFactory = mock[MessageFactory]
  val eventBus = mock[XucEventBus]
  val statEventBus = mock[XucStatsEventBus]
  val wsBus = mock[WsBus]
  val agentActionService = TestProbe()

  class Helper {
    trait TestActorFactory extends ActorFactory {
      override def getCtiLink(username: String) = testCtiLink.ref
      override def getFilter(user: XucUser, router:ActorRef) = TestProbe().ref
      override def getAgentManager = TestProbe().ref
      override val statusPublishURI = "/user/fakepublish"

    }
    def actor() = {
      val a = TestActorRef[CtiRouter](
        Props(new CtiRouter(XucUser("test","pwd"), wsBus, eventBus=eventBus, statEventBus=statEventBus, messageFactory=messageFactory)
          with TestActorFactory with CtiUserAction))
      a.underlyingActor.agentActionService = agentActionService.ref
      (a, a.underlyingActor)
    }
  }

  "a Cti user action" should {
    "send unconditionnal forward to ctiLink" in new Helper {
      val (ref,ctiRouter) = actor()

      val json = new JSONObject()

      stub(messageFactory.createUnconditionnalForward("3125",true)).toReturn(json)

      ref ! BaseRequest(TestProbe().ref,UncForward("3125", true))

      verify(messageFactory).createUnconditionnalForward("3125",true)
      testCtiLink.expectMsg(json)

    }
    "Send na forward to ctiLink" in new Helper {
      val (ref,ctiRouter) = actor()

      val json = new JSONObject()

      stub(messageFactory.createNaForward("2356",false)).toReturn(json)

      ref ! BaseRequest(TestProbe().ref,NaForward("2356", false))

      verify(messageFactory).createNaForward("2356",false)
      testCtiLink.expectMsg(json)

    }

    "Send busy forward to ctilink" in new Helper {
      val (ref,ctiRouter) = actor()

      val json = new JSONObject()

      stub(messageFactory.createBusyForward("5699",true)).toReturn(json)

      ref ! BaseRequest(TestProbe().ref,BusyForward("5699", true))

      verify(messageFactory).createBusyForward("5699",true)
      testCtiLink.expectMsg(json)

    }
  }

}
