package services.phone

import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import services.XucAmiBus.{ChannelEvent, AmiType}
import services.{XucAmiBus, XucEventBus}
import xivo.events.{UserData, PhoneEvent, PhoneEventType}
import xivo.xucami.models.{CallerId, Channel, ChannelState}

class PhoneEventTransformerSpec extends TestKitSpec("PhoneEventTransformerSpec") with MockitoSugar {


  class Helper {
    val amiBus = mock[XucAmiBus]
    val eventBus = mock[XucEventBus]
    val userDataFilter = mock[UserData.filter]

    def actor(withFilter:UserData.filter= UserData.filterData) = {
      val sa = TestActorRef(new PhoneEventTransformer(eventBus, amiBus, withFilter))
      (sa, sa.underlyingActor)
    }
  }

  val ringingChannel = Channel("123.1","SIP/psp",CallerId("Alfred","1438"),"123.0",ChannelState.RINGING, connectedLineNb = Some("4567"))
  val ringingChannelAgentCallback = Channel("123.1","Local/id-22@agentcallback-00000009;1",CallerId("Alfred","1438"),"123.0",ChannelState.RINGING, connectedLineNb = Some("4567"))
  val ringingChannelWithUserData = Channel("123.1","SIP/psp",CallerId("Alfred","1438"),"123.0",ChannelState.RINGING, connectedLineNb = Some("4567"),variables= Map("USR_DATA1"->"Hello"))
  val ringingChannelFromQueue = Channel("123.1","SIP/psp",CallerId("Alfred","1438"),"123.0",ChannelState.RINGING, connectedLineNb = Some("4567"),variables= Map("XIVO_QUEUENAME"->"blue"))

  val dialingChannel = Channel("1446653516.110","SIP/g2lhmi-00000056",CallerId("Irène Dupont","1118"),"1446653516.110",ChannelState.ORIGINATING, exten=Some("1052"))
  val ringingChannelOriginate = Channel("1446653516.110","SIP/g2lhmi-00000056",CallerId("Irène Dupont","1118"),"1446653516.110",ChannelState.RINGING, connectedLineNb=Some("1052"),
                                          variables= Map("XUC_CALLTYPE"->"Originate"))
  val upChannelOriginate = Channel("1446653516.110","SIP/g2lhmi-00000056",CallerId("Irène Dupont","1118"),"1446653516.110",ChannelState.UP, connectedLineNb=Some("1052"),
    variables= Map("XUC_CALLTYPE"->"Originate"))
  val upChannelOrigonateEstablished = Channel("1446653516.110","SIP/g2lhmi-00000056",CallerId("Irène Dupont","1118"),"1446653516.110",ChannelState.UP, connectedLineNb=Some("1052"),
  variables= Map("XUC_CALLTYPE"->"Originate","XIVO_CALLOPTIONS"->"T"))

  val channelEstablished = Channel("1446653516.110","SIP/g2lhmi-00000056",CallerId("Irène Dupont","1118"),"1446653516.110",ChannelState.UP, connectedLineNb=Some("1052"),
    variables= Map())
  val channelEstablishedAgentCallback = Channel("1446653516.110","Local/id-22@agentcallback-00000009;1",CallerId("Irène Dupont","1118"),"1446653516.110",ChannelState.UP, connectedLineNb=Some("1052"),
    variables= Map())

  val channelHangup = Channel("1446653516.110","SIP/g2lhmi-00000056",CallerId("Irène Dupont","1118"),"1446653516.110",ChannelState.HUNGUP, connectedLineNb=Some("1052"),
    variables= Map())

  "A phone event transformer" should {
    "subscribe to ami bus on when starting" in new Helper {

      val (ref, a) = actor()
      verify(amiBus).subscribe(ref, AmiType.ChannelEvent)
    }

    "publish a ringing event on channel event received" in new Helper {
      val (ref, a) = actor()

      ref ! ChannelEvent(ringingChannel)

      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventRinging,ringingChannel.callerId.number,ringingChannel.connectedLineNb.getOrElse(""),ringingChannel.linkedChannelId, ringingChannel.id))

    }
    "publish a ringing event with user data" in new Helper {
      val (ref, a) = actor(userDataFilter)
      stub(userDataFilter(ringingChannelWithUserData.variables)).toReturn(Map("USR_DATA1"->"Hello"))

      ref ! ChannelEvent(ringingChannelWithUserData)


      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventRinging,ringingChannelWithUserData.callerId.number,
        ringingChannelWithUserData.connectedLineNb.getOrElse(""),ringingChannelWithUserData.linkedChannelId, ringingChannelWithUserData.id, userData=ringingChannelWithUserData.variables))

      verify(userDataFilter)(ringingChannelWithUserData.variables)

    }
    "Do not publish agent callback events" in new Helper {
      val (ref, a) = actor()

      ref ! ChannelEvent(ringingChannelAgentCallback)

      verifyNoMoreInteractions(eventBus)

    }
    "Publish ringing event with queue name" in new Helper {
      val (ref, a) = actor()

      ref ! ChannelEvent(ringingChannelFromQueue)

      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventRinging,ringingChannel.callerId.number,
        ringingChannel.connectedLineNb.getOrElse(""),ringingChannel.linkedChannelId, ringingChannel.id, queueName=Some("blue")))

    }
    "Publish dialing event" in new Helper {
      val (ref, a) = actor(userDataFilter)
      stub(userDataFilter(dialingChannel.variables)).toReturn(Map())

      ref ! ChannelEvent(dialingChannel)
      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventDialing,dialingChannel.callerId.number,dialingChannel.exten.getOrElse(""),dialingChannel.linkedChannelId, dialingChannel.id))

      verify(userDataFilter)(dialingChannel.variables)
    }
    "On ringing originate publish dialing event" in new Helper {
      val (ref, a) = actor(userDataFilter)
      stub(userDataFilter(ringingChannelOriginate.variables)).toReturn(Map("XUC_CALLTYPE"->"Originate"))

      ref ! ChannelEvent(ringingChannelOriginate)

      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventDialing,
          ringingChannelOriginate.callerId.number,ringingChannelOriginate.connectedLineNb.getOrElse(""),ringingChannelOriginate.linkedChannelId, ringingChannelOriginate.id, userData=Map("XUC_CALLTYPE"->"Originate")))

      verify(userDataFilter)(ringingChannelOriginate.variables)

    }
    "On up originate publish dialing event" in new Helper {
      val (ref, a) = actor(userDataFilter)
      stub(userDataFilter(upChannelOriginate.variables)).toReturn(Map("XUC_CALLTYPE"->"Originate"))

      ref ! ChannelEvent(upChannelOriginate)

      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventDialing,
        upChannelOriginate.callerId.number,upChannelOriginate.connectedLineNb.getOrElse(""),upChannelOriginate.linkedChannelId, upChannelOriginate.id, userData=Map("XUC_CALLTYPE"->"Originate")))

      verify(userDataFilter)(upChannelOriginate.variables)

    }
    "On up originate established publish established event" in new Helper {
      val (ref, a) = actor(userDataFilter)
      stub(userDataFilter(upChannelOrigonateEstablished.variables)).toReturn(Map("XUC_CALLTYPE"->"Originate"))

      ref ! ChannelEvent(upChannelOrigonateEstablished)

      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventEstablished,
        upChannelOriginate.callerId.number,upChannelOrigonateEstablished.connectedLineNb.getOrElse(""),upChannelOrigonateEstablished.linkedChannelId, upChannelOrigonateEstablished.id, userData=Map("XUC_CALLTYPE"->"Originate")))

      verify(userDataFilter)(upChannelOrigonateEstablished.variables)

    }

    "On up publish established event" in new Helper {
      val (ref, a) = actor(userDataFilter)
      stub(userDataFilter(channelEstablished.variables)).toReturn(Map())

      ref ! ChannelEvent(channelEstablished)
      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventEstablished,channelEstablished.callerId.number,channelEstablished.connectedLineNb.getOrElse(""),channelEstablished.linkedChannelId, channelEstablished.id))

      verify(userDataFilter)(channelEstablished.variables)

    }
    "Do not publish agent callback established events" in new Helper {
      val (ref, a) = actor()

      ref ! ChannelEvent(channelEstablishedAgentCallback)

      verifyNoMoreInteractions(eventBus)

    }
    "On hungup publish released event" in new Helper {
      val (ref, a) = actor(userDataFilter)
      stub(userDataFilter(channelHangup.variables)).toReturn(Map())

      ref ! ChannelEvent(channelHangup)
      verify(eventBus).publish(PhoneEvent(PhoneEventType.EventReleased,channelHangup.callerId.number,channelHangup.connectedLineNb.getOrElse(""),channelHangup.linkedChannelId, channelHangup.id))

      verify(userDataFilter)(channelHangup.variables)

    }
  }
}
