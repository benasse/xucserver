package services

import java.util.{Date, UUID}

import akka.actor.{ActorRef, Props, Terminated}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import com.fasterxml.jackson.databind.node.ObjectNode
import models.{CallbackStatus, XucUser}
import org.json.JSONObject
import org.mockito.Mockito.{never, stub, verify}
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerSuite
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{AgentStatusUpdate, DirectoryResult, IpbxCommandResponse}
import org.xivo.cti.model.Endpoint
import org.xivo.cti.model.Endpoint.EndpointType
import play.api.test.FakeApplication
import play.api.test.Helpers.running
import play.libs.Json
import services.XucEventBus.{Topic, TopicType}
import services.config.ConfigDispatcher._
import services.config.LineConfig
import services.line.{PhoneController, PhoneControllerFactory}
import services.request._
import services.user.UserAction
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType._
import xivo.models.{Line, XivoDevice}
import xivo.network.CtiLinkKeepAlive.StartKeepAlive
import xivo.network.LoggedOn
import xivo.phonedevices.OutboundDial
import xivo.websocket.LinkState.{down, up}
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus.{WsContent, WsMessageEvent}
import xivo.websocket.{LinkStatusUpdate, WebSocketEvent, WsBus}

class CtiRouterSpec
  extends TestKitSpec("CtiRouterSpec")
  with OneAppPerSuite
  with MockitoSugar {

  class Helper {
    val parent = TestProbe()
    val testCtiLink = TestProbe()
    val testCtiFilter = TestProbe()
    val configDispatcher = TestProbe()
    val agentConfig = TestProbe()
    val agentActionService = TestProbe()
    val wsBus = mock[WsBus]
    val eventBus = mock[XucEventBus]
    val statEventBus = mock[XucStatsEventBus]
    val amiBusConnector = TestProbe()
    val messageFactory = mock[MessageFactory]
    val callHistoryManager = TestProbe()
    val xivoDirectoryInterface = TestProbe()
    val callbackMgrInterface = TestProbe()
    val phoneControllerFactory = mock[PhoneControllerFactory]
    val phoneController = TestProbe()

    trait TestActorFactory extends ActorFactory {
      override def getFilter(user: XucUser, router: ActorRef) = testCtiFilter.ref
      override def getCtiLink(username: String) = testCtiLink.ref
      override def getAgentManager = TestProbe().ref
      override val statusPublishURI = "/user/fakepublish"
      override val configDispatcherURI = configDispatcher.ref.path.toString
      override val agentConfigURI = agentConfig.ref.path.toString
      override val amiBusConnectorURI = amiBusConnector.ref.path.toString
      override val callHistoryManagerURI = callHistoryManager.ref.path.toString
      override val xivoDirectoryInterfaceURI = xivoDirectoryInterface.ref.path.toString
      override val callbackMgrInterfaceURI = callbackMgrInterface.ref.path.toString
    }

    val mForward = mock[(ForwardRequest)=>{}]
    trait testUserActionService extends UserAction {
      def forward(request:ForwardRequest) = {
        mForward(request)
      }
    }

    def actor(user: XucUser) = {
      val a = TestActorRef[CtiRouter](
        Props(new CtiRouter(user, wsBus, eventBus=eventBus, statEventBus=statEventBus, messageFactory=messageFactory, phoneControllerFactory=phoneControllerFactory)
          with TestActorFactory with testUserActionService),
        parent.ref,
        "testCtiRouter")
      a.underlyingActor.agentActionService = agentActionService.ref
      a.underlyingActor.phoneController = Some(phoneController.ref)
      (a, a.underlyingActor)
    }
  }

  "A CtiRouter actor" should {


    """ forward Start message to CtiFilter
        update router user field
        send loggedon message to ctifilter if exists""" in new Helper {
      val user = XucUser("yoip", "4etr,wqd", Some(2004))
      val (ref, ctiRouter) = actor(user)
      val loggedOnMsg = LoggedOn(user, "34", null)
      ctiRouter.loggedOnMsg = Some(loggedOnMsg)

      val updatedUser = XucUser("yoip", "pwd", Some(2006))
      val startMessage = Start(updatedUser)

      ref ! startMessage

      testCtiFilter.expectMsgAllOf(startMessage,loggedOnMsg)

      ctiRouter.user should be(updatedUser)
    }

    """If logon message exists
        Send new client connected to ctiFilter on wsconnected
        update router user field
        send start message to ctifilter"""  in new Helper {

      val user = XucUser("wsconnected", "4etr,wqd", Some(2004))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34", null))
      val updatedUser = XucUser("yoip", "pwd", Some(2006))

      val wsActor = TestProbe()

      ref ! WsConnected(wsActor.ref, updatedUser)

      testCtiFilter.expectMsgAllOf(Start(updatedUser),ClientConnected(wsActor.ref, LoggedOn(user, "34", null)))
      ctiRouter.user should be(updatedUser)

    }
    "stop self and ctilink when ctilink terminates" in new Helper {
      val user = XucUser("shre", "sdf78d", Some(2002))
      val (ref, ctiRouter) = actor(user)

      parent.watch(testCtiFilter.ref)
      parent.watch(ref)
      ctiRouter.ctiLink = testCtiLink.ref

      val terminated = mock[Terminated]
      stub(terminated.getActor()).toReturn(testCtiLink.ref)

      ctiRouter.receive(terminated)

      parent.expectMsgAllClassOf(classOf[Terminated], classOf[Terminated])
    }

    "forward directory result to the config dispatcher" in new Helper {
      val (ref, ctiRouter) = actor(XucUser("tata", "sdf78d", Some(2006)))

      val message = new DirectoryResult

      ctiRouter.receive(message)

      configDispatcher.expectMsg(message)
    }

    "forward ipbxcommandmessage to agent service" in new Helper {
      val (ref, _) = actor(XucUser("ipbxcommand", "sdf78d", Some(2006)))
      val ipbxcommandMessage = new IpbxCommandResponse("agent_login_invalid_exten", new Date)

      ref ! ipbxcommandMessage

      agentActionService.expectMsg(ipbxcommandMessage)
    }
    "forward Cti messages to the ctiFilter" in new Helper {
      val (ref, ctiRouter) = actor(XucUser("tata", "sdf78d", Some(2006)))
      val ctiMessage = new AgentStatusUpdate(34, null)

      ref ! ctiMessage

      testCtiFilter.expectMsg(ctiMessage)
    }

    "forward config request to configDispatcher" in new Helper {
      val (ref, ctiRouter) = actor(XucUser("cfgRequest", "sdf78d", Some(2006)))

      val requestConfig = RequestConfig(self, GetConfig("agent"))

      ref ! requestConfig

      configDispatcher.expectMsg(requestConfig)
    }
    "forward config change request to config dispatcher " in new Helper {
      val (ref, ctiRouter) = actor(XucUser("cfgRequest", "sdf78d", Some(2006)))

      val cfgChangeRequest = ConfigChangeRequest(ref,SetAgentQueue(32,21,7))

      ref ! cfgChangeRequest

      configDispatcher.expectMsg(cfgChangeRequest)

    }

    "forward agent status request to configDispatcher and do not start keepalive" in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeAgent
      val (ref, ctiRouter) = actor(XucUser("cfgRequest", "sdf78d", Some(2006)))

      val requestStatus = RequestStatus(self, 32, TypeAgent)

      ref ! requestStatus

      configDispatcher.expectMsg(requestStatus)
    }

    "send started message to creator when loggedon foward to filter and config dispatcher start keepalive" in new Helper {
      val user = XucUser("loggedON", "sdf78d", Some(2006))
      val (ref, ctiRouter) = actor(user)
      val linkKeepAlive = TestProbe()
      ctiRouter.ctiKeepAlive = linkKeepAlive.ref

      ctiRouter.creator = self

      val loggedOn = LoggedOn(user, "34", null)

      ref ! loggedOn

      testCtiFilter.expectMsg(loggedOn)
      expectMsgType[Started]
      linkKeepAlive.expectMsg(StartKeepAlive(loggedOn.userId, ctiRouter.ctiLink))

    }

    "Publish JsonNode messages to the web socket bus" in new Helper {
      val user = XucUser("moilp", "98dd", Some(1456))
      val (ref, ctiRouter) = actor(user)

      val message = Json.parse("{\"hello\": \"test\"}")

      ref ! message

      verify(wsBus).publish(WsMessageEvent(WsBus.browserTopic(user.ctiUsername),WsContent(message)))
    }

    "publish link status down message when CtiLink sends one and remove loggedon message" in new Helper {
      val user = XucUser("linkstatus", "sdf78d", Some(2006))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34", null))

      ref ! LinkStatusUpdate(down)

      verify(wsBus).publish(WsMessageEvent(WsBus.browserTopic("linkstatus"),WsContent(WebSocketEvent.createEvent(LinkStatusUpdate(down)))))
      ctiRouter.loggedOnMsg should be(None)
    }
    "send start message to cti link on link status up do not publish link status" in new Helper {
      val user = XucUser("linkstatusup", "sdf78d", Some(2006))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.ctiLink = testCtiLink.ref

      ref ! LinkStatusUpdate(up)

      testCtiLink.expectMsg(Start(user))

      verify(wsBus,never).publish(WsMessageEvent(WsBus.browserTopic("linkstatusup"),WebSocketEvent.createEvent(LinkStatusUpdate(down))))
    }
    "receive a request sucess on request forwarded to ctilink" in new Helper {
      val user = XucUser("requestSucess", "password", Some(5023))
      val (ref, ctiRouter) = actor(user)

      ref ! Start(user)
      ref ! LoggedOn(user, "34", null)
      expectMsgType[Started]
      val ctiRequest = new MessageFactory().createDial(new Endpoint(EndpointType.PHONE, "3546"))
      ref ! ctiRequest
      expectMsgType[CtiReqSuccess]

    }

    "try to resend request if user not logged to the cti server while receiving a request" in new Helper {
      val user = XucUser("resendRequest", "password", Some(5023))
      val (ref, ctiRouter) = actor(user)

      ref ! Start(user)
      val ctiRequest = new MessageFactory().createDial(new Endpoint(EndpointType.PHONE, "3546"))
      ref ! ctiRequest
      ref ! LoggedOn(user, "34", null)
      expectMsgType[Started]
      expectMsgType[CtiReqSuccess]

    }

    "send an error on request retry if user not logged to the cti server" in new Helper {
      val user = XucUser("requestNotLogged", "password", Some(5023))
      val (ref, ctiRouter) = actor(user)

      ref ! Start(user)
      val ctiRequest = new MessageFactory().createDial(new Endpoint(EndpointType.PHONE, "3546"))
      ref ! ctiRequest
      expectMsgType[CtiReqError]
    }

    "decode base requests and send browser message to cti filter" in new Helper {
      val (ref, _) = actor(XucUser("baseRequest", "sdf78d", Some(2006)))

      val bMessage = BrowserMessage(createMessage("cmd"))
      val baseRequest = BaseRequest(self,bMessage)

      ref ! baseRequest

      testCtiFilter.expectMsg(bMessage)
    }

    "Send update config request to config dipatcher" in new Helper {
      val (ref, _) = actor(XucUser("UpdateConfigRequest", "sdf78d", Some(2006)))

      val updateConfigRequest = mock[UpdateConfigRequest]

      ref ! BaseRequest(self, updateConfigRequest)

      configDispatcher.expectMsg(ConfigChangeRequest(self, updateConfigRequest))

    }
    "Send move agent in group message to agent config service" in new Helper {
      val (ref, _) = actor(XucUser("agentConfigRequest", "sdf78d", Some(2006)))

      val agentConfigRequest = mock[AgentConfigRequest]

      ref ! BaseRequest(self,agentConfigRequest)

      agentConfig.expectMsg(BaseRequest(self,agentConfigRequest))
    }
    "send agent login request without any id to cti filter to be completed" in new Helper {
      val (ref, _) = actor(XucUser("agentLoginWithoutId", "sdf78d", Some(2006)))

      val loginReq = AgentLoginRequest(None,Some("2001"))

      ref ! BaseRequest(self,loginReq)

      testCtiFilter.expectMsg(loginReq)

    }
    "send agent listen request with no userid to ctifilter" in new Helper {
      val (ref, _) = actor(XucUser("agentListenRequest", "listen", Some(3006)))

      val agentListen = AgentListen(34)

      ref ! BaseRequest(self, agentListen)

      testCtiFilter.expectMsg(agentListen)

    }
    "Send agent listen request with a userid to agent action service" in new Helper {
      val (ref, _) = actor(XucUser("agentListenRequest", "listen", Some(3006)))

      val agentListen = AgentListen(34, Some(56))

      ref ! agentListen

      agentActionService.expectMsg(agentListen)

    }
    "send agent action requests to agent action service" in new  Helper {
      val (ref, _) = actor(XucUser("agentActionRequest", "sdf78d", Some(2006)))

      val agentActionRequest = mock[AgentActionRequest]

      ref ! BaseRequest(self,agentActionRequest)

      agentActionService.expectMsg(agentActionRequest)

    }

    "subscribe sender to agent events on request" in new Helper {
      val (ref, _) = actor(XucUser("eventbusrequest", "sdf78d", Some(2006)))

      val subscribeToAgentEvents = SubscribeToAgentEvents

      val requester = TestProbe()

      ref ! BaseRequest(requester.ref, subscribeToAgentEvents)

      verify(eventBus).subscribe(requester.ref, Topic(TopicType.Event, ObjectType.TypeAgent))
    }

    "add phone number, forward config request from browser to configDispatcher and subscribe to event bus" in new Helper {
      val (ref, ctiRouter) = actor(XucUser("configRequest", "sdf78d", Some(2006)))

      val requester = TestProbe()

      val requestConfig = BaseRequest(requester.ref, GetConfig("agent"))

      ref ! requestConfig

      configDispatcher.expectMsg(RequestConfigForNb(requester.ref, GetConfig("agent"), Some("2006")))
      verify(eventBus).subscribe(requester.ref, Topic(TopicType.Config, ObjectType.TypeAgent))

    }
    "forward get list request from browser to configDispatcher and subscribe to event bus" in new Helper {
      import services.config.ConfigDispatcher.ObjectType._

      val (ref, ctiRouter) = actor(XucUser("configRequest", "sdf78d", Some(2006)))

      val requester = TestProbe()

      val requestList = BaseRequest(requester.ref, GetList(TypeQueueMember))

      ref ! requestList

      configDispatcher.expectMsg(RequestConfig(requester.ref, GetList("queuemember")))
      verify(eventBus).subscribe(requester.ref, Topic(TopicType.Config, ObjectType.TypeQueueMember))

    }
    "forward monitor action from browser to AmiBusConnector" in new Helper {

      val (ref, ctiRouter) = actor(XucUser("configRequest", "sdf78d", Some(2006)))

      val request = MonitorPause(1)

      ref ! BaseRequest(null, request)

      amiBusConnector.expectMsg(MonitorPause(1))

    }
    "forward outboundDial to amiBusconnector" in new Helper {
      val (ref, ctiRouter) = actor(XucUser("configRequest", "sdf78d", Some(2006)))

      val request = OutboundDial("123456789", 85, "3000", Map("VAR" -> "Value"), "10.20.10.3")

      ref ! request

      amiBusConnector.expectMsg(request)

    }
    """upon configQuery command received
      |- send configQuery request to config service""" in new Helper {
      val user = XucUser("configQuery", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)

      val requester = TestProbe()

      val configQuery = mock[ConfigQuery]
      val requestConfig = BaseRequest(requester.ref,configQuery)

      ref ! requestConfig

      configDispatcher.expectMsg(RequestConfig(requester.ref, configQuery))
    }

    "Subscribe to queue statistics on request" in new Helper {
      val user = XucUser("subscribestats", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val requester = TestProbe()

      val requestSubStat = BaseRequest(requester.ref,SubscribeToQueueStats)

      ref ! requestSubStat

      verify(statEventBus).subscribe(requester.ref, ObjectDefinition(Queue))
    }

    "Subscribe to agent statistics on request and get all statistics" in new Helper {
      val user = XucUser("subscribeagentstats", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val requester = TestProbe()

      val requestSubStat = BaseRequest(requester.ref,SubscribeToAgentStats)

      ref ! requestSubStat

      configDispatcher.expectMsg(RequestConfig(requester.ref, GetAgentStatistics))
      verify(eventBus).subscribe(requester.ref, Topic(TopicType.Stat, ObjectType.TypeAgent))
    }

    "subscribe to queue calls on request and get queue calls" in new Helper {
      val user = XucUser("subscribequeuecalls", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val requester = TestProbe()
      val queueId = 3

      val requestQCalls = BaseRequest(requester.ref, SubscribeToQueueCalls(queueId))

      ref ! requestQCalls

      configDispatcher.expectMsg(RequestConfig(requester.ref, GetQueueCalls(queueId)))
      verify(eventBus).subscribe(requester.ref, Topic(TopicType.Event, ObjectType.TypeQueue, Some(queueId)))
    }

    "unsubscribe to queue calls on request" in new Helper {
      val user = XucUser("unsubscribequeuecalls", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val requester = TestProbe()
      val queueId = 3

      val requestQCalls = BaseRequest(requester.ref, UnSubscribeToQueueCalls(queueId))

      ref ! requestQCalls

      verify(eventBus).unsubscribe(requester.ref, Topic(TopicType.Event, ObjectType.TypeQueue, Some(queueId)))
    }

    "transform the InviteConferenceRoom message and send the result to cti link" in new Helper {
      val userId = 5
      val user = XucUser("InviteConferenceRoom", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val json = new JSONObject()
      stub(messageFactory.createInviteConferenceRoom(userId)).toReturn(json)
      val requester = TestProbe()

      ref ! BaseRequest(requester.ref, InviteConferenceRoom(userId))

      verify(messageFactory).createInviteConferenceRoom(userId)
      testCtiLink.expectMsg(json)
    }

    "send forward request to useractionservice" in new Helper {
      val (ref, ctiRouter) = actor(XucUser("useractionservice", "pwd", Some(2104)))

      val forwardRequest = mock[ForwardRequest]

      ref ! BaseRequest(TestProbe().ref,forwardRequest)

      verify(mForward)(forwardRequest)
    }

    "send GetAgentCallHistory request to CallHistoryManager service" in new Helper {
      val userName = "toto"
      val (ref, ctiRouter) = actor(XucUser(userName, "pwd", Some(2104)))
      val historyRequest = GetAgentCallHistory(5)
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, historyRequest)

      callHistoryManager.expectMsg(BaseRequest(testProbe.ref, AgentCallHistoryRequest(5, userName)))
    }

    "send GetUserCallHistory request to CallHistoryManager service" in new Helper {
      val userName = "toto"
      val (ref, ctiRouter) = actor(XucUser(userName, "pwd", Some(2104)))
      val historyRequest = GetUserCallHistory(5)
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, historyRequest)

      callHistoryManager.expectMsg(BaseRequest(testProbe.ref, UserCallHistoryRequest(5, userName)))
    }

    "send DirectoryLookUp request to xivoDirectory interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, DirectoryLookUp("term"))

      xivoDirectoryInterface.expectMsg(UserBaseRequest(testProbe.ref, DirectoryLookUp("term"), user))
    }


    "send GetFavorites request to xivoDirectory interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, GetFavorites)

      xivoDirectoryInterface.expectMsg(UserBaseRequest(testProbe.ref, GetFavorites, user))
    }

    "send SetFavorite request to xivoDirectory interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, AddFavorite("123", "xivou"))

      xivoDirectoryInterface.expectMsg(UserBaseRequest(testProbe.ref, AddFavorite("123", "xivou"), user))
    }

    "send RemoveFavorite request to xivoDirectory interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, RemoveFavorite("34", "xivou"))

      xivoDirectoryInterface.expectMsg(UserBaseRequest(testProbe.ref, RemoveFavorite("34", "xivou"), user))
    }

    "transfer GetCallbackLists to callbackMgr interface and subscribe to callback events" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, GetCallbackLists)

      callbackMgrInterface.expectMsg(BaseRequest(testProbe.ref, GetCallbackLists))
      verify(eventBus).subscribe(testProbe.ref, XucEventBus.callbackListsTopic())
    }

    "transfer TakeCallback to callbackMgr interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()
      val uuid = UUID.randomUUID()

      ref ! BaseRequest(testProbe.ref, TakeCallback(uuid.toString))

      callbackMgrInterface.expectMsg(TakeCallbackWithAgent(uuid.toString, "toto"))
    }

    "transfer ReleaseCallback to callbackMgr interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()
      val uuid = UUID.randomUUID()

      ref ! BaseRequest(testProbe.ref, ReleaseCallback(uuid.toString))

      callbackMgrInterface.expectMsg(ReleaseCallback(uuid.toString))
    }

    "enrich StartCallback with the username and transfer to the callbackMgr interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()
      val uuid = UUID.randomUUID()

      ref ! BaseRequest(testProbe.ref, StartCallback(uuid.toString, "3322154"))

      callbackMgrInterface.expectMsg(BaseRequest(testProbe.ref, StartCallbackWithUser(uuid.toString, "3322154", "toto")))
    }

    "transfer PhoneRequests to the phoneController" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val request = new PhoneRequest {}

      ref ! BaseRequest(self, request)

      phoneController.expectMsg(request)
    }

    "transfer UpdateCallbackTicket to the callbackMgr interface" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val testProbe = TestProbe()
      val uuid = UUID.randomUUID()

      ref ! BaseRequest(testProbe.ref, UpdateCallbackTicket(uuid.toString, Some(CallbackStatus.Answered), Some("Small comment")))

      callbackMgrInterface.expectMsg(BaseRequest(testProbe.ref, UpdateCallbackTicket(uuid.toString, Some(CallbackStatus.Answered), Some("Small comment"))))
    }

    "stop the previous phoneController and initialize a new one when receiving a LineConfig" in new Helper {
      val user = XucUser("toto", "pwd", Some(2104))
      val (ref, ctiRouter) = actor(user)
      val previousController = TestProbe()
      ctiRouter.phoneController = Some(previousController.ref)
      val line = Line(3, "default", "SIP", "ahjhjk", Some(XivoDevice("78", Some("10.10.0.2"), "Snom")))
      val lcf = LineConfig("12", "1000", Some(line))
      stub(phoneControllerFactory.createPhoneController("1000", line, ctiRouter.context)).toReturn(phoneController.ref)
      watch(previousController.ref)

      ref ! lcf

      expectTerminated(previousController.ref)
      phoneController.expectMsg(PhoneController.Start)
      ctiRouter.phoneController shouldEqual Some(phoneController.ref)
    }
  }

  "CtiRouter with different configuration" should {
    val reconfiguredApp = FakeApplication(additionalConfiguration = Map("xivo.useDird"-> false))

    "transform DirectoryLookUp to SearchDirectory and send to ctiFilter if XivoDirectory not enabled" in new Helper {
      running(reconfiguredApp) {
        val user = XucUser("toto", "pwd", Some(2104))
        val (ref, ctiRouter) = actor(user)
        ctiRouter.ctiLink = testCtiLink.ref
        ctiRouter.userLoggedOn = true
        val testProbe = TestProbe()
        val json = new JSONObject()
        stub(messageFactory.createSearchDirectory("searchedTerm")).toReturn(json)

        ref ! BaseRequest(testProbe.ref, DirectoryLookUp("searchedTerm"))

        expectMsgType[CtiReqSuccess]
        testCtiLink.expectMsg(json)
      }
    }
  }

  private def createMessage(command: String): ObjectNode = {
    val message = Json.newObject()
    message.put("claz", "web")
    message.put("command", command)
  }

}
