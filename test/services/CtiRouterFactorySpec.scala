package services

import akka.actor.{ActorRef, Props}
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import models.XucUser
import org.mockito.Mockito.{verify,stub}
import org.scalatest.mock.MockitoSugar
import org.mockito.Matchers._

class CtiRouterFactorySpec
  extends TestKitSpec("XiVO-CTI-system")
  with MockitoSugar
{

  class Helper {
    val createRouter = mock[(Props,String) => ActorRef]
    val ctiRouterFactory = new CtiRouterFactory(createRouter)

  }

  "CtiRouterFactory" should {

    "create router if there's no actor for given username" in new Helper {
        val user = XucUser("hawkeye","45,.dfee",Some(2002))
        val routerId = ActorFactory.routerPath(user.ctiUsername)
        val ctiRouterCreated = TestProbe().ref
        stub(createRouter(any(),org.mockito.Matchers.eq(routerId))).toReturn(ctiRouterCreated)

        val ctiRouter = ctiRouterFactory.getRouter(user)

        verify(createRouter)(any(),org.mockito.Matchers.eq(routerId))
        ctiRouterFactory.routers(user.ctiUsername) should be(ctiRouterCreated)
    }

    "return existing actor for given username" in new Helper {
        val user = XucUser("phil","33df-fee",Some(2002))
        val expectedActorRef = TestProbe().ref
        ctiRouterFactory.routers = Map(user.ctiUsername->expectedActorRef)
        val ctiRouter = ctiRouterFactory.getRouter(user)
        ctiRouter should be(expectedActorRef)
    }

  }

}
