package services

import akka.actor.{Props, ActorRef, ActorContext}
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito.stub
import org.mockito.Mockito.verify
import org.mockito.Mockito.times
import org.mockito.Matchers._
import xivo.models.Agent
import scala.collection.mutable.{ Map => MMap }

class AgentActorFactorySpec extends TestKitSpec("AgentActorFactorySpec") with MockitoSugar{

  "an agent actor factory" should {
    "return a new agent actor" in {
      val agaf = new AgentActorFactory
      val context = mock[ActorContext]
      val agentActor = TestProbe().ref

      stub(context.actorOf(anyObject(),anyString())).toReturn(agentActor)

      val act = agaf.getOrCreate(32, context)
      act should be(agentActor)
    }

    "return a previously created actor on getOrCreate" in {

      val agaf = new AgentActorFactory
      val context = mock[ActorContext]
      val agentActor = TestProbe().ref

      stub(context.actorOf(anyObject(),anyString())).toReturn(agentActor)

      agaf.getOrCreate(32, context)

      agaf.getOrCreate(32, context) should be(agentActor)
      verify(context, times(1)).actorOf(any[Props],anyString())

    }

    "return a previously created actor on get" in {

      val agaf = new AgentActorFactory
      val context = mock[ActorContext]
      val agentActor = TestProbe().ref

      stub(context.actorOf(anyObject(),anyString())).toReturn(agentActor)

      agaf.getOrCreate(32, context)

      agaf.get(32) should be(Some(agentActor))

    }

    "return a new agent stat collector" in {
      val agaf = new AgentActorFactory
      val context = mock[ActorContext]
      val agentStatCollectorActor = TestProbe().ref

      stub(context.actorOf(anyObject(),anyString())).toReturn(agentStatCollectorActor)

      agaf.getOrCreateAgentStatCollector(22, context) should be(agentStatCollectorActor)

    }

    "return a previously created agent stat collector" in {
      val agaf = new AgentActorFactory
      val context = mock[ActorContext]
      val agentStatCollectorActor = TestProbe().ref
      agaf.agStatCollectors += (22L -> agentStatCollectorActor)

      agaf.getOrCreateAgentStatCollector(22, context) should be(agentStatCollectorActor)
      verify(context, times(0)).actorOf(any[Props],anyString())

    }
  }

}
