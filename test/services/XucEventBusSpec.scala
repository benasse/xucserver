package services

import java.util.UUID

import akka.testkit.TestProbe
import akkatest.TestKitSpec
import models.CallbackList
import org.scalatest.BeforeAndAfter
import org.scalatest.mock.MockitoSugar
import services.XucEventBus.{Topic, TopicType, XucEvent}
import services.config.ConfigDispatcher.ObjectType
import services.request.{CallbackReleased, CallbackTaken}
import xivo.events.{PhoneEvent, PhoneEventType}

import scala.concurrent.duration._

class XucEventBusSpec extends TestKitSpec("XucEventBusSpec") with MockitoSugar with BeforeAndAfter {
  var eventBus: XucEventBus = null
  var actor: TestProbe = null

  before {
    eventBus = new XucEventBus
    actor = TestProbe()
  }

  "XucEventBus" should {

    "send the event to the subscriber" in {
      val topic  = XucEventBus.Topic(TopicType.Event,ObjectType.TypeAgent,None)

      val event = XucEvent(topic,"hello")

      eventBus.subscribe(actor.ref, topic)

      eventBus.publish(event)

      actor.expectMsg("hello")
    }

    "send event to all levels of channels subscribers" in {
      val allReceiver = TestProbe()
      val agentReceiver = TestProbe()

      eventBus.subscribe(allReceiver.ref, Topic(TopicType.Event, ObjectType.TypeAgent))
      eventBus.subscribe(agentReceiver.ref, Topic(TopicType.Event, ObjectType.TypeAgent, Some(54)))


      eventBus.publish(XucEvent(Topic(TopicType.Event, ObjectType.TypeAgent, Some(54)),"hello to all"))

      allReceiver.expectMsg("hello to all")
      agentReceiver.expectMsg("hello to all")
    }
    "send event to different levels of channels subscribers" in {
      val allReceiver = TestProbe()
      val agent54Receiver = TestProbe()
      val agent53Receiver = TestProbe()

      eventBus.subscribe(allReceiver.ref, Topic(TopicType.Event, ObjectType.TypeAgent))
      eventBus.subscribe(agent54Receiver.ref, Topic(TopicType.Event, ObjectType.TypeAgent, Some(54)))
      eventBus.subscribe(agent53Receiver.ref, Topic(TopicType.Event, ObjectType.TypeAgent, Some(53)))


      eventBus.publish(XucEvent(Topic(TopicType.Event, ObjectType.TypeAgent, Some(53)),"hello to ag53"))
      eventBus.publish(XucEvent(Topic(TopicType.Event, ObjectType.TypeAgent, Some(54)),"hello to ag54"))

      allReceiver.expectMsgAllOf("hello to ag53","hello to ag54")
      agent53Receiver.expectMsg("hello to ag53")
      agent53Receiver.expectNoMsg(100 millis)
      agent54Receiver.expectMsg("hello to ag54")
      agent54Receiver.expectNoMsg(100 millis)
    }
    "Do not send event to all agents id starting with the same number" in {
      val agent51Receiver = TestProbe()
      val agent510Receiver = TestProbe()

      eventBus.subscribe(agent51Receiver.ref, XucEventBus.agentEventTopic(51))
      eventBus.subscribe(agent510Receiver.ref, XucEventBus.agentEventTopic(510))


      eventBus.publish(XucEvent(XucEventBus.agentEventTopic(510),"agent 510 event"))

      agent510Receiver.expectMsg("agent 510 event")
      agent51Receiver.expectNoMsg(100 millis)

    }
    "Can subscribe to leaf topic and main topic" in {
      val agent64Receiver = TestProbe()

      eventBus.subscribe(agent64Receiver.ref, XucEventBus.configTopic(ObjectType.TypeQueue))
      eventBus.subscribe(agent64Receiver.ref, XucEventBus.agentEventTopic(64))
      eventBus.subscribe(agent64Receiver.ref, Topic(TopicType.Event, ObjectType.TypeAgent))

      eventBus.publish(XucEvent(XucEventBus.agentEventTopic(600),"agent 600 event"))

      agent64Receiver.expectMsg("agent 600 event")
    }

    "Publish phone event" in {
      val phoneEventReceiver = TestProbe()

      val event = PhoneEvent(PhoneEventType.EventRinging,"1500","0664486754","1230045.6","1230045.8")

      eventBus.subscribe(phoneEventReceiver.ref,XucEventBus.phoneEventTopic("1500"))

      eventBus.publish(event)

      phoneEventReceiver.expectMsg(event)
    }

    "Publish CallbackReleased" in {
      val receiver = TestProbe()
      eventBus.subscribe(receiver.ref,XucEventBus.callbackListsTopic())
      val event = CallbackReleased(UUID.randomUUID())

      eventBus.publish(event)

      receiver.expectMsg(event)
    }

    "Publish CallbackTaken" in {
      val receiver = TestProbe()
      eventBus.subscribe(receiver.ref,XucEventBus.callbackListsTopic())
      val event = CallbackTaken(UUID.randomUUID(), 12)

      eventBus.publish(event)

      receiver.expectMsg(event)
    }

    "Publish list of CallbackList" in {
      val receiver = TestProbe()
      val list = List(CallbackList(Some(UUID.randomUUID()), "the name", 12, List()))
      eventBus.subscribe(receiver.ref,XucEventBus.callbackListsTopic())

      eventBus.publish(list)

      receiver.expectMsg(list)
    }

  }
}
