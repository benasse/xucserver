package services.line

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.joda.time.DateTime
import org.mockito.Mockito.verify
import org.scalatest.mock.MockitoSugar
import services.config.ConfigDispatcher._
import services.config.{AgentOnPhone, OutboundQueueNumber}
import services.request.PhoneRequest._
import services.{ActorFactory, XucEventBus}
import xivo.events.AgentState.{AgentDialing, AgentLoggedOut, AgentLogin, AgentReady}
import xivo.phonedevices.DeviceAdapter

class PhoneControllerSpec extends TestKitSpec("PhoneControllerSpec") with MockitoSugar {

  class Helper() {
    val deviceAdapter= mock[DeviceAdapter]
    val eventBus = mock[XucEventBus]
    val lineNumber = "1000"
    val configDispatcher = TestProbe()

    def actor() = {
      val res = TestActorRef(new PhoneController(lineNumber, deviceAdapter, eventBus) with TestActorFactory)
      (res, res.underlyingActor)
    }

    trait TestActorFactory extends ActorFactory {
      override val configDispatcherURI = configDispatcher.ref.path.toString
    }
  }

  "PhoneController actor" should {
    "subscribe to agent state events and ask for current agent state when receiving Start" in new Helper {
      val (ref, a) = actor()

      ref ! PhoneController.Start

      verify(eventBus).subscribe(ref, XucEventBus.allAgentsEventsTopic)
      configDispatcher.expectMsg(RequestConfig(ref, GetAgentOnPhone(lineNumber)))
    }

    "save the agent id and ask for outbound queue when receiving an AgentLogin on the current line" in new Helper {
      val (ref, a) = actor()
      val queues = List(7, 9, 6)

      ref ! AgentLogin(12, new DateTime, lineNumber, queues)

      configDispatcher.expectMsg(RequestConfig(ref, OutboundQueueNumberQuery(queues)))
      a.agentId shouldEqual Some(12)
    }

    "do nothing if the received message is not for the good line" in new Helper {
      val (ref, a) = actor()
      val queues = List(7, 9, 6)

      ref ! AgentLogin(12, new DateTime, lineNumber + "1", queues)

      configDispatcher.expectNoMsg()
      a.agentId shouldEqual None
    }

    "save the queue number when receiving an OutboundQueueNumber" in new Helper {
      val (ref, a) = actor()
      ref ! OutboundQueueNumber("3000")
      a.queueNumber shouldEqual Some("3000")
    }

    "unset the agentId and the queueNumber when receiving AgentLogout" in new Helper {
      val (ref, a) = actor()
      a.agentId = Some(12)
      a.queueNumber = Some("3000")

      ref ! AgentLoggedOut(12, new DateTime, lineNumber, List())

      a.agentId shouldEqual None
      a.queueNumber shouldEqual None
    }

    "do noting if the AgentLoggedOut is not for the current line" in new Helper {
      val (ref, a) = actor()
      a.agentId = Some(12)
      a.queueNumber = Some("3000")

      ref ! AgentLoggedOut(12, new DateTime, lineNumber + "1", List())

      a.agentId shouldEqual Some(12)
      a.queueNumber shouldEqual Some("3000")
    }

    "set the agentId and ask for the agent queues and the agent state when receiving AgentOnPhone" in new Helper {
      val (ref, a) = actor()

      ref ! AgentOnPhone(12, lineNumber)

      a.agentId shouldEqual Some(12)
      configDispatcher.expectMsg(RequestConfig(ref, GetQueuesForAgent(12)))
      configDispatcher.expectMsg(RequestStatus(ref, 12, ObjectType.TypeAgent))
    }

    "ask for outbound queue when receiving QueuesForAgent" in new Helper {
      val (ref, a) = actor()

      ref ! QueuesForAgent(List(3, 4), 12)

      configDispatcher.expectMsg(RequestConfig(ref, OutboundQueueNumberQuery(List(3, 4))))
    }

    "enable outbound dial and ask for the outbound queue when receiving AgentReady" in new Helper {
      val (ref, a) = actor()

      ref ! AgentReady(12, new DateTime, lineNumber, List(3, 4))

      a.enableOutboundDial should be(true)
      configDispatcher.expectMsg(RequestConfig(ref, OutboundQueueNumberQuery(List(3, 4))))
    }

    "not enable outbound dial if the AgentReady is not for us" in new Helper {
      val (ref, a) = actor()
      ref ! AgentReady(12, new DateTime, lineNumber + "1", List())
      a.enableOutboundDial should be(false)
    }

    "disable outbound dial when receiving another agent state" in new Helper {
      val (ref, a) = actor()
      a.enableOutboundDial = true

      ref ! AgentDialing(12, new DateTime, lineNumber, List())

      a.enableOutboundDial should be(false)
    }

    "not disable outbound dial if the agent state is not for us" in new Helper {
      val (ref, a) = actor()
      a.enableOutboundDial = true

      ref ! AgentDialing(12, new DateTime, lineNumber + "1", List())

      a.enableOutboundDial should be(true)
    }

    "do an outbound dial if the agentId and the queue number are set, and outbound dial is enabled" in new Helper {
      val (ref, a) = actor()
      a.enableOutboundDial = true
      a.agentId = Some(12)
      a.queueNumber = Some("3000")

      ref ! Dial("335847", Map("VARIABLE" -> "Value"))

      verify(deviceAdapter).odial("335847", 12, "3000", Map("VARIABLE" -> "Value"), self)
    }

    "do a standard dial otherwise" in new Helper {
      val (ref, a) = actor()
      ref ! Dial("335847", Map("VARIABLE" -> "Value"))
      verify(deviceAdapter).dial("335847", Map("VARIABLE" -> "Value"), self)
    }

    "hangup the call" in new Helper {
      val (ref, a) = actor()
      ref ! Hangup
      verify(deviceAdapter).hangup(self)
    }

    "do an attended transfer" in new Helper {
      val (ref, a) = actor()
      ref ! AttendedTransfer("1000")
      verify(deviceAdapter).attendedTransfer("1000", self)
    }

    "complete the transfer" in new Helper {
      val (ref, a) = actor()
      ref ! CompleteTransfer
      verify(deviceAdapter).completeTransfer(self)
    }

    "cancel the transfer" in new Helper {
      val (ref, a) = actor()
      ref ! CancelTransfer
      verify(deviceAdapter).cancelTransfer(self)
    }

    "do a conference" in new Helper {
      val (ref, a) = actor()
      ref ! Conference
      verify(deviceAdapter).conference(self)
    }

    "hold the call" in new Helper {
      val (ref, a) = actor()
      ref ! Hold
      verify(deviceAdapter).hold(self)
    }

    "answer the call" in new Helper {
      val (ref, a) = actor()
      ref ! Answer
      verify(deviceAdapter).answer(self)
    }

  }
}
