package services

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.XucUser
import org.joda.time.DateTime
import org.json.JSONObject
import org.mockito.Mockito.{stub, verify}
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message._
import org.xivo.cti.model.{PhoneHintStatus, QueueStatRequest, UserStatus}
import play.libs.Json
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import services.config.ConfigDispatcher.ObjectType._
import services.config.ConfigDispatcher._
import services.config.LineConfig
import services.line.LineState
import services.request._
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped, CallData, LineEvent}
import xivo.events.AgentState.{AgentDialing, AgentLoggedOut, AgentOnWrapup, AgentReady}
import xivo.events.{PhoneEventType, PhoneEvent}
import xivo.models.XivoObject.ObjectDefinition
import xivo.network.LoggedOn
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}

import scala.collection.JavaConversions.seqAsJavaList
import scala.collection.JavaConverters.asScalaIteratorConverter
import scala.concurrent.duration.DurationInt

class CtiFilterSpec
  extends TestKitSpec("CtiFilterSpec") with MockitoSugar {

  class Helper {
    val messageFactory = new MessageFactory()
    val eventBus = mock[XucEventBus]
    type ToBrowserMessage = com.fasterxml.jackson.databind.node.ObjectNode
    val ctiRouter = TestProbe()
    val agentActionService = TestProbe()


    def actor(user: Option[XucUser] = None, filterConfig: Option[FilterConfig] = None) = {
      val a = TestActorRef(new CtiFilter(eventBus, ctiRouter.ref))
      user.foreach(a.underlyingActor.user = _)
      filterConfig.foreach(a.underlyingActor.filterConfig = _)
      (a, a.underlyingActor)
    }

    def createUserConfigUpdate(userId: Int, agentId: Int, phoneId: Int, voiceMailConfig: VoiceMailConfig): UserConfigUpdate = {
      val userConfigUpdate = new UserConfigUpdate
      userConfigUpdate.setAgentId(agentId)
      userConfigUpdate.setUserId(userId)
      userConfigUpdate.addLineId(phoneId)
      userConfigUpdate.setVoiceMailId(voiceMailConfig.id)
      userConfigUpdate.setVoiceMailEnabled(voiceMailConfig.enabled)
      userConfigUpdate
    }

    def createPhoneStatusMessageForMe(phoneId: String, status: String) = {
      val phoneStatusUpdate = new PhoneStatusUpdate()
      phoneStatusUpdate.setLineId(Integer.parseInt(phoneId))
      phoneStatusUpdate.setHintStatus(PhoneHintStatus.valueOf(status).getHintStatus.toString)
      phoneStatusUpdate
    }

    def createPhoneConfigUpdateMessageForMe(phoneId: String, phoneNumber: String) = {
      val phoneConfigUpdate = new PhoneConfigUpdate()
      phoneConfigUpdate.setNumber(phoneNumber)
      phoneConfigUpdate.setId(Integer.parseInt(phoneId))
      phoneConfigUpdate
    }

    def expectToBrowserMessage(msgType: String, payLoad: Any) = {
      val toBrowserMessage = createToBrowserMessage(msgType, payLoad)
      expectMsg(toBrowserMessage)
    }

    def createToBrowserMessage(msgType: String, payLoad: Any): ToBrowserMessage = {
      val toBrowserMessage = Json.newObject()
      toBrowserMessage.put("msgType", msgType)
      toBrowserMessage.set("ctiMessage", Json.toJson(payLoad))
      toBrowserMessage
    }

    def expectResponseConformToCtiRequest(expectedResponse: JSONObject): Unit =
      expectConformToCtiRequest(expectMsgClass(classOf[JSONObject]), expectedResponse)

    def expectConformToCtiRequest(message: JSONObject, expectedResponse: JSONObject): Unit = {
      val keys: List[String] = message.keys().asScala.collect {
        case s: String if s != "commandid" => s
      }.toList

      keys.foreach(key => message.get(key).toString should be(expectedResponse.get(key).toString))
    }

    def createBrowserMessage(command: String, properties: Map[String, Any] = Map()): BrowserMessage = {
      val message = Json.newObject()
      message.put("claz", "web")
      message.put("command", command)
      for ((key, value) <- properties) {
        (key, value) match {
          case (key: String, value: String) => message.put(key, value)
          case (key: String, value: Boolean) => message.put(key, value)
          case (key: String, value: Integer) => message.put(key, value)
          case _ => throw new Exception("cannot put property value")
        }
      }
      BrowserMessage(message)
    }
  }

  "A CtiFilter actor" should {

    """upon reception of LoggedOn:
    - save userId
    - save user statuses and send it to web socket
    - send evtLoggedOn,
    - send GetUserConfig request to the CTI server
    - Request user status
    - Request voicemail status""" in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeUser

      val user = XucUser("bob", "pwd")
      val userId = 5
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withUserId(userId).withVoiceMailConfig(VoiceMailConfig.enabled(33))))
      val myMsgFactory = mock[MessageFactory]
      ctiFilter.messageFactory = myMsgFactory

      val userStatuses = java.util.Arrays.asList(new UserStatus("available"), new UserStatus("disconnected"))
      val wsUserStatuses = WebSocketEvent.createEvent(userStatuses)
      val wsLoggedOn = Json.parse("{\"msgType\": \"LoggedOn\"}")
      val requestUserConfig = messageFactory.createGetUserConfig(userId.toString)
      val requestVoiceMailStatus = messageFactory.createGetVoicemailStatus("33")
      stub(myMsgFactory.createGetUserConfig(userId.toString)).toReturn(requestUserConfig)
      stub(myMsgFactory.createGetVoicemailStatus("33")).toReturn(requestVoiceMailStatus)
      val requestUserStatus = RequestStatus(ref, 5, TypeUser)

      ref ! LoggedOn(user, userId.toString, userStatuses)

      expectMsgAllOf(wsLoggedOn, requestUserConfig, wsUserStatuses, requestUserStatus, requestVoiceMailStatus)

      ctiFilter.filterConfig.userId should be(Some(userId))
    }

    """upon reception of clientconnected :
      | send evtLoggedOn
      | send user statuses to web socket
      | Request user status
      | Request agent status
      | Request phone status
      | send GetUserConfig request to the CTI server
      | Request voicemail status
    """ in new Helper {
        val userId = 896
        val agentId = 741
        val phoneId = 623
        val user = XucUser("testFilterUser", "pwd", Some(2350))
        val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withAgentId(agentId).withUserId(userId).withPhoneId(phoneId).withVoiceMailConfig(VoiceMailConfig.enabled(55))))
        val connectedActor = TestProbe()
        val myMsgFactory = mock[MessageFactory]
        ctiFilter.messageFactory = myMsgFactory

        val wsLoggedOn = Json.parse("{\"msgType\": \"LoggedOn\"}")
        val userStatuses = java.util.Arrays.asList(new UserStatus("available"), new UserStatus("disconnected"))
        val wsUserStatuses = WebSocketEvent.createEvent(userStatuses)
        val requestUserStatus = RequestStatus(ref, userId, TypeUser)
        val requestPhoneStatus = RequestStatus(ref, phoneId, TypePhone)
        val requestAgentStatus = RequestStatus(ref, agentId, TypeAgent)
        val requestUserConfig = messageFactory.createGetUserConfig(userId.toString)
        val requestVoiceMailStatus = messageFactory.createGetVoicemailStatus("55")
        stub(myMsgFactory.createGetUserConfig(userId.toString)).toReturn(requestUserConfig)
        stub(myMsgFactory.createGetVoicemailStatus("55")).toReturn(requestVoiceMailStatus)

        ref ! ClientConnected(connectedActor.ref, LoggedOn(user, userId.toString, userStatuses))

        connectedActor.expectMsgAllOf(WsContent(wsLoggedOn),  WsContent(wsUserStatuses))
        expectMsgAllOf(requestPhoneStatus,requestAgentStatus)
        ctiRouter.expectMsgAllOf(requestUserConfig,requestUserStatus,requestVoiceMailStatus)

    }
    """upon reception of UserStatusUpdate
      |- send evtUserStatusUpdate""" in new Helper {
      val userId = 55
      val status = "dnd"
      val user = XucUser("testFilterUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withUserId(userId)))

      val userStatusUpdate = new UserStatusUpdate()
      userStatusUpdate.setUserId(userId)
      userStatusUpdate.setStatus(status)

      ref ! userStatusUpdate
      val expected = Json.newObject()
      val jsonStatusNode = Json.newObject()
      jsonStatusNode.put("status", status)
      expected.put("msgType", "UserStatusUpdate")
      expected.set("ctiMessage", jsonStatusNode)

      ctiRouter.expectMsg(expected)
    }

    """upon reception of voicemail update send if back to router """.stripMargin in new Helper {
      val user = XucUser("testvoicemail update", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withVoiceMailConfig(VoiceMailConfig.enabled(56))))

      var voiceMailStatusUpdate = new VoiceMailStatusUpdate()
      voiceMailStatusUpdate.setVoiceMailId(56)

      ref ! voiceMailStatusUpdate

      ctiRouter.expectMsg(WebSocketEvent.createEvent(voiceMailStatusUpdate))
    }

    "When voicemail status update is not mine do nothing" in new Helper {
      val (ref, ctiFilter) = actor(Some(XucUser("NotMyVMUpdate","***")), Some(FilterConfig().withVoiceMailConfig(VoiceMailConfig.enabled(59))))

      var voiceMailStatusUpdate = new VoiceMailStatusUpdate()
      voiceMailStatusUpdate.setVoiceMailId(11)

      ref ! voiceMailStatusUpdate

      ctiRouter.expectNoMsg(100 millis)

    }

    """upon reception of UserUpdateConfig:
      - save agentId,
      - save phoneId,
      - save voicemailData,
      - request line configuration by id,
      - request last agent status
      - unsubscribe to agent events
      - subcribe to agent events for this id
      - publish user config update
      - request voicemail status
      """ in new Helper {
      import services.config.ConfigDispatcher.ObjectType.TypeAgent
      val userId = 55

      val user = XucUser("testFilterUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withUserId(userId)))
      val myMsgFactory = mock[MessageFactory]
      ctiFilter.messageFactory = myMsgFactory


      val agentId = 41
      val phoneId = 3
      val voiceMailId = 21
      val voiceMailConfig = VoiceMailConfig.enabled(voiceMailId)

      val userConfigUpdate = createUserConfigUpdate(userId, agentId, phoneId, voiceMailConfig)
      val jsonUserConfigUpdate = WebSocketEvent.createEvent(userConfigUpdate)

      val requestVoiceMailStatus = messageFactory.createGetVoicemailStatus(voiceMailId.toString)
      stub(myMsgFactory.createGetVoicemailStatus(voiceMailId.toString)).toReturn(requestVoiceMailStatus)


      ref ! userConfigUpdate

      ctiFilter.filterConfig.phoneId should be(Some(phoneId))
      ctiFilter.filterConfig.agentId should be(Some(agentId))
      ctiFilter.filterConfig.userId should be(Some(userId))
      ctiFilter.filterConfig.voiceMailConfig should be(voiceMailConfig)

      ctiRouter.expectMsgAllOf(RequestConfig(ref, LineConfigQueryById(phoneId)), RequestStatus(ref, agentId, TypeAgent),jsonUserConfigUpdate, requestVoiceMailStatus)

      val agentTopic = XucEventBus.agentEventTopic(agentId)
      verify(eventBus).unsubscribe(ref)
      verify(eventBus).subscribe(ref, agentTopic)
    }

    "do not update agentId if not present  (0) in UserUpdateConfig" in new Helper {
      val agentId = 127
      val userId = 55
      val user = XucUser("testFilterUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user),Some(FilterConfig().withAgentId(agentId).withUserId(userId)))

      val userConfigUpdate = createUserConfigUpdate(userId, 0, 55, VoiceMailConfig.disabled(0))

      ref ! userConfigUpdate

      expectNoMsg(100 millis)
      ctiFilter.filterConfig.agentId should be(Some(agentId))
    }

    "send PhoneStatusChanged when phone status update received" in new Helper {
      val user = XucUser("testPhoneStatusChanged", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withPhoneId(3)))

      val phoneStatusUpdate = createPhoneStatusMessageForMe("3", "RINGING")
      val expPhoneStatus = WebSocketEvent.createEvent(PhoneHintStatus.valueOf("RINGING"))

      ref ! phoneStatusUpdate

      ctiRouter.expectMsg(expPhoneStatus)
    }

    "send  sheet message on received" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))

      val sheet = new Sheet()
      val expSheet = WebSocketEvent.createEvent(sheet)

      ref ! sheet

      ctiRouter.expectMsgAllOf(expSheet)
    }

    "request AgentLogout with user agent id upon receipt of agentLogout from browser without agent id" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004) )
      val (ref, ctiFilter) = actor(Some(user),Some(FilterConfig().withAgentId(47)))
      val message = createBrowserMessage("agentLogout")
      val expectedResponse = messageFactory.createAgentLogout("47")

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request AgentLogout upon receipt of agentLogout from browser" in new Helper {
      val agentId = 72
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user),Some(FilterConfig().withAgentId(agentId)))
      val message = createBrowserMessage("agentLogout", Map("agentid" -> agentId.toString))
      val expectedResponse = messageFactory.createAgentLogout(agentId.toString)

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request pause agent using User agent id upon receipt of pause agent from browser without agent id" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user),Some(FilterConfig().withAgentId(78)))
      val message = createBrowserMessage("pauseAgent")
      val expectedResponse = messageFactory.createPauseAgent("78")

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request pause agent upon receipt of pause agent from browser" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))
      val agentId = "54"
      val message = createBrowserMessage("pauseAgent", Map("agentid" -> agentId))
      val expectedResponse = messageFactory.createPauseAgent(agentId)

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request unpause agent using User agent id upon receipt of unpause agent from browser without agent id" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user),Some(FilterConfig().withAgentId(943)))
      val message = createBrowserMessage("unpauseAgent")
      val expectedResponse = messageFactory.createUnpauseAgent("943")

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request unpause agent upon receipt of unpause agent from browser" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))
      val agentId = "21"
      val message = createBrowserMessage("unpauseAgent", Map("agentid" -> agentId))
      val expectedResponse = messageFactory.createUnpauseAgent(agentId)

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request user status update upon receipt of userStatusUpdate from browser" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user),Some(FilterConfig().withUserId(456)))
      val status = "away"

      val message = createBrowserMessage("userStatusUpdate", Map("status" -> status))
      val expectedResponse = messageFactory.createUserAvailState("456", status)

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request change dnd upon receipt of dnd change request from browser" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))
      val state = true
      val message = createBrowserMessage("dnd", Map("state" -> true))
      val expectedResponse = messageFactory.createDND(state)

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request remove agent from queue on request from browser" in new Helper {
      val user = XucUser("testRemoveAgentFromQueue", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))
      val message = createBrowserMessage("removeAgentFromQueue", Map("agentId" -> 32,"queueId" -> 21))

      ref ! message

      ctiRouter.expectMsg(ConfigChangeRequest(ref,RemoveAgentFromQueue(32,21)))

    }

    "request direct transfer upon receipt of direct transfer from browser" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))
      val destination = "3567"
      val message = createBrowserMessage("directTransfer", Map("destination" -> destination))
      val expectedResponse = messageFactory.createDirectTransfer(destination)

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request get queue statistic upon receipt of websocket get queue statistics message from browser" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))
      val message = createBrowserMessage("getQueueStatistics", Map("queueId" -> 33, "window" -> 3600, "xqos" -> 60))

      val expectedResponse = messageFactory.createGetQueueStatistics(List(new QueueStatRequest("33", 3600, 60)))

      ref ! message
      expectResponseConformToCtiRequest(expectedResponse)
    }

    "request search directory receipt of search directory from browser" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))
      val pattern = "abcd"
      val message = createBrowserMessage("searchDirectory", Map("pattern" -> pattern))
      val expectedResponse = messageFactory.createSearchDirectory(pattern)

      ref ! message

      expectResponseConformToCtiRequest(expectedResponse)
    }

    """when receiveing start message:
        - reset logon state
        - save user
    """ in new Helper {

      val previousUser = XucUser("testFilterUser", "pwd", None)
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(previousUser))

      ctiFilter.receive(Start(user))

      ctiFilter.user should be(user)
      ctiFilter.user.phoneNumber should be(Some(2004))
    }

    """upon reception of lineconfig:
       - update user phoneid
       - subscribe to LineEvents
       - subscribe to phoneEvents
       - send lineconfig to the router""" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(6005))
      val (ref, ctiFilter) = actor(Some(user))

      ref ! LineConfig("23", user.phoneNumber.get.toString)

      verify(eventBus).subscribe(ref, XucEventBus.lineEventTopic(6005))
      verify(eventBus).subscribe(ref, XucEventBus.phoneEventTopic("6005"))
      ctiFilter.filterConfig.phoneId should be(Some(23))
      ctiRouter.expectMsg(LineConfig("23", user.phoneNumber.get.toString))
    }

    "send queue statistic message on receive statistic message with one stat from stat event bus" in new Helper {
      import xivo.models.XivoObject.ObjectType._

      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))

      val queueId = 5
      val objDef = ObjectDefinition(Queue, Some(queueId))
      val aggregatedStatEvent = AggregatedStatEvent(objDef, List[Stat](Stat("totalnumber", 32)))

      ref ! aggregatedStatEvent

      ctiRouter.expectMsg(
        Json.parse("{\"msgType\": \"QueueStatistics\",\"ctiMessage\":{\"queueId\":" + queueId + ",\"counters\":[{\"statName\":\"totalnumber\",\"value\":32.0}]}}"))
    }

    "send queue statistic message on receive statistic message with more stats from stat event bus" in new Helper {
      import xivo.models.XivoObject.ObjectType._

      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, ctiFilter) = actor(Some(user))

      val queueId = 4
      val objDef = ObjectDefinition(Queue, Some(queueId))
      val aggregatedStatEvent = AggregatedStatEvent(objDef, List[Stat](Stat("totalnumber", 32), Stat("totaltime", 30)))

      ref ! aggregatedStatEvent

      ctiRouter.expectMsg(
        Json.parse("{\"msgType\": \"QueueStatistics\",\"ctiMessage\":{\"queueId\":" + queueId + ",\"counters\":" +
          "[{\"statName\":\"totalnumber\",\"value\":32.0}, {\"statName\":\"totaltime\",\"value\":30.0}]}}"))
    }

    "On agent login request without id, send back a login request with user agent id" in new Helper {
      val agtId=98
      val phoneNb = "3018"
      val user = XucUser("testFilterUser", "pwd", Some(2004))
      val (ref, _) = actor(Some(user),Some(FilterConfig().withAgentId(agtId)))

      ref ! AgentLoginRequest(None, Some(phoneNb))

      ctiRouter.expectMsg(BaseRequest(ref,AgentLoginRequest(Some(agtId), Some(phoneNb))))

    }
    "on agent login request without id send an error if user is not an agent" in new Helper {
      val phoneNb = "2030"
      val user = XucUser("usernotagent", "pwd", Some(2004))
      val (ref, _) = actor(Some(user))

      ref ! AgentLoginRequest(None, Some(phoneNb))

      ctiRouter.expectMsg(WebSocketEvent.createError(WSMsgType.AgentError,"user usernotagent is not an agent"))

    }

  }
  "A ctifilter upon reception of agent ready" should {
    """ publish event to router
        request line config
    """ in new Helper {
      val agentId = 18
      val loggedOnPhoneNb = "2350"
      val user = XucUser("testFilterUser", "pwd", Some(loggedOnPhoneNb.toInt))
      val (ref, ctiFilter) = actor(Some(user))

      val agentState = AgentReady(agentId, new DateTime(), loggedOnPhoneNb, List(32, 45, 54))

      val jsonAgentState = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsgAllOf(RequestConfig(ref, LineConfigQueryByNb(loggedOnPhoneNb)), jsonAgentState)
    }
    """ publish event to router
        and NOT request line config if already done
    """ in new Helper {
      val agentId = 18
      val loggedOnPhoneNb = "2350"
      val user = XucUser("testFilterUser", "pwd", Some(loggedOnPhoneNb.toInt))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withPhoneId(321)))

      val agentState = AgentReady(agentId, new DateTime(), loggedOnPhoneNb, List(32, 45, 54))

      val jsonAgentState = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsg(jsonAgentState)

    }
    """ if phone number requested is not the one the agent is logged on
        send error
        and publish event to router
    """ in new Helper {
      def loggedInOnAnotherPhoneMsg(phoneNb: String) = Json.parse("{\"msgType\": \"AgentError\",\"ctiMessage\":{\"Error\":\"LoggedInOnAnotherPhone\",\"phoneNb\":\"" + phoneNb + "\",\"RequestedNb\":\"2350\"}}")

      val user = XucUser("testFilterUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withPhoneId(321)))

      val agentState = AgentReady(18, new DateTime(), "4460", List(32, 45, 54))

      val jsonAgentState = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsgAllOf(loggedInOnAnotherPhoneMsg("4460"), jsonAgentState)
    }


  }
  "A CtiFilter upon reception off agent logout" should {
    "reset phoneId from filter config and unsubscribe from PhoneEvents" in new Helper {
      val (ref, ctiFilter) = actor(Some(XucUser("testFilterUser", "pwd", Some(2350))), Some(FilterConfig().withPhoneId(321)))

      ref ! AgentLoggedOut(52, new DateTime, "2350", List())

      ctiFilter.filterConfig.phoneId should be(None)
      verify(eventBus).unsubscribe(ref, XucEventBus.phoneEventTopic("2350"))
    }
  }
  "A CtiFilter upon reception on agent state" should {
    import xivo.events.AgentState.{AgentLoggedOut, AgentLogin}
    """ publish event to router
        request line config
    """ in new Helper {
      val agentId = 18
      val loggedOnPhoneNb = "2350"
      val user = XucUser("testFilterUser", "pwd", Some(loggedOnPhoneNb.toInt))
      val (ref, ctiFilter) = actor(Some(user))
      val agentState = AgentOnWrapup(agentId, new DateTime(), loggedOnPhoneNb, List())

      val exp = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsgAllOf(RequestConfig(ref, LineConfigQueryByNb(loggedOnPhoneNb)), exp)

    }
    """ publish event to router
        and NOT request line config if already done
    """ in new Helper {
      val agentId = 18
      val loggedOnPhoneNb = "2440"
      val user = XucUser("testAgentState", "pwd", Some(loggedOnPhoneNb.toInt))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withPhoneId(321)))

      val agentState = AgentOnWrapup(agentId, new DateTime(), loggedOnPhoneNb, List())

      val exp = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsgAllOf(exp)
    }
    "publish event to router and do not request line config if event is agent loggedout" in new Helper {
      val agentId = 18
      val loggedOnPhoneNb = "2440"
      val user = XucUser("testAgentState", "pwd", Some(loggedOnPhoneNb.toInt))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withNoPhoneId))

      val agentState = AgentLoggedOut(agentId, new DateTime(), loggedOnPhoneNb, List())

      val exp = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsg(exp)
      ctiRouter.expectNoMsg()
      ctiFilter.filterConfig.phoneId should be(None)

    }
    """ if phone number requested is not the one the agent is logged on
        send error
        and publish event to router
    """ in new Helper {
      def loggedInOnAnotherPhoneMsg(phoneNb: String) = Json.parse("{\"msgType\": \"AgentError\",\"ctiMessage\":{\"Error\":\"LoggedInOnAnotherPhone\",\"phoneNb\":\"" + phoneNb + "\",\"RequestedNb\":\"2350\"}}")

      val user = XucUser("testFilterUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withPhoneId(321)))

      val agentState = AgentOnWrapup(18, new DateTime(), "4460", List())

      val jsonAgentState = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsgAllOf(loggedInOnAnotherPhoneMsg("4460"), jsonAgentState)
    }
    """ if phone number requested is not the one the agent is logged on
        do not send error if agentStatereceived is loggedOut
        and publish event to router
    """ in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user))

      val agentState = AgentLoggedOut(18, new DateTime(), "4460", List())

      val jsonAgentState = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsg(jsonAgentState)
    }
    """ on agent login event 
        - request line config
        - publish AgentLogin to the router""" in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(4350))
      val (ref, ctiFilter) = actor(Some(user))
      val agentLoggedIn = AgentLogin(32, new DateTime(), "4350", List(32, 45, 54))
      val jsonAgentLoggedIn = WebSocketEvent.createEvent(agentLoggedIn)

      ref ! agentLoggedIn

      ctiRouter.expectMsgAllOf(RequestConfig(ref, LineConfigQueryByNb("4350")), jsonAgentLoggedIn)
    }

    """ on line event
        - publish event to the router""" in new Helper() {
      val user = XucUser("testFilterUser", "pwd", Some(3350))
      val (ref, ctiFilter) = actor(Some(user))

      val lineEvent = LineEvent(33, CallData("callId", "linkedId", LineState.BUSY, "33"))
      val jsonLineEvent = WebSocketEvent.createEvent(lineEvent)

      ref ! lineEvent
      ctiRouter.expectMsg(jsonLineEvent)
    }
  }
    "A CtiFilter on agent listen" should {
    "on started publish event to router" in new Helper() {
      val (ref, ctiFilter) = actor(Some(XucUser("testlistenstarted", "pwd", Some(4410))))

      val agls = AgentListenStarted("4410",Some(54))
      val jsonEvent = WebSocketEvent.createEvent(agls)

      ref ! agls

      ctiRouter.expectMsg(jsonEvent)
    }

    "on stopped publish event to router" in new Helper() {
      val (ref, ctiFilter) = actor(Some(XucUser("testlistenstopped", "pwd", Some(1540))))

      val listenStopped = AgentListenStopped("1540",Some(25))
      val jsonEvent = WebSocketEvent.createEvent(listenStopped)

      ref ! listenStopped

      ctiRouter.expectMsg(jsonEvent)

    }

    """on request, update with my agentid and forward to agent action""" in new Helper {
      val (ref, ctiFilter) = actor(Some( XucUser("testagentlisten", "pwd", Some(3350))))
      ctiFilter.filterConfig = FilterConfig() withUserId(78)

      ref ! AgentListen(56)

      ctiRouter.expectMsg(AgentListen(56, Some(78)))

    }

    """do nothing if not an agent""" in new Helper {
      val (ref, ctiFilter) = actor(Some( XucUser("testagentlisten", "pwd", Some(3350))))

      ref ! AgentListen(56)

      ctiRouter.expectMsg(InvalidRequest("Cannot listen valid userid not found", s"${AgentListen(56)}"))

    }

    """ if the phone number in the agent state is empty
      do not send error
      and publish event to router
    """ in new Helper {
      val user = XucUser("testFilterUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig().withPhoneId(321)))

      val agentState = AgentDialing(18, new DateTime(), "", List(32, 45, 54))

      val jsonAgentState = WebSocketEvent.createEvent(agentState)

      ref ! agentState

      ctiRouter.expectMsg(jsonAgentState)
    }
    }
  "A cti filter on phone event" should {
    "publish the event" in new Helper{
      val user = XucUser("testPhoneEventUser", "pwd", Some(2350))
      val (ref, ctiFilter) = actor(Some(user), Some(FilterConfig()))

      val phoneEvent = PhoneEvent(PhoneEventType.EventRinging,"1500","0664486754","1230045.6","1230045.8")

      val jsonPhoneEvent = WebSocketEvent.createEvent(phoneEvent)

      ref ! phoneEvent

      ctiRouter.expectMsg(jsonPhoneEvent)
    }
  }

}
