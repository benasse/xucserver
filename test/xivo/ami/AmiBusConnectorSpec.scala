package xivo.ami

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.joda.time.DateTime
import org.mockito.Mockito.{stub, verify}
import org.scalatest.mock.MockitoSugar
import services.XucAmiBus
import services.XucAmiBus._
import services.config.ConfigRepository
import services.line.LineState
import services.request.{KeyLightRequest, MonitorPause, MonitorUnpause}
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped, CallData, LineEvent}
import xivo.events.AgentState.AgentLogin
import xivo.models.Agent
import xivo.phonedevices.OutboundDial
import xivo.xucami.models._
import xivo.xucami.userevents.UserEventAgentLogin

import scala.collection.immutable.HashMap

class AmiBusConnectorSpec extends TestKitSpec("AmiBusConnectorSpec")  with MockitoSugar {

  class Helper {
    val configRepository = mock[ConfigRepository]
    val agentManager = TestProbe()
    val lineManager = TestProbe()
    val configDispatcher = TestProbe()
    val amiBus = mock[XucAmiBus]

    def actor() = {
      val a = TestActorRef(new AmiBusConnector(configRepository, agentManager.ref, lineManager.ref, configDispatcher.ref, amiBus))
      (a, a.underlyingActor)

    }
  }

  "AmiBusConnector" should {
    "subscribe to the amiBus for ChannelEvents, UserEventAgent and QueueEvents" in new Helper {
      val (ref, _) = actor()
      verify(amiBus).subscribe(ref, AmiType.ChannelEvent)
      verify(amiBus).subscribe(ref, AmiType.UserEventAgent)
      verify(amiBus).subscribe(ref, AmiType.QueueEvent)
    }

    "completeChannel with agentId and forward it to the agentManager" in new Helper {
      val (ref, _) = actor()
      val agent325 = Agent(123, "thomas", "legrand", "325", "default")
      stub(configRepository.getAgent("325")).toReturn(Some(agent325))
      var channel = new Channel(
        "123", "SIP/eert", CallerId("unknownCalled", "0123456"), "123", agentNumber = Some("325"), monitored = MonitorState.ACTIVE)

      ref ! ChannelEvent(channel)

      verify(configRepository).getAgent("325")
      agentManager.expectMsg(AgentCallUpdate(123, MonitorState.ACTIVE))
    }

    "send a line event to the lineManager on new channelEvent using calleridnum " in new Helper {
      val (ref, _) = actor()
      var channel = Channel("1432536873.13", "SIP/eert", CallerId("unknownCalled", "0123456"),"1432536873.12", ChannelState.ORIGINATING, variables = HashMap("var"->"value"))

      ref ! ChannelEvent(channel)

      lineManager.expectMsg(LineEvent(123456, CallData("1432536873.12","1432536873.13", LineState.ORIGINATING, "0123456", variables=HashMap("var"->"value"))))
    }

    "send a line event to the lineManager on new channelEvent using connectedLineNum" in new Helper {
      val (ref, _) = actor()
      var channel = Channel("1432536873.13", "SIP/eert", CallerId("unknownCalled", "0123456"), "1432536873.12", ChannelState.ORIGINATING, connectedLineNb=Some("44500"),
        variables = HashMap("var"->"value"))

      ref ! ChannelEvent(channel)

      lineManager.expectMsgAllOf(LineEvent(123456, CallData(channel)),LineEvent(44500, CallData(channel)))
    }

    "on MonitorPause replace agentId by agentNumber and publish it on the amiBus" in new Helper() {
      val (ref, _) = actor()

      val id: Agent.Id = 233
      val agent233 = Agent(id, "carl", "deloin", "443", "default")
      stub(configRepository.getAgent(id)).toReturn(Some(agent233))

      ref ! MonitorPause(233)

      verify(configRepository).getAgent(id)
      verify(amiBus).publish(ChannelRequest(ChannelActionRequest(Some("443"), ChannelAction.PauseMonitor)))
    }

    "on MonitorUnpause replace agentId by agentNumber and publish it on the amiBus" in new Helper() {
      val (ref, _) = actor()

      val id: Agent.Id = 244
      val agent244 = Agent(id, "carl", "deloin", "553", "default")
      stub(configRepository.getAgent(id)).toReturn(Some(agent244))

      ref ! MonitorUnpause(244)

      verify(configRepository).getAgent(id)
      verify(amiBus).publish(ChannelRequest(ChannelActionRequest(Some("553"), ChannelAction.UnpauseMonitor)))
    }

    "Create set var action on key light request" in new Helper {
      val (ref, _) = actor()

      val keyLightRequest = mock[KeyLightRequest]
      val amiRequest = mock[AmiMessage]
      stub(keyLightRequest.toAmi).toReturn(amiRequest)

      ref ! keyLightRequest

      verify(amiBus).publish(amiRequest)

    }
    "forward Spy started to line manager" in new Helper {
      val (ref,_) = actor()
      val listenerId = 789
      val listenedId = 451
      val spyStarted = SpyStarted(SpyChannels(Channel("1","b",CallerId("a","2000"),"1"),Channel("2","b",CallerId("a","3000"),"1")))

      stub(configRepository.getAgentLoggedOnPhoneNumber("3000")).toReturn(Some(listenedId))

      ref ! spyStarted

      lineManager.expectMsg(AgentListenStarted("3000", Some(listenedId)))
    }
    "forward Spy Stopped to line manager" in new Helper {
      val (ref,_) = actor()
      val listenedId = 213

      val spyStopped = SpyStopped(Channel("1","n", CallerId("u","5000"),"1"))
      stub(configRepository.getAgentLoggedOnPhoneNumber("5000")).toReturn(Some(listenedId))

      ref ! spyStopped

      lineManager.expectMsg(AgentListenStopped("5000",Some(listenedId)))

    }
    "on ami AmiUserEventAgent with agent login send agent state to agent manager" in new Helper {
      val (ref,_) = actor()

      val userEventAgentLogin = new UserEventAgentLogin("test")
      userEventAgentLogin.setAgentId("18")

      ref ! AmiUserEventAgent(userEventAgentLogin)

      agentManager.expectMsg(AgentLogin(userEventAgentLogin))

    }
    "publish ami action request" in new Helper {
      val (ref,_) = actor()
      val requestToAmi = OutboundDial("0298143388", 85, "3000",Map("VAR" -> "Value"), "10.20.10.3")

      ref ! requestToAmi

      val expected = AmiRequest(OutBoundDialActionRequest("0298143388","select_agent(agent=agent_85)","3000",Map("VAR" -> "Value"),"10.20.10.3"))
      verify(amiBus).publish(expected)
    }

    "forward EnterQueue and LeaveQueue events to the config manager" in new Helper {
      val enterQueue = EnterQueue("foo", "123456.789", QueueCall(0, "bar", "3331545", new DateTime()))
      val leaveQueue = LeaveQueue("foo", "123456.789")
      val (ref, _) = actor()

      ref ! enterQueue
      configDispatcher.expectMsg(enterQueue)

      ref ! leaveQueue
      configDispatcher.expectMsg(leaveQueue)
    }
  }
}
