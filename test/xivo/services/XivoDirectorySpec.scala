package xivo.services

import akka.actor.{Actor, Props}
import akka.testkit.{TestProbe, TestActorRef}
import akkatest.TestKitSpec
import models.{XucUser, XivoUser, DirSearchResult, Token}
import org.joda.time.DateTime
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito.when
import org.scalatestplus.play.OneAppPerSuite
import play.api.libs.json.{JsString, Json}
import play.api.libs.ws.{WSResponse, WSRequestHolder}
import play.api.test.FakeApplication
import play.api.test.Helpers._
import services.directory.DirectoryTransformer
import DirectoryTransformer.RawDirectoryResult
import services.config.ConfigRepository
import services.request._
import xivo.network.XiVOWSdef
import xivo.services.XivoAuthentication.GetToken
import xivo.services.XivoDirectory._

import scala.concurrent.Future

class XivoDirectorySpec extends TestKitSpec("XivoDirectorySpec")  with MockitoSugar with OneAppPerSuite{
  val appliConfig: Map[String, Any] = {
    Map(
      "xivohost" -> "xivoIP",
      "xivoAuthPort" -> 9497
    )
  }
  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = appliConfig
    )

  "XivoDirectory actor " should {
    "get token from xivoAuth and return search result for a given term" in new Helper {
        val (ref, _) = actor()
        val result = mock[DirSearchResult]
        val wsRequest = mock[WSRequestHolder]
        when(xivoWS.get("xivoIP", "0.1/directories/lookup/default?term=ben", headers=Map("X-Auth-Token"-> token),
          port=Some(9489))).thenReturn(wsRequest)
        val dirdResult = Json.parse(searchResponse)
        val response = mock[WSResponse]
        when(response.json).thenReturn(dirdResult)
        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! UserBaseRequest(requester.ref, DirectoryLookUp("ben"), user)
        directoryTransformer.expectMsg(
          RawDirectoryResult(requester.ref, DirLookupResult(DirSearchResult.parse(dirdResult)))
        )
    }

    "get token from xivoAuth and return search result for favorites" in new Helper {
        val (ref, _) = actor()
        val result = mock[DirSearchResult]
        val wsRequest = mock[WSRequestHolder]
        when(xivoWS.get("xivoIP", "0.1/directories/favorites/default", headers=Map("X-Auth-Token"-> token),
          port=Some(9489))).thenReturn(wsRequest)
        val dirdResult = Json.parse(searchResponse)
        val response = mock[WSResponse]
        when(response.json).thenReturn(dirdResult)
        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! UserBaseRequest(requester.ref, GetFavorites, user)
        directoryTransformer.expectMsg(
          RawDirectoryResult(requester.ref, Favorites(DirSearchResult.parse(dirdResult)))
        )
    }

    "get token from xivoAuth and add contact to favorites" in new Helper {
        val (ref, _) = actor()
        val wsRequest = mock[WSRequestHolder]
        val directory = "xivo-users"
        val contactId = "123"
        when(xivoWS.put("xivoIP", s"0.1/directories/favorites/$directory/$contactId", headers=Map("X-Auth-Token"-> token),
          port=Some(9489))).thenReturn(wsRequest)
        val response = mock[WSResponse]
        when(response.status).thenReturn(204)
        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! UserBaseRequest(requester.ref, AddFavorite(contactId, directory), user)
        directoryTransformer.expectMsg(
          RawDirectoryResult(requester.ref, FavoriteUpdated(Action.Added, contactId, directory))
        )
    }

    "get token from xivoAuth and remove contact from favorites" in new Helper {
        val (ref, _) = actor()
        val wsRequest = mock[WSRequestHolder]
        val directory = "xivo-users"
        val contactId = "123"
        when(xivoWS.del("xivoIP", s"0.1/directories/favorites/$directory/$contactId", headers=Map("X-Auth-Token"-> token),
          port=Some(9489))).thenReturn(wsRequest)
        val response = mock[WSResponse]
        when(response.status).thenReturn(204)
        when(wsRequest.execute()).thenReturn(Future.successful(response))
        ref ! UserBaseRequest(requester.ref, RemoveFavorite(contactId, directory), user)
        directoryTransformer.expectMsg(
          RawDirectoryResult(requester.ref, FavoriteUpdated(Action.Removed, contactId, directory))
        )
      }
  }

  "FavoriteUpdated case class" should {
    "provide implicit json writer" in {
      val result = Json.toJson(FavoriteUpdated(Action.Added, "contactIdValue", "sourceValue"))
      result \ "action" shouldEqual JsString("Added")
      result \ "contact_id" shouldEqual JsString("contactIdValue")
      result \ "source" shouldEqual JsString("sourceValue")
    }
  }

  class Helper {
    val appliConfig: Map[String, Any] = {
      Map(
        "xivohost" -> "xivoIP",
        "xivoAuthPort" -> 9497
      )
    }

    val requester = TestProbe()
    val userId = 45
    val token = s"auth-toke-user-id$userId"
    val xivoAuth = system.actorOf(Props(new Actor() {
      def receive: Receive = {
        case GetToken(id) =>
          if (id == userId) {
            sender ! Token(token, new DateTime(), new DateTime(), "autId", s"xivoId$userId")
          }
      }
    }))
    val directoryTransformer = TestProbe()
    val repo = mock[ConfigRepository]
    val user = XucUser("testUsername","pass")
    when(repo.getUser(user.ctiUsername)).thenReturn(Some(XivoUser(userId, "tFirst", Some("tLast"), Some("testUsername"), Some("password"))))
    val xivoWS: XiVOWSdef = mock[XiVOWSdef]
    def actor() = {
      val a = TestActorRef(new XivoDirectory(xivoAuth, directoryTransformer.ref, repo, xivoWS))
      (a, a.underlyingActor)
    }

    val searchResponse =
      """
        |{
        |    "column_headers": [
        |        "Favoris",
        |        "Nom",
        |        "Numéro",
        |        "Mobile",
        |        "Autre numéro",
        |        "Personnel",
        |        "Email"
        |    ],
        |    "column_types": [
        |        "favorite",
        |        "name",
        |        "number",
        |        "mobile",
        |        "number",
        |        "personal",
        |        "name"
        |    ],
        |    "results": [
        |        {
        |            "column_values": [
        |                false,
        |                "Peter Pan",
        |                "44201",
        |                "+33061254658",
        |                "+33231568456",
        |                false,
        |                "pp@test.nodomain"
        |            ],
        |            "relations": {
        |                "agent_id": null,
        |                "endpoint_id": null,
        |                "source_entry_id": "565-qsdf-qsdf-5-555",
        |                "user_id": null,
        |                "xivo_id": null
        |            },
        |            "source": "avc"
        |        },
        |        {
        |            "column_values": [
        |                true,
        |                "Lord Aramis",
        |                "44205",
        |                "012345687",
        |                null,
        |                false,
        |                null
        |            ],
        |            "relations": {
        |                "agent_id": 4,
        |                "endpoint_id": 2,
        |                "source_entry_id": "1",
        |                "user_id": 111,
        |                "xivo_id": "3c3f8fe6-f776-4917-bc5d-1733975256d2"
        |            },
        |            "source": "internal"
        |        }
        |    ],
        |    "term": "44"
        |}
        |    """.stripMargin
  }


}
