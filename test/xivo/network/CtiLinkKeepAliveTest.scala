package xivo.network

import akka.testkit.{TestProbe, TestActorRef}
import akkatest.TestKitSpec
import org.xivo.cti.MessageFactory
import xivo.network.CtiLinkKeepAlive.StartKeepAlive

class CtiLinkKeepAliveTest extends  TestKitSpec("CtiLinkKeepAliveTest") {

  class Helper {
    def actor() = {
      val a = TestActorRef(CtiLinkKeepAlive.props("testUser"))
      (a, a.underlyingActor)
    }
  }

  "keep alive" should {
    "send a user status to ctiLink" in new Helper {
      val (ref, _) = actor()
      val ctiLink = TestProbe()

      ref ! StartKeepAlive("67", ctiLink.ref)

      val msg = new MessageFactory().createGetUserStatus("67")

      ref ! msg

      ctiLink.expectMsg(msg)

    }
  }

}
