package xivo.websocket

import java.util.UUID

import models.{CallbackList, CallbackLists, QueueCallList}
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, Period}
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}
import org.xivo.cti.message.QueueConfigUpdate
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import services.agent.{AgentStatistic, StatDateTime, Statistic}
import services.config.ConfigDispatcher._
import services.config.LineConfig
import services.request.{CallbackClotured, CallbackReleased, CallbackStarted, CallbackTaken}
import xivo.events.AgentState.AgentReady
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType._
import xivo.models._
import xivo.websocket.WsBus.WsContent
import xivo.xucami.models.QueueCall

class WsEncoderSpec extends WordSpec with Matchers with BeforeAndAfterEach {

  var encoder: WsEncoder = null

  override def beforeEach(): Unit = {
    encoder = new WsEncoder
  }

  "Ws encoder" should {
    "do not encode unknow message" in {
      encoder.encode("unknown message") should be(None)
    }
    "encode agent state event" in {
      val agentState = AgentReady(32, new DateTime(), "4350", List())
      val result = Some(WsContent(WebSocketEvent.createEvent(agentState)))

      val encoded = encoder.encode(agentState)

      encoded should be(result)

    }
    "encode queue list event" in {
      val queues = List(new QueueConfigUpdate)

      val encoded = encoder.encode(QueueList(queues))

      encoded should be(Some(WsContent(WebSocketEvent.createQueuesEvent(queues))))

    }
    "encode agent list " in {
      val agents = List(Agent(1, "John", "Doe", "2250", "default"))

      val encoded = encoder.encode(AgentList(agents))

      encoded should be(Some(WsContent(WebSocketEvent.createAgentListEvent(agents))))
    }
    "encode agent group list" in {
      val agentGroups = List(AgentGroup(Some(1),"yellow"))

      val encoded = encoder.encode(AgentGroupList(agentGroups))

      encoded should be(Some(WsContent(WebSocketEvent.createAgentGroupsEvent(agentGroups))))

    }
    "encode queue member list" in {
      val agentQueueMembers = List(AgentQueueMember(2,3,5))

      val encoded = encoder.encode(AgentQueueMemberList(agentQueueMembers))

      encoded should be(Some(WsContent(WebSocketEvent.createAgentQueueMembersEvent(agentQueueMembers))))
    }

    "encode agent" in {
      val agent = Agent(1, "John", "Doe", "2250", "default")

      val encoded = encoder.encode(agent)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(agent))))
    }
    "encode agent queue member" in {
      val queueM = AgentQueueMember(1, 2, 8)

      val encoded = encoder.encode(queueM)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(queueM))))
    }
    "encode queue" in {
      val queueConfigUpdate = new QueueConfigUpdate()
      queueConfigUpdate.setId(58)
      queueConfigUpdate.setDisplayName("bank_customers")

      val encoded = encoder.encode(queueConfigUpdate)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(queueConfigUpdate))))
    }
    "encode agent directory" in {
      val ad = AgentDirectory(List())

      val encoded = encoder.encode(ad)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(ad))))

    }
    "encode agregated stat event" in {
      val aggregatedStatEvent = AggregatedStatEvent(ObjectDefinition(Queue, Some(78)), List[Stat](Stat("totalnumber", 32)))

      val encoded = encoder.encode(aggregatedStatEvent)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(aggregatedStatEvent))))
    }
    "encode agentstatistic with date time event" in {
      val agentstat = AgentStatistic(32,List(Statistic("loginTime",StatDateTime(new DateTime()))))


      val encoded = encoder.encode(agentstat)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(agentstat))))
    }
    "encode queuecalls" in {
      val queueCalls = QueueCallList(4, List(
        QueueCall(1, "foo", "33345678", new DateTime),
        QueueCall(2, "bar", "4478965", new DateTime)))

      encoder.encode(queueCalls) should be(Some(WsContent(WebSocketEvent.createEvent(queueCalls))))
    }
    "encode CallHistory" in {
      val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
      val callHistory = CallHistory(List(
        CallDetail(format.parseDateTime("2014-01-01 08:00:00"), Some(new Period(0, 12, 47, 0)), "3345678", Some("1235748"), CallStatus.Ongoing)
      ))

      encoder.encode(callHistory) should be(Some(WsContent(WebSocketEvent.createEvent(callHistory))))
    }

    "encode CallbackLists" in {
      val list = CallbackList(Some(UUID.randomUUID()), "Test", 1, List())
      encoder.encode(CallbackLists(List(list))) should be(Some(WsContent(WebSocketEvent.createEvent(CallbackLists(List(list))))))
    }

    "encode CallbackTaken" in {
      val cbTaken = CallbackTaken(UUID.randomUUID(), 12)
      encoder.encode(cbTaken) should be(Some(WsContent(WebSocketEvent.createEvent(cbTaken))))
    }

    "encode CallbackReleased" in {
      val cbReleased = CallbackReleased(UUID.randomUUID())
      encoder.encode(cbReleased) should be(Some(WsContent(WebSocketEvent.createEvent(cbReleased))))
    }

    "encode CallbackStarted" in {
      val requestUuid = UUID.randomUUID()
      val ticketUuid = UUID.randomUUID()
      val cbStarted = CallbackStarted(requestUuid, ticketUuid)
      encoder.encode(cbStarted) should be(Some(WsContent(WebSocketEvent.createEvent(cbStarted))))
    }

    "encode CallbackClotured" in {
      val cbClotured = CallbackClotured(UUID.randomUUID())
      encoder.encode(cbClotured) should be(Some(WsContent(WebSocketEvent.createEvent(cbClotured))))
    }

    "encode LineConfig" in {
      val lineCfg = LineConfig("1", "2005", Some(Line(1, "default", "SIP", "name", None, Some("password"),
        "xivoIP")))
      encoder.encode(lineCfg) should be(Some(WsContent(WebSocketEvent.createEvent(lineCfg))))
    }
  }

}
