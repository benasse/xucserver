package xivo.models

import org.specs2.mutable.Specification
import models.QueueLog
import xivo.data.dbunit.DBUtil
import play.api.test.Helpers.running
import play.api.test.FakeApplication

object QueueLogSpec extends Specification {

  "queuelog" should {
    "be able to be retreived" in {
      val appliConfig: Map[String, String] = {
        Map(
          "logger.root" -> "OFF",
          "logger.application" -> "OFF",
          "logger.services" -> "OFF",
          "db.default.driver" -> "org.postgresql.Driver",
          "db.default.url" -> "jdbc:postgresql://127.0.0.1/testdata",
          "db.default.user" -> "test",
          "db.default.password" -> "test")
      }
      running(FakeApplication(additionalConfiguration = appliConfig)) {
        DBUtil.setupDB("queuelog.xml")
        QueueLog.getAll("cast((now() - interval '2 seconds') as varchar)").foreach(println)
        QueueLog.getAll("'2014-04-18 12:12:20.225112'").foreach(println)
        println(QueueLog.getAll("'2014-04-18 12:12:20.225111'").last)
        QueueLog.getAll("'2014-04-18 12:12:20.225111'").size should be > 1
      }
    }
  }

}