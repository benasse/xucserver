package xivo.models

import org.scalatestplus.play.{WsScalaTestClient, OneAppPerSuite, PlaySpec}
import play.api.libs.json._
import play.api.test.{FakeApplication, PlaySpecification}
import xivo.data.dbunit.DBUtil
import org.scalatest._
import xuctest.{BaseTest, IntegrationTest}

class AgentQueueMemberSpec extends BaseTest with OptionValues with WsScalaTestClient with OneAppPerSuite {

  def integrationConfig: Map[String, String] = {
    Map(
      "db.default.driver" -> "org.postgresql.Driver",
      "db.default.url" -> "jdbc:postgresql://127.0.0.1/testdata",
      "db.default.user" -> "test",
      "db.default.password" -> "test"
    ) ++ xivoIntegrationConfig
  }

  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = integrationConfig
    )

  "agent queue member" should {

    "be able to have penalty updated" taggedAs(IntegrationTest) ignore {
      val agentId = 19
      val queueId = 1

      val result = AgentQueueMember.setAgentQueue(agentId, queueId, 11)
      result should be(Some(AgentQueueMember(agentId, queueId, 11)))
    }
    "be able to remove an agent from a queue" taggedAs(IntegrationTest) ignore {
      val agentId = 19
      val queueId = 1

      AgentQueueMember.setAgentQueue(agentId, queueId, 11)

      val result = AgentQueueMember.removeAgentFromQueue(agentId, queueId)

      result should be(Some(AgentQueueMember(agentId, queueId, -1)))
    }
    "be able to retreive a list of queue members from db" in {
      DBUtil.setupDB("agentqueuemember.xml")

      val aqms = AgentQueueMember.all()

      aqms.length should be(2)
      aqms should contain (AgentQueueMember(21,101,2))

    }
    "be able to retreive queue members from agent number and queue name" in {
      DBUtil.setupDB("agentqueuemember.xml")

      val agqm = AgentQueueMember.getQueueMember("1230","yellow")

      agqm should be(Some(AgentQueueMember(21,102,7)))

    }

  }
}