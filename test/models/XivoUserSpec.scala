package models

import org.scalatest.BeforeAndAfterEach
import org.specs2.specification.AfterExample
import play.api.libs.ws.WSRequestHolder
import play.api.test.PlaySpecification
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.Json
import xivo.network.{XiVOWS, XiVOWSdef}
import xuctest.BaseTest
import scala.concurrent.Future
import play.api.libs.ws.WSResponse

class XivoUserSpec extends BaseTest with BeforeAndAfterEach with MockitoSugar {

  val user = """{
    "caller_id": "\"acd46 acd46\"",
    "description": "",
    "firstname": "acd46",
    "id": 90,
    "language": null,
    "lastname": "acd46",
    "links": [
    {
      "href": "https://192.168.51.232:50051/1.1/users/90",
      "rel": "users"
    }
    ],
    "mobile_phone_number": null,
    "music_on_hold": "default",
    "outgoing_caller_id": "default",
    "password": "0000",
    "preprocess_subroutine": null,
    "timezone": "",
    "userfield": "",
    "username": "acd46"
  }"""

  val ctiProfile = """{
    "cti_profile_id": null,
    "enabled": false,
    "links": [
    {
      "href": "https://192.168.51.232:50051/1.1/users/290",
      "rel": "users"
    }
    ],
    "user_id": 90
  }"""

  override def afterEach = XivoUser.ws = XiVOWS

  "XivoUser" should {
    "be able to get a list of users from xivo" in {
      val ws: XiVOWSdef = mock[XiVOWSdef]
      val responseHolder = mock[WSRequestHolder]
      val response = mock[WSResponse]
      stub(response.json).toReturn(Json.parse("{\"items\": [" + user + "," + user + "]}"))
      stub(responseHolder.get()).toReturn(Future.successful(response))
      stub(ws.withWS("users")).toReturn(responseHolder)
      XivoUser.ws = ws
      XivoUser.all.length should be(2)
    }
    "filter users without cti activated from a list of users" in {
      val ws: XiVOWSdef = mock[XiVOWSdef]
      val responseHolder = mock[WSRequestHolder]
      val response = mock[WSResponse]
      stub(response.json).toReturn(Json.parse(ctiProfile))
      stub(responseHolder.get()).toReturn(Future.successful(response))
      stub(ws.withWS("users/90/cti")).toReturn(responseHolder)
      val user = XivoUser(90, "fred", Some("enVacances"), Some("ffref"), Some("rien"))
      XivoUser.ws = ws

      XivoUser.filterNonCtiUsers(List(user)).length should be(0)
    }
  }
}