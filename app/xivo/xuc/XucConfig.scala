package xivo.xuc

import play.api.Play.current
import play.api.libs.ws.WSAuthScheme
import scala.collection.JavaConverters._
import scala.concurrent.duration._

object XucConfig {
  def xivoHost: String = current.configuration.getString("xivohost").getOrElse("")

  def outboundLength:Int = current.configuration.getInt("xuc.outboundLength").getOrElse(6)

  val XivoCtiIdentitySuffix = "XuC"
  def EventUser: String = current.configuration.getString("xivocti.eventUser").getOrElse("")
  def XivoCtiTimeout = current.configuration.getString("xivocti.timeout").getOrElse("1").toInt

  def XivoWs_host = current.configuration.getString("XivoWs.host").getOrElse("")
  def XivoWs_port = current.configuration.getString("XivoWs.port").getOrElse("")
  def XivoWs_wsUser = current.configuration.getString("XivoWs.wsUser").getOrElse("")
  def XivoWs_wsPwd = current.configuration.getString("XivoWs.wsPwd").getOrElse("")

  def EVENT_URL = current.configuration.getString("api.eventUrl").getOrElse("")
  def DefaultPassword = current.configuration.getString("xivocti.defaultPassword").getOrElse("")
  
  val XIVOCTI_HOST:String = current.configuration.getString("xivocti.host").getOrElse("localhost")
  val XIVOCTI_PORT:String = current.configuration.getString("xivocti.port").getOrElse("5003")


  def SnomUser = current.configuration.getString("Snom.user").getOrElse("")
  def SnomPassword = current.configuration.getString("Snom.password").getOrElse("")

  def metricsRegistryName: String = current.configuration.getString("techMetrics.name").getOrElse("techMetrics")
  def metricsRegistryJVM: Boolean = current.configuration.getBoolean("techMetrics.jvm").getOrElse(true)
  def metricsLogReporter: Boolean = current.configuration.getBoolean("techMetrics.logReporter").getOrElse(true)
  def metricsLogReporterPeriod: Long = current.configuration.getLong("techMetrics.logReporterPeriod").getOrElse(5)

  def xucPeers : List[String] = current.configuration.getStringList("xuc.peers").getOrElse(new java.util.ArrayList[String]()).asScala.toList

  def recordingHost: String = current.configuration.getString("recording.host").getOrElse("localhost")
  def recordingPort: Int = current.configuration.getInt("recording.port").getOrElse(9000)
  def recordingToken: String = current.configuration.getString("recording.token").getOrElse("")

  def configHost: String  = current.configuration.getString("config.host").getOrElse("")
  def configPort: Int     = current.configuration.getInt("config.port").getOrElse(0)
  def configToken: String = current.configuration.getString("config.token").getOrElse("")


  val defaultWSAuthScheme = WSAuthScheme.BASIC
  val defaultProtocol = "https"
  val defaultWSTimeout = 3 seconds
  object XivoAuth {
    val getTokenURI = "0.1/token"
    val backend = "xivo_user"
    def port: Int = current.configuration.getInt("xivo.authPort").getOrElse(9497)
    val defaultExpires: Long = current.configuration.getLong("xivo.auth.defaultExpires").getOrElse(17200)
  }

  def useXivoDir = current.configuration.getBoolean("xivo.useDird").getOrElse(true)
  object XivoDir {
    def defaultProfile = current.configuration.getString("xivo.dirdDefaultProfile").getOrElse("default")
    val searchURI = "0.1/directories/lookup"
    val favoriteURI = "0.1/directories/favorites"
    val searchArg = "?term="
    def port: Int = current.configuration.getInt("xivo.dirdPort").getOrElse(9489)
  }

}

object XucStatsConfig {
  def resetSchedule: String = current.configuration.getString("xucstats.resetSchedule").getOrElse("0 0 0 * * ?")
  val initFromMidnight:Boolean = current.configuration.getBoolean("xucstats.initFromMidnight").getOrElse(false)

  val aggregationPeriod: Long = current.configuration.getLong("xucstats.aggregationPeriod").getOrElse(1)

  val statsLogReporter = current.configuration.getBoolean("metrics.logReporter").getOrElse(true)
  val statsLogReporterPeriod: Long = current.configuration.getLong("metrics.logReporterPeriod").getOrElse(5)
}
