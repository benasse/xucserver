package xivo.events

import org.joda.time.{DateTime, Seconds}
import org.xivo.cti.message.AgentStatusUpdate
import org.xivo.cti.model.Availability._
import org.xivo.cti.model.StatusReason._
import play.api.libs.json._
import xivo.models.{AgentLoginStatus, Agent}
import xivo.xucami.models.MonitorState
import xivo.xucami.models.MonitorState.MonitorState
import xivo.xucami.userevents.UserEventAgentLogin

import scala.collection.JavaConversions._

abstract class AgentState(val name: String, val agentId: Agent.Id, val changed: DateTime, val phoneNb: String, val queues: List[Int], val cause: Option[String] = Some("")) {
  def getSince: Long = Seconds.secondsBetween(changed, new DateTime()).getSeconds

  def withCause(newCause:String): AgentState

  def withPhoneNb(newPhoneNb:String): AgentState

  override def equals(that: Any) = {
    that match {
      case that: AgentState =>
        that.canEqual(this) &&
          that.name == this.name &&
          that.agentId == this.agentId &&
            that.phoneNb == this.phoneNb &&
          that.queues == this.queues &&
          that.cause == this.cause
      case _ => false
    }
  }

  def canEqual(a: Any) = a.isInstanceOf[AgentState]
}

object AgentState {

  private def commonJson(agentState: AgentState): JsObject = {
    val json = Json.obj(
      "name" -> agentState.name,
      "agentId" -> agentState.agentId,
      "phoneNb" -> agentState.phoneNb,
      "since" -> agentState.getSince,
      "queues" -> agentState.queues)

    agentState.cause match {
      case Some(cause) =>
        json.++(Json.obj("cause" -> cause))
      case None =>
        json
    }
  }

  val agentOnCallWrites = new Writes[AgentOnCall] {
    def writes(c: AgentOnCall): JsValue = {
      commonJson(c).++(Json.obj(
        "acd" -> c.acd,
        "direction" -> c.direction.toString,
        "monitorState" -> c.monitorState.toString)
      )
    }
  }

  val agentStateCommonWrites = new Writes[AgentState] {
    def writes(c: AgentState): JsValue = {
      commonJson(c)
    }
  }

  implicit val agentStateWrites =
    new Writes[AgentState] {
      def writes(c: AgentState) = c match {
        case aoc: AgentOnCall => agentOnCallWrites.writes(aoc)
        case other => agentStateCommonWrites.writes(other)
      }

    }

  object CallDirection extends Enumeration {
    type CallDirection = Value
    val Incoming = Value
    val Outgoing = Value
    val DirectionUnknown = Value
  }
  import xivo.events.AgentState.CallDirection._

  case class AgentLogin(id: Agent.Id, override val changed: DateTime, override val phoneNb: String, override val queues: List[Int], override val cause: Option[String] = Some(""))
    extends AgentState("AgentLogin",id, changed, phoneNb, queues, cause) {

    def withCause(newCause:String) = this.copy(cause = Some(newCause))
    def withPhoneNb(newPhoneNb: String): AgentState = this.copy(phoneNb = newPhoneNb)
  }
  object AgentLogin {
    def apply(state: AgentState): AgentLogin = AgentLogin(state.agentId, new DateTime(state.getSince), state.phoneNb, state.queues)
    def apply(uEvtAgtLogin: UserEventAgentLogin): AgentLogin = AgentLogin(uEvtAgtLogin.getAgentId().toLong,new DateTime(),uEvtAgtLogin.getExtension(),List())
    def apply(agentLoginStatus: AgentLoginStatus): AgentLogin = AgentLogin(agentLoginStatus.id, agentLoginStatus.loginDate, agentLoginStatus.phoneNumber, List())
  }

  case class AgentReady(id: Agent.Id, override val changed: DateTime, override val phoneNb: String, override val queues: List[Int], override val cause: Option[String] = Some(""))
    extends AgentState("AgentReady", id, changed, phoneNb, queues, cause) {

    def withCause(newCause:String) = this.copy(cause = Some(newCause))
    def withPhoneNb(newPhoneNb: String): AgentState = this.copy(phoneNb = newPhoneNb)

  }
  object AgentReady {
    def apply(state: AgentState): AgentReady = AgentReady(state.agentId, new DateTime, state.phoneNb, state.queues, state.cause)
  }
  case class AgentDialing(id: Agent.Id, override val changed: DateTime, override val phoneNb: String, override val queues: List[Int], override val cause: Option[String] = Some(""))
    extends AgentState("AgentDialing", id, changed, phoneNb, queues, cause) {

    def withCause(newCause:String) = this.copy(cause = Some(newCause))
    def withPhoneNb(newPhoneNb: String): AgentState = this.copy(phoneNb = newPhoneNb)
  }

  object AgentDialing {
    def apply(state: AgentState): AgentDialing = AgentDialing(state.agentId, new DateTime, state.phoneNb, state.queues, state.cause)
  }

  case class AgentOnCall(id: Agent.Id, override val changed: DateTime, acd: Boolean, val direction: CallDirection,
                         override val phoneNb: String, override val queues: List[Int], val onPause:Boolean,
                         override val cause: Option[String] = Some(""), val monitorState: MonitorState = MonitorState.UNKNOWN)
    extends AgentState("AgentOnCall",id, changed, phoneNb, queues, cause) {

    def withMonitorState(newMonitorState: MonitorState) = this.copy(monitorState = newMonitorState)
    def withCause(newCause:String) = this.copy(cause = Some(newCause))
    def withPhoneNb(newPhoneNb: String): AgentOnCall = this.copy(phoneNb = newPhoneNb)

    override def equals(that: Any) = {
      that match {
        case that: AgentOnCall =>
          that.canEqual(this) &&
            that.name == this.name &&
            that.agentId == this.agentId &&
            that.phoneNb == this.phoneNb &&
            that.queues == this.queues &&
            that.cause == this.cause &&
            that.acd == this.acd &&
            that.direction == this.direction &&
            that.monitorState == this.monitorState
        case _ => false
      }
    }

    override def canEqual(a: Any) = a.isInstanceOf[AgentOnCall]
  }

  object AgentOnCall {
    def apply(state: AgentState, callDirection: CallDirection, acdState:Boolean, paused:Boolean): AgentOnCall =
      AgentOnCall(state.agentId, new DateTime,acd = acdState,callDirection, state.phoneNb, state.queues,  onPause=paused, state.cause)
  }
  case class AgentOnPause( id: Agent.Id, override val changed: DateTime, override val phoneNb: String, override val queues: List[Int], override val cause: Option[String] = Some(""))
    extends AgentState("AgentOnPause", id, changed, phoneNb, queues, cause) {

    def withCause(newCause:String) = this.copy(cause = Some(newCause), changed = new DateTime())
    def withPhoneNb(newPhoneNb: String): AgentState = this.copy(phoneNb = newPhoneNb)
  }

  object AgentOnPause {
    def apply(state: AgentState): AgentOnPause = AgentOnPause(state.agentId, new DateTime, state.phoneNb, state.queues, state.cause)
  }

  case class AgentLoggedOut(id: Agent.Id, override val changed: DateTime, override val phoneNb: String, override val queues: List[Int], override val cause: Option[String] = Some(""))
    extends AgentState("AgentLoggedOut", id, changed, phoneNb, queues, cause) {

    def withCause(newCause:String) = this.copy(cause = Some(newCause))
    def withPhoneNb(newPhoneNb: String): AgentState = this.copy(phoneNb = newPhoneNb)
  }

  case class AgentOnWrapup(id: Agent.Id, override val changed: DateTime, override val phoneNb: String, override val queues: List[Int], override val cause: Option[String] = Some(""))
    extends AgentState("AgentOnWrapup", id, changed, phoneNb, queues, cause) {

    def withCause(newCause:String) = this.copy(cause = Some(newCause))
    def withPhoneNb(newPhoneNb: String): AgentState = this.copy(phoneNb = newPhoneNb)
  }

  case class AgentRinging(id:Agent.Id, override val changed: DateTime, override val phoneNb:String, override val queues: List[Int], override val cause: Option[String] = Some(""))
    extends AgentState("AgentRinging", id, new DateTime, phoneNb, queues, cause) {

    def withCause(newCause:String) = this.copy(cause = Some(newCause))
    def withPhoneNb(newPhoneNb: String): AgentState = this.copy(phoneNb = newPhoneNb)
  }

  object AgentRinging {
    def apply(state: AgentState): AgentRinging = AgentRinging(state.agentId, new DateTime, state.phoneNb, state.queues, state.cause)
  }

  def fromAgentStatusUpdate(agentStatus: AgentStatusUpdate, date: DateTime = new DateTime): Option[AgentState] = {
    val changed = new DateTime(agentStatus.getStatus.getSince)
    val phoneNb = agentStatus.getStatus.getPhonenumber
    val queues: List[Int] = (agentStatus.getStatus.getQueues().toList).map(Int.unbox(_))
    (agentStatus.getStatus.getAvailability, agentStatus.getStatus.getReason) match {
      case (AVAILABLE, _) => Some(AgentReady(agentStatus.getAgentId, changed, agentStatus.getStatus.getPhonenumber, queues))
      case (UNAVAILABLE, ON_WRAPUP) => Some(AgentOnWrapup(agentStatus.getAgentId, new DateTime, phoneNb, queues))
      case (UNAVAILABLE, NONE) => Some(AgentOnPause(agentStatus.getAgentId, new DateTime, phoneNb, queues))
      case (LOGGED_OUT, _) => Some(AgentLoggedOut(agentStatus.getAgentId, changed, phoneNb, queues))
      case (_, _) => None
    }
  }

  def toJson(agentState: AgentState): JsValue = {
    Json.toJson(agentState)
  }
}
