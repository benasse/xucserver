package xivo.data

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import models.QLogTransformer.QLogDataTransformer
import models.QueueLog.QueueLogData
import stats.Statistic.ResetStat
import stats.{ProdStatCreator, QueueEventDispatcher}

trait EvtDispatcher {
  val queueEventDispatcher: ActorRef
}

trait ProductionEvtDispatcher extends EvtDispatcher {
  this: Actor =>
  val queueEventDispatcher = context.actorOf(Props(new QueueEventDispatcher with ProdStatCreator), "queueEventDispatcher")
}

object QLogDispatcher {
  def props(qLogTransformer: QLogDataTransformer): Props = Props(new QLogDispatcher(qLogTransformer) with ProductionEvtDispatcher)
}

class QLogDispatcher(qLogTransformer: QLogDataTransformer) extends Actor with ActorLogging {
  this: EvtDispatcher =>


  def receive = {

    case ResetStat => queueEventDispatcher ! ResetStat

    case qLogData: QueueLogData =>
      log.debug(s"$qLogData")
      queueEventDispatcher ! qLogTransformer(qLogData)
  }
}