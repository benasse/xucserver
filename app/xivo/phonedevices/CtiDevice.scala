package xivo.phonedevices

import akka.actor.ActorRef
import org.xivo.cti.MessageFactory
import xivo.models.Line
import xivo.xuc.XucConfig

class CtiDevice(val line:Line, val lineNumber: String) extends DeviceAdapter {
  val messageFactory = new MessageFactory

  override def dial(destination: String, variables: Map[String, String], sender: ActorRef) = sender ! StandardDial(line, lineNumber, destination, variables, XucConfig.xivoHost)
  override def answer(sender: ActorRef) = {}
  override def hangup(sender: ActorRef) = sender ! messageFactory.createHangup
  override def attendedTransfer(destination: String, sender: ActorRef) = sender ! messageFactory.createAttendedTransfer(destination)
  override def completeTransfer(sender: ActorRef) = sender ! messageFactory.createCompleteTransfer
  override def cancelTransfer(sender: ActorRef) =  sender ! messageFactory.createCancelTransfer
  override def conference(sender: ActorRef) = {}
  override def hold(sender: ActorRef) = {}

}