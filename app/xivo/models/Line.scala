package xivo.models

import xivo.xuc.XucConfig

import scala.concurrent.Await
import play.api.libs.json._
import scala.concurrent.duration.DurationInt
import xivo.network.XiVOWS
import play.api.http.Status._
import play.api.Logger

import scala.language.postfixOps

case class XivoDevice(id: String, ip: Option[String], vendor: String)
case class XivoLine(id: Int, context: String, protocol:String, name: String, device_id: Option[String])
case class XivoSipEndpointLink(line_id: Int, endpoint_id: Int, endpoint: String)
case class XivoEndpoint(username: String, secret: String)
case class Line(id: Int, context: String, protocol:String, name: String, dev: Option[XivoDevice],
                password: Option[String] = None, xivoIp: String = XucConfig.xivoHost) {
  def interface =  s"${protocol.toUpperCase}/$name"
}

trait LineFactory {
  def get(Id: Line.Id): Option[Line]
}

object Line extends LineFactory {
  type Id = Long
  val waitResult = 10 seconds

  val logger = Logger(getClass.getName)
  implicit val context = scala.concurrent.ExecutionContext.Implicits.global
  implicit val lineReads = Json.reads[XivoLine]
  implicit val deviReads = Json.reads[XivoDevice]
  implicit val sipEndpointLinkReads = Json.reads[XivoSipEndpointLink]
  implicit val endpointReads = Json.reads[XivoEndpoint]

  def get(Id: Line.Id): Option[Line] = {
    getXiVOLine(Id) match {
      case Some(xivoline) =>
            val password: Option[String] = getEndpoint(xivoline).fold[Option[String]](None)(e => Some(e.secret))
            Some(Line(xivoline.id, xivoline.context, xivoline.protocol, xivoline.name,
              xivoline.device_id.flatMap(getDevice), password))
      case _ => None
    }
  }

  private def getXiVOLine(id: Line.Id): Option[XivoLine] = {
    val responseFuture = XiVOWS.withWS(s"lines/$id").get()
    val resultFuture = responseFuture map { response =>
      response.status match {
        case OK =>
          response.json.validate[XivoLine] match {
            case JsError(e) =>
              logger.debug(s"Unparsable JSON received for line $id : $e")
              None
            case JsSuccess(line, _) =>
              logger.debug(s"Returning line $line")
              Some(line)
          }
        case default =>
          logger.warn(s"Webservice get line for id: $id failed.")
          None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def all: List[Line] = {
    for (xivoLine <- allLines) yield
      Line(
        xivoLine.id, xivoLine.context, xivoLine.protocol, xivoLine.name,
        getDevice(xivoLine.device_id.getOrElse("unknown")),
        getEndpoint(xivoLine).fold[Option[String]](None)(e => Some(e.secret))
      )
  }
  private def allLines: List[XivoLine] = {
    val responseFuture = XiVOWS.withWS("lines").get()
    val resultFuture = responseFuture map {
      response =>
        (response.json \ "items").validate[List[XivoLine]].get
    }
    Await.result(resultFuture, waitResult)
  }

  def getDevice(id: String): Option[XivoDevice] = {
    val responseFuture = XiVOWS.withWS(s"devices/$id").get()
    val resultFuture = responseFuture map { response =>
      if (response.status == OK) {
        response.json.validate[XivoDevice] match {
          case JsError(e) => logger.warn(s"Unparsable JSON received for device $id : $e")
            None
          case JsSuccess(device, _) => Some(device)
        }
      } else {
        logger.warn(s"Webservice get device for id: $id failed.")
        None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def getEndpoint(xivoLine: XivoLine): Option[XivoEndpoint] = {
    xivoLine.protocol match {
      case "sip" =>
        getSipEndpointLink(xivoLine.id).fold[Option[XivoEndpoint]](None)(link => getEndpoint(link.endpoint_id))
      case _ =>
        None
    }
  }

  def getSipEndpointLink(id: Int): Option[XivoSipEndpointLink] = {
    val responseFuture = XiVOWS.withWS(s"lines/$id/endpoints/sip").get()
    val resultFuture = responseFuture map { response =>
      if (response.status == OK) {
        response.json.validate[XivoSipEndpointLink] match {
          case JsError(e) => logger.warn(s"Unparsable JSON received for endpoint for lineId $id : $e")
            None
          case JsSuccess(endpoint, _) => Some(endpoint)
        }
      } else {
        logger.warn(s"Webservice get device for id: $id failed.")
        None
      }
    }
    Await.result(resultFuture, waitResult)
  }

  def getEndpoint(id: Int): Option[XivoEndpoint] = {
    val responseFuture = XiVOWS.withWS(s"endpoints/sip/$id").get()
    val resultFuture = responseFuture map { response =>
      if (response.status == OK) {
        response.json.validate[XivoEndpoint] match {
          case JsError(e) => logger.warn(s"Unparsable JSON received for endpoint for lineId $id : $e")
            None
          case JsSuccess(endpoint, _) => Some(endpoint)
        }
      } else {
        logger.warn(s"Webservice get device for id: $id failed.")
        None
      }
    }
    Await.result(resultFuture, waitResult)
  }

}
