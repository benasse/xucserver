package xivo.xucstats

import java.util.concurrent.TimeUnit

import com.codahale.metrics.Slf4jReporter
import play.api._
import play.api.libs.concurrent.Akka
import play.api.Play.current
import akka.actor.{ActorRef, Props}
import stats.{QueueStatCache, StatsAggregator}
import stats.StatsAggregator.SubscribeToBus
import xivo.data.QLogDispatcher
import xivo.data.QLogTransfer
import models.QLogTransformer
import models.QueueLog
import us.theatr.akka.quartz.QuartzActor
import services.XucStatsEventBus
import com.kenshoo.play.metrics.MetricsRegistry

class XucStatsPlugin(val app: Application) extends Plugin {

  override def onStart() {
    Logger.info("Starting xuc stats")
    Logger.info("-------------parameters--------------------")
    Logger.info(s"resetSchedule           : ${XucStatsConfig.resetSchedule}")
    Logger.info(s"initFromMidnight        : ${XucStatsConfig.initFromMidnight}")
    Logger.info(s"statsLogReporter        : ${XucStatsConfig.statsLogReporter}")
    Logger.info(s"statsLogReporterPeriod  : ${XucStatsConfig.statsLogReporterPeriod}")

    val cronScheduler = Akka.system.actorOf(Props[QuartzActor])
    val qlogDispatcher = Akka.system.actorOf(QLogDispatcher.props(QLogTransformer.transform), "QLogDispatcher")
    val qStatCache = Akka.system.actorOf(QueueStatCache.props,"QStatCache")
    val agregator = initiateGlobalAggregator
    XucStatsEventBus.statEventBus.setBusManager(qStatCache)
    val qlogTransfer = Akka.system.actorOf(Props(new QLogTransfer(QueueLog.getAll, qlogDispatcher)), "QLogTransfer")

    if (XucStatsConfig.statsLogReporter) {
      val reporter= Slf4jReporter.forRegistry(MetricsRegistry.defaultRegistry)
        .outputTo(Logger.logger)
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .build()
      reporter.start(XucStatsConfig.statsLogReporterPeriod, TimeUnit.MINUTES)
    }
  }

  private def initiateGlobalAggregator = {
    val globalAggregator = Akka.system.actorOf(StatsAggregator.props, "GlobalAggregator")
    globalAggregator ! SubscribeToBus
    globalAggregator
  }

  override def onStop() {
    Logger.info("Xuc stats Application shutdown...")
  }
  
  override def enabled = true
}
