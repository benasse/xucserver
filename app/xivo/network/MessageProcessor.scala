package xivo.network

import org.xivo.cti.MessageParser
import org.xivo.cti.message.CtiResponseMessage
import org.json.JSONException
import scala.util.Try
import org.xivo.cti.message.PhoneStatusUpdate
import play.api.Logger
import java.security.InvalidParameterException

class MessageProcessor {
  val log = Logger(getClass.getName)

  val MsgSeparator = "\n"
  var partialMessage = ""

  var messageParser: MessageParser = new MessageParser

  def parseMessage(message: String): Option[CtiResponseMessage[_]] = {

    try {
      messageParser.parseBuffer(message) match {
        case msg if msg != null => Some(msg)
        case unknown =>
          log.debug(s"unable to parse message $unknown")
          None
      }
    } catch {
      case e: JSONException =>
        log.debug("Invalid JSON in message : " + message)
        None
      case e: InvalidParameterException =>
        log.debug("Invalid parameter in message : " + message)
        None
      case e: IllegalArgumentException =>
        log.debug("Illegal argument in message : " + message)
        None
    }
  }

  def processBuffer(buffer: String): Array[CtiResponseMessage[_]] = {
    var msgs = (partialMessage + buffer).split(MsgSeparator)

    if (!buffer.endsWith(MsgSeparator)) {
      partialMessage = msgs.takeRight(1)(0)
      msgs = msgs.dropRight(1)
    } else partialMessage = ""

    for {
      message <- msgs
      decoded = parseMessage(message)
      if (decoded.isDefined)
    } yield decoded.get

  }

}