package xivo.websocket

import org.json.JSONObject
import play.api.Logger
import com.fasterxml.jackson.databind.JsonNode

class MessageParser(val messageFactory: org.xivo.cti.MessageFactory) {

  def decodeMessage(message: JsonNode): JSONObject = {

      message.get("command").asText() match {
        case "agentLogin" =>

          Logger("WSMessageParser.decodeMessage").debug("Create AgentLogin as asked by the websocket command")
          messageFactory.createAgentLogin(message.get("agentno").asText)

        case "agentLogout" =>
          Logger("WSMessageParser.decodeMessage").debug("CreateAgentLogout as asked by the websocket command")
          messageFactory.createAgentLogout(message.get("agentid").asText)

        case _ =>
          throw new IllegalArgumentException("Unknown websocket command")
      }
  }
}
