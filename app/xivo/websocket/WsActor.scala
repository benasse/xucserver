package xivo.websocket

import akka.actor._
import models.XucUser
import play.api.libs.json.JsValue
import services._
import services.request._
import services.user.UserRight
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus.WsContent
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object WsActor {
  def props(user: XucUser, routerFactory: CtiRouterFactory = CtiRouterFactory.factory): Future[ActorRef => Props] =
    UserRight.getRights(user.ctiUsername).map(right =>
      out => Props(new WsActor(user, out, routerFactory.getRouter(user), right)))



  case class WsConnected(wsActor: ActorRef,user : XucUser)
}

class WsActor(user: XucUser,  out: ActorRef,
              userRouter : ActorRef,
              right: UserRight,
              bus : WsBus = WsBus.bus,
              requestDecoder : RequestDecoder = new RequestDecoder,
              wsEncoder : WsEncoder = new WsEncoder)  extends Actor with ActorLogging {
  log.info(s"${user.ctiUsername} new web socket actor created")
  userRouter ! WsConnected(self, user)
  bus.subscribe(self,WsBus.browserTopic(user.ctiUsername))



  override def receive: Receive = {

    case msg: JsValue =>
      log.debug(s"[${user.ctiUsername} - Val From browser] >>>> $msg")
      requestDecoder.decode(msg) match {
        case Ping => self ! WsContent(play.libs.Json.parse(msg.toString()))
        case browserMsg : BrowserMessage =>
          logrequest(s"$browserMsg")
          userRouter ! BaseRequest(self, browserMsg)
        case invalid : InvalidRequest =>
          logrequest(s"$invalid", log.error)
          self ! WsContent(WebSocketEvent.createError(WSMsgType.Error, invalid.toString))
        case xucRequest : XucRequest =>
          logrequest(s"$xucRequest")
          userRouter ! BaseRequest(self, xucRequest)
      }

    case WsContent(json) =>
      log.debug(s"[${user.ctiUsername} - To browser  ] <<<< $json received from $sender")
      out ! json

    case other => right.filter(other).foreach(m => wsEncoder.encode(m) match {
      case Some(WsContent(json)) =>
        log.debug(s"[${user.ctiUsername} - To browser  ] <<<< $json received from $sender")
        out ! json
      case ukn => log.debug(s"${user.ctiUsername} cannot uncode message for browser $other")
    })

  }

  override def postStop() = {
    log.info(s"${user.ctiUsername} web socket closed")
    userRouter ! Leave(self)
    bus.unsubscribe(self)
  }

  private def logrequest(request:String, logf : String => Unit = log.info) = logf(s"[${user.ctiUsername}] Req: <$request>")
}
