package xivo.websocket

import models.{CallbackLists, QueueCallList}
import org.xivo.cti.message.QueueConfigUpdate
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.config.ConfigDispatcher._
import services.config.LineConfig
import services.request.{CallbackClotured, CallbackStarted, CallbackReleased, CallbackTaken}
import xivo.events.AgentState
import xivo.models.{Agent, AgentQueueMember, CallHistory}
import xivo.websocket.WsBus.WsContent

class WsEncoder {

  def encode(msg : Any): Option[WsContent] = msg match {
    case agentState : AgentState =>
      Some(WsContent(WebSocketEvent.createEvent(agentState)))

    case agent: Agent =>
      Some(WsContent(WebSocketEvent.createEvent(agent)))

    case agentQM: AgentQueueMember =>
      Some(WsContent(WebSocketEvent.createEvent(agentQM)))

    case queue: QueueConfigUpdate =>
      Some(WsContent(WebSocketEvent.createEvent(queue)))

    case QueueList(queues) =>
      Some(WsContent(WebSocketEvent.createQueuesEvent(queues)))

    case AgentList(agents) =>
      Some(WsContent(WebSocketEvent.createAgentListEvent(agents)))

    case AgentGroupList(agentGroups) =>
      Some(WsContent(WebSocketEvent.createAgentGroupsEvent(agentGroups)))

    case AgentQueueMemberList(agentQueueMembers) =>
      Some(WsContent(WebSocketEvent.createAgentQueueMembersEvent(agentQueueMembers)))

    case ad : AgentDirectory =>
      Some(WsContent(WebSocketEvent.createEvent(ad)))

    case aggregatedStat : AggregatedStatEvent =>
      Some(WsContent(WebSocketEvent.createEvent(aggregatedStat)))

    case MeetmeList(meetmeList) => Some(WsContent(WebSocketEvent.createConferenceListEvent(meetmeList)))

    case agentStat : AgentStatistic => Some(WsContent(WebSocketEvent.createEvent(agentStat)))

    case queueCalls: QueueCallList => Some(WsContent(WebSocketEvent.createEvent(queueCalls)))

    case history: CallHistory => Some(WsContent(WebSocketEvent.createEvent(history)))

    case cbLists: CallbackLists => Some(WsContent(WebSocketEvent.createEvent(cbLists)))

    case cbTaken: CallbackTaken => Some(WsContent(WebSocketEvent.createEvent(cbTaken)))

    case cbReleased: CallbackReleased => Some(WsContent(WebSocketEvent.createEvent(cbReleased)))

    case cbStarted: CallbackStarted => Some(WsContent(WebSocketEvent.createEvent(cbStarted)))

    case cbClotured: CallbackClotured => Some(WsContent(WebSocketEvent.createEvent(cbClotured)))

    case lineCfg: LineConfig => Some(WsContent(WebSocketEvent.createEvent(lineCfg)))

    case unknown => None
  }

}
