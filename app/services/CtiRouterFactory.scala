package services

import akka.actor.{Props, ActorRef}
import models.XucUser
import play.api.Logger
import play.api.libs.concurrent.Akka
import play.api.Play.current

class CtiRouterFactory (creator:(Props,String) => ActorRef) {
  protected[services] var routers: Map[String, ActorRef] = Map()
  val log = Logger(getClass.getName)
  log.info("Building router factory")

  def getRouter(user: XucUser): ActorRef = {

    routers.get(user.ctiUsername) match {
      case Some(ref) =>
        log.info(s"number of routers ${routers.size}")
        ref
      case _ =>
        log.info(s"creating new router for ${user.ctiUsername}")
        val newRouter = creator(CtiRouter.props(user),ActorFactory.routerPath(user.ctiUsername))
        routers = routers + (user.ctiUsername -> newRouter)
        log.info(s"number of routers ${routers.size}")
        newRouter
    }
  }
}

object CtiRouterFactory {
  lazy val factory = new CtiRouterFactory(Akka.system.actorOf)

}