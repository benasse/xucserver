package services.user

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import services.request.{BaseRequest, UserCallHistoryRequest}
import services.{ProductionActorFactory, ActorFactory}
import xivo.models.CallHistory

object CallHistoryProxy {
  def props() = Props(new CallHistoryProxy with ProductionActorFactory)
}

class CallHistoryProxy extends Actor with ActorLogging {
  this: ActorFactory =>
  var requester: Option[ActorRef] = None

  override def receive: Receive = {
    case rq: UserCallHistoryRequest => requester = Some(sender)
      context.actorSelection(callHistoryManagerURI) ! BaseRequest(self, rq)
    case history: CallHistory => requester.foreach(_ ! history)
  }
}
