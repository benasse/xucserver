package services.agent

import akka.actor._
import services.request.{BaseRequest, AgentLogout}
import services.{ProductionActorFactory, ActorFactory, XucEventBus}
import xivo.events.AgentState.AgentLoggedOut
import xivo.xuc.api.{RequestSuccess, RequestTimeout}

import scala.concurrent.duration._

object LogoutProcessor {
  def props(): Props =  Props(new LogoutProcessor() with ProductionActorFactory)
}

case class LogoutAgent(phoneNb: String)

class LogoutProcessor(val eventBus: XucEventBus = XucEventBus.bus)
  extends Actor with ActorLogging {
  this: ActorFactory =>

  log.info(s"starting $self")

  context.setReceiveTimeout(60 seconds)

  override def receive: Receive = {
    case LogoutAgent(phoneNb) =>
      context.actorSelection(configManagerURI) ! BaseRequest(self, AgentLogout(phoneNb))
      context.setReceiveTimeout(1 seconds)
      context.become(expectingResult(sender, phoneNb))

    case ReceiveTimeout =>
      log.error(s"LogoutProcessor started and not used, stopping itself")
      context.stop(self)

    case unknown =>
      log.error(s"Should not happen, in receive received: $unknown")
      context.stop(self)
  }

  def expectingResult(requester: ActorRef, phoneNb: String): Receive = {

    case AgentLoggedOut(id, _, phoneNb, _, _) =>
      log.debug(s"Agent id: $id logged out from $phoneNb")
      requester ! RequestSuccess(s"Agent id: $id logged out from $phoneNb")
      context.stop(self)

    case ReceiveTimeout =>
      log.error(s"Agent logout from phone: $phoneNb timed out")
      requester ! RequestTimeout(s"Agent logout from phoneNb: $phoneNb timed out")
      context.stop(self)

    case unknown =>
      log.debug(s"Dropped, in expectingResult received: $unknown")
  }

}


