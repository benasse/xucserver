package services.agent

import akka.actor.Props
import services.config.ConfigDispatcher._
import services.request.SetAgentQueue
import xivo.models.Agent


object  AgentInGroupMover {

  def props(groupId: Long, fromQueueId : Long, fromPenalty: Int) = Props(new AgentInGroupMover(groupId, fromQueueId, fromPenalty))

}

class AgentInGroupMover(groupId: Long, fromQueueId : Long, fromPenalty: Int) extends  AgentInGroupAction(groupId, fromQueueId, fromPenalty) {


  def actionOnAgent(agent: Agent, toQueueId: Option[Long], toPenalty: Option[Int]) = {
    context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self, SetAgentQueue(agent.id, toQueueId.get, toPenalty.get))
    if (toQueueId.get != fromQueueId)
      context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self, RemoveAgentFromQueue(agent.id, fromQueueId))
  }
}
