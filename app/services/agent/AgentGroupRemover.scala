package services.agent

import akka.actor.Props
import services.config.ConfigDispatcher.{ConfigChangeRequest, RemoveAgentFromQueue}
import xivo.models.Agent


object  AgentGroupRemover {

  def props(groupId: Long, queueId : Long, penalty: Int) = Props(new AgentGroupRemover(groupId, queueId, penalty))

}


class AgentGroupRemover(groupId: Long, queueId : Long, penalty: Int) extends  AgentInGroupAction(groupId, queueId, penalty) {


  override def actionOnAgent(agent: Agent, toQueueId: Option[Long], penalty: Option[Int]) = {
    context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self, RemoveAgentFromQueue(agent.id, queueId))
  }
}
