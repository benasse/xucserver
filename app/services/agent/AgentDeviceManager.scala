package services.agent

import akka.actor.{Props, Actor, ActorLogging}
import services.request.{PhoneKey, TurnOffKeyLight, TurnOnKeyLight}
import services.{ProductionActorFactory, ActorFactory, XucEventBus}
import xivo.events.AgentState.{AgentLoggedOut, AgentOnPause, AgentReady}

object AgentDeviceManager {
  def props: Props =  Props(new AgentDeviceManager with ProductionActorFactory)
}
class AgentDeviceManager( val eventBus: XucEventBus = XucEventBus.bus) extends Actor with ActorLogging {
  this: ActorFactory =>

  log.info(s"starting $self")

  eventBus.subscribe(self,XucEventBus.allAgentsEventsTopic)

  override def receive: Receive = {

    case AgentOnPause(id, _, phoneNb, _, _) =>
      log.debug(s"Agent: $id on pause, turn on key pause light on phone $phoneNb.")
      context.actorSelection(amiBusConnectorURI) ! TurnOnKeyLight(phoneNb, PhoneKey.Pause)

    case AgentReady(id, _, phoneNb, _, _) =>
      log.debug(s"Agent: $id ready, turn off key pause light turn on logon light on phone $phoneNb.")
      context.actorSelection(amiBusConnectorURI) ! TurnOffKeyLight(phoneNb, PhoneKey.Pause)
      context.actorSelection(amiBusConnectorURI) ! TurnOnKeyLight(phoneNb, PhoneKey.Logon)

    case AgentLoggedOut(id, _, phoneNb, _, _) =>
      log.debug(s"Agent: $id logggedout, turn off key pause light turn off logon light on phone $phoneNb.")
      context.actorSelection(amiBusConnectorURI) ! TurnOffKeyLight(phoneNb, PhoneKey.Pause)
      context.actorSelection(amiBusConnectorURI) ! TurnOffKeyLight(phoneNb, PhoneKey.Logon)

    case unknown =>
  }
}
