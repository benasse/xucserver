package services.agent

import akka.actor.Props
import services.agent.AgentInGroupAction.AgentsDestination
import services.config.ConfigDispatcher._
import services.request.SetAgentQueue
import xivo.models.Agent

object AgentFromGroupNotInQueueAdder {
  def props(groupId: Long, queueId : Long, penalty: Int) = Props(new AgentFromGroupNotInQueueAdder(groupId, queueId, penalty))
}

class AgentFromGroupNotInQueueAdder(groupId: Long, queueId : Long, penalty: Int) extends  AgentInGroupAction(groupId, queueId, penalty) {

  override def receive: Receive = {
    case AgentsDestination(toQueueId, toPenalty) =>
      context.actorSelection(configDispatcherURI) !  RequestConfig(self, GetAgentsNotInQueue(groupId, queueId))
      context.become(forAllAgents(Some(toQueueId), Some(toPenalty)))
  }

  override def actionOnAgent(agent: Agent, toQueueId: Option[Long], toPenalty: Option[Int]): Unit = {
    context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self,SetAgentQueue(agent.id,toQueueId.get,toPenalty.get))
  }
}
