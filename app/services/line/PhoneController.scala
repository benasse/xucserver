package services.line

import akka.actor._
import services.config.ConfigDispatcher._
import services.config.{AgentOnPhone, OutboundQueueNumber}
import services.request.PhoneRequest._
import services.{ActorFactory, ProductionActorFactory, XucEventBus}
import xivo.events.AgentState
import xivo.events.AgentState.{AgentLoggedOut, AgentLogin, AgentReady}
import xivo.models.Line
import xivo.phonedevices.{DeviceAdapter, DeviceAdapterFactory}

trait PhoneControllerFactory {
  def createPhoneController(lineNumber: String, line: Line, context: ActorContext): ActorRef
}

object PhoneController extends PhoneControllerFactory {
  case object Start
  val deviceAdapterFactory = new DeviceAdapterFactory
  override def createPhoneController(lineNumber: String, line: Line, context: ActorContext): ActorRef = context.actorOf(Props(
    new PhoneController(lineNumber, deviceAdapterFactory.getAdapter(line, lineNumber)) with ProductionActorFactory))
}

class PhoneController(lineNumber: String, deviceAdapter: DeviceAdapter, bus: XucEventBus = XucEventBus.bus)
  extends Actor with ActorLogging {
  this: ActorFactory =>

  var agentId: Option[Long] = None
  var queueNumber: Option[String] = None
  var enableOutboundDial = false

  override def receive: Receive = {
    case PhoneController.Start => bus.subscribe(self, XucEventBus.allAgentsEventsTopic)
      context.actorSelection(configDispatcherURI) ! RequestConfig(self, GetAgentOnPhone(lineNumber))

    case AgentOnPhone(agent, _) => agentId = Some(agent)
      context.actorSelection(configDispatcherURI) ! RequestConfig(self, GetQueuesForAgent(agent))
      context.actorSelection(configDispatcherURI) ! RequestStatus(self, agent.toInt, ObjectType.TypeAgent)

    case QueuesForAgent(queues, _) =>
      context.actorSelection(configDispatcherURI) ! RequestConfig(self, OutboundQueueNumberQuery(queues.map(_.toInt)))

    case AgentLogin(id, _, line, queues, _) if line == lineNumber => agentId = Some(id)
      context.actorSelection(configDispatcherURI) ! RequestConfig(self, OutboundQueueNumberQuery(queues))

    case OutboundQueueNumber(number) => log.info(s"Using queue $number for outbound dialing")
      queueNumber = Some(number)

    case AgentLoggedOut(_, _, line, _, _) if line == lineNumber => agentId = None
      queueNumber = None

    case AgentReady(_, _, line, queues, _) if line == lineNumber => enableOutboundDial = true
      context.actorSelection(configDispatcherURI) ! RequestConfig(self, OutboundQueueNumberQuery(queues))

    case s: AgentState if s.phoneNb == lineNumber => enableOutboundDial = false

    case Dial(destination, variables) => log.debug("websocket command dial" + destination)
      if(agentId.isDefined && queueNumber.isDefined && enableOutboundDial)
        deviceAdapter.odial(destination, agentId.get, queueNumber.get, variables, sender)
      else
        deviceAdapter.dial(destination, variables, sender)

    case Hangup => deviceAdapter.hangup(sender)

    case AttendedTransfer(destination) => deviceAdapter.attendedTransfer(destination, sender)

    case CompleteTransfer => deviceAdapter.completeTransfer(sender)

    case CancelTransfer => deviceAdapter.cancelTransfer(sender)

    case Conference => deviceAdapter.conference(sender)

    case Hold => deviceAdapter.hold(sender)

    case Answer => deviceAdapter.answer(sender)
  }
}
