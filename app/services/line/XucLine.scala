package services.line

import akka.actor.{Props, Actor}
import play.api.Logger
import services.XucEventBus
import xivo.ami.AmiBusConnector.{AgentListenNotification, LineEvent}

object LineState extends Enumeration(-2) {
  type LineState = Value
  val UNITIALIZED, HUNGUP, DOWN, RSRVD, OFFHOOK, DIALING, ORIGINATING, RINGING, UP, BUSY, DIALING_OFFHOOK, PRERING = Value
}

object CallLegDirection extends Enumeration {
  type CallLegDirection = Value
  val UNKNOWN, ORIGINATING, TERMINATING = Value
}

trait XucLineLog {
  this: XucLine =>
  val logger = Logger(getClass.getPackage.getName + ".CtiFilter." + self.path.name)
  private def logMessage(msg: String): String = s"[Line-${number}] - $msg"
  object log {
    def debug(msg: String) = logger.debug(logMessage(msg))
    def info(msg: String) = logger.info(logMessage(msg))
    def error(msg: String) = logger.error(logMessage(msg))
    def warn(msg: String) = logger.warn(logMessage(msg))
  }
}

object XucLine {
  val defaultEventBus = XucEventBus.bus
  def props(number: Int): Props = Props(new XucLine(number, defaultEventBus))
}

class XucLine(val number: Int, bus: XucEventBus) extends Actor with XucLineLog {

  def receive: Receive = {

    case event: LineEvent =>
      log.debug(s"Publishing lineEvent $event")
      bus.publish(event)

    case agentListenNotification: AgentListenNotification =>
      bus.publish(agentListenNotification)

    case unknown =>
      log.debug(s"Received unprocessed message: $unknown")
  }

}
