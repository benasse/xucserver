package services.directory

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import models.RichDirectoryResult
import models.RichFavorites
import services.directory.DirectoryTransformer.RawDirectoryResult
import xivo.services.XivoDirectory.{Favorites, FavoriteUpdated, XivoDirectoryMsg, DirLookupResult}
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent

object DirectoryTransformer {
  def props(transformer: DirectoryTransformerLogic): Props = Props(new DirectoryTransformer(transformer))

  case class RawDirectoryResult(requester: ActorRef, result: XivoDirectoryMsg)
}

class DirectoryTransformer(transformer: DirectoryTransformerLogic) extends Actor with ActorLogging {

  def receive: Receive = {
    case RawDirectoryResult(ref, result: DirLookupResult) =>
      val response: RichDirectoryResult = transformer.addStatusesDirResult(result.result)
      log.debug(s"RichDirectoryResult: ${response.headers} ${response.entries}")
      ref ! WsContent(WebSocketEvent.createEvent(response))
    case RawDirectoryResult(ref, result: Favorites) =>
      val response: RichFavorites = transformer.addStatusesFavorites(result.result)
      log.debug(s"RichDirectoryResult: ${response.headers} ${response.entries}")
      ref ! WsContent(WebSocketEvent.createEvent(response))
    case RawDirectoryResult(ref, result: FavoriteUpdated) =>
      log.debug(s"XivoDirectoryResult: $result")
      ref ! WsContent(WebSocketEvent.createEvent(result))
    case any =>
      log.debug(s"Received unprocessed message: $any")
  }
}
