package services.config

import akka.actor.{Actor, ActorLogging, ActorRef, Props, actorRef2Scala}
import com.kenshoo.play.metrics.MetricsRegistry
import models.{RichDirectoryResult, XivoUserDaoWs}
import org.xivo.cti.message._
import org.xivo.cti.model.{Meetme, PhoneHintStatus}
import play.api.libs.json._
import services.XucEventBus.XucEvent
import services.XucStatsEventBus.{Stat, StatUpdate}
import services.agent.AgentStatistic
import services.request.{UpdateConfigRequest, SetAgentQueue, XucRequest}
import services.{XucEventBus, XucStatsEventBus}
import stats.DoubleGauge
import xivo.events.AgentState
import xivo.models.XivoObject.{ObjectDefinition, ObjectType => StatObjectType}
import xivo.models._
import xivo.websocket.WebSocketEvent
import xivo.xucami.models.{EnterQueue, LeaveQueue}

import scala.collection.JavaConversions._

case class UserPhoneStatus(username: String, status: PhoneHintStatus)
case class AgentUserStatus(val agentId: Agent.Id, val userStatus: String)
case class LineConfig(id: String, number: String, line: Option[Line] = None)
case object LineConfig{
  implicit val writes =  new Writes[LineConfig] {
    def writes(lc: LineConfig) = Json.obj(
      "id" -> lc.id,
      "name" -> lc.line.fold("")(l => l.name),
      "password" -> lc.line.fold("")(l => l.password.getOrElse("")),
      "xivoIp" -> lc.line.fold("")(l => l.xivoIp)
    )
  }
}
case class OutboundQueueNumber(number: String)
case class AgentOnPhone(agentId: Agent.Id, phoneNumber: String)

object ConfigDispatcher {

  def props(configRepository: ConfigRepository, lineManager: ActorRef, agentManager:ActorRef): Props =
    Props(new ConfigDispatcher(configRepository, lineManager, agentManager) with ConfigInitializer with XivoUserDaoWs with AgentLoginStatusDaoDb with MetricCache)

  import services.config.AgentDirectoryEntry.agentDirectoryEntryWrites

  implicit val agentDirectoryWrites = new Writes[AgentDirectory] {
    def writes(c: AgentDirectory): JsValue = {
      Json.obj(
        "directory" -> Json.toJson(c.directory))
    }
  }

  object ObjectType extends Enumeration {
    type ObjectType = Value
    val TypeQueue = Value
    val TypeAgent = Value
    val TypeQueueMember = Value
    val TypeUser = Value
    val TypePhone = Value
    val TypeLine = Value
    val TypeAgentGroup = Value
    val TypeMeetme = Value
    val TypeCallback = Value

  }

  case class AgentDirectory(directory: List[AgentDirectoryEntry])
  object AgentDirectory {
    def toJson(agentDirectory : AgentDirectory) = Json.toJson(agentDirectory)
  }

  import services.config.ConfigDispatcher.ObjectType._
  case class RequestStatus(requester: ActorRef, id: Int, objectType: ObjectType)

  case class ConfigChangeRequest(requester: ActorRef, update: UpdateConfigRequest)

  case class RemoveAgentFromQueue(agentId : Agent.Id, queueId : Long) extends UpdateConfigRequest


  class ConfigQuery extends  XucRequest
  case object GetAgentStates extends ConfigQuery {
    def validate(json: JsValue) = JsSuccess(GetAgentStates)
  }

  case object GetAgentStatistics extends ConfigQuery
  case object GetAgentDirectory extends ConfigQuery {
    def validate(json: JsValue) = JsSuccess(GetAgentDirectory)
  }

  case class GetConfig(objectType: ObjectType) extends ConfigQuery
  case class GetAgents(groupId: Long, queueId: Long, penalty: Int) extends  ConfigQuery
  case class GetAgentsNotInQueue(groupId: Long, queueId: Long) extends ConfigQuery
  case class GetQueuesForAgent(agentId: Agent.Id) extends ConfigQuery

  case class RequestConfig(requester: ActorRef, request: ConfigQuery)
  case class RequestConfigForNb(requester: ActorRef, request: ConfigQuery, phoneNb: Option[String])

  object GetConfig {
    def validate(json: JsValue) = json.validate[GetConfig]

    def apply(objectType: String): GetConfig = objectType match {
      case "queue" => GetConfig(TypeQueue)
      case "agent" => GetConfig(TypeAgent)
      case "queuemember" => GetConfig(TypeQueueMember)
      case "line" => GetConfig(TypeLine)
      case unknown => GetConfig(TypeAgent)
    }
    implicit val GetConfigRead : Reads[GetConfig] = (JsPath \ "objectType").read[String].map(GetConfig.apply)
  }
  case class GetList(objectType: ObjectType) extends ConfigQuery
  object GetList {
    def apply(objectType: String): GetList = objectType match {
      case "queue" => GetList(TypeQueue)
      case "agent" => GetList(TypeAgent)
      case "agentgroup" => GetList(TypeAgentGroup)
      case "queuemember" => GetList(TypeQueueMember)
      case "meetme" => GetList(TypeMeetme)
      case unknown => GetList(TypeAgent)
    }
    def validate(json: JsValue) = json.validate[GetList]

    implicit val GetListRead : Reads[GetList] = (JsPath \ "objectType").read[String].map(GetList.apply)
  }
  case class LineConfigQueryByNb(number: String) extends ConfigQuery
  case class LineConfigQueryById(id: Int) extends ConfigQuery

  case class OutboundQueueNumberQuery(queues: List[Int]) extends ConfigQuery

  case class GetAgentOnPhone(phoneNumber: String) extends ConfigQuery

  case class QueueList(queues: List[QueueConfigUpdate])
  case class AgentList(agents: List[Agent])
  case class AgentGroupList(agentGroups: List[AgentGroup])
  case class AgentQueueMemberList(agentQueueMembers: List[AgentQueueMember])
  case class MeetmeList(meetmes: List[Meetme])
  case class QueuesForAgent(queueIds: List[Long], agentId: Agent.Id)

  case class GetQueueCalls(queueId: Long) extends ConfigQuery
}

trait MetricUpdate {
  def updateOrCreateMetric(queueId:Int, statName: String, statValue: Double)

  def prefix(queueId: Int, statName:String) = s"Queue.$queueId.$statName"
}

trait MetricCache extends MetricUpdate {

  def updateOrCreateMetric(queueId:Int, statName: String, statValue: Double) = (MetricsRegistry.defaultRegistry.getGauges.get(prefix(queueId, statName)) match {
    case gauge: DoubleGauge => gauge
    case null => MetricsRegistry.defaultRegistry.register(prefix(queueId, statName), new DoubleGauge())
  }).setValue(statValue)
}

class ConfigDispatcher(val configRepository: ConfigRepository,
                       val lineManager: ActorRef,
                       val agentManager: ActorRef,
                       val eventBus: XucEventBus = XucEventBus.bus,
                       val statsBus: XucStatsEventBus = XucStatsEventBus.statEventBus,
                       val agentQueueMemberFactory : AgentQueueMemberFactory = AgentQueueMember,
                       val agentGroupFactory : AgentGroupFactory = AgentGroup) extends Actor with ActorLogging  {
  this: ConfigInitializer with MetricUpdate =>
  import services.config.ConfigDispatcher.ObjectType._
  import services.config.ConfigDispatcher._
  import services.config.ConfigManager.PublishUserPhoneStatuses

  log.info("ConfigDispatcher started " + self)
  eventBus.subscribe(self, XucEventBus.allAgentsEventsTopic)
  eventBus.subscribe(self, XucEventBus.allAgentsStatsTopic)

  def configDispatcherReceive: Receive = {

    case phoneConfig: PhoneConfigUpdate =>
      configRepository.onPhoneConfigUpdate(phoneConfig)
      try {
        lineManager ! CreateLine(phoneConfig.getNumber.toInt)
      } catch {
        case e: NumberFormatException =>
          log.error(s"Impossible to create line (number cannot be parsed) for phone ${phoneConfig.getNumber}")
      }

    case phoneStatus: PhoneStatusUpdate =>
      configRepository.onPhoneStatusUpdate(phoneStatus)
      log.debug(s"phone status : $phoneStatus")
      configRepository.getUserPhoneStatus(phoneStatus) foreach (sender ! _)

    case userStatus: UserStatusUpdate =>
      log.debug(s"user status : $userStatus")
      configRepository.getAgentUserStatus(userStatus).foreach(sender ! _)

    case queueStats: QueueStatistics =>
      log.debug(s"queue statistics : $queueStats")
      import scala.collection.JavaConversions._
      val stats = queueStats.getCounters.map(counter => {
          updateOrCreateMetric(queueStats.getQueueId, counter.getStatName.name(),counter.getValue.toDouble)
          Stat(counter.getStatName.name(), counter.getValue.toDouble)
        }
        ).toList
      statsBus.publish(StatUpdate(ObjectDefinition(StatObjectType.Queue, Some(queueStats.getQueueId)), stats))
      log.debug(s"Published: ${StatUpdate(ObjectDefinition(StatObjectType.Queue, Some(queueStats.getQueueId)), stats)}")


    case PublishUserPhoneStatuses =>
      configRepository.getAllUserPhoneStatuses.foreach(sender ! _)

    case directoryResult: DirectoryResult =>

      val result: RichDirectoryResult = configRepository.addStatuses(directoryResult)
      log.debug("********** " + result.getEntries + " >>>>>>>>>>>" + Json.toJson(result))

      sender ! WebSocketEvent.createEvent(result)

    case RequestConfig(requester, lcr: LineConfigQueryByNb) =>
      log.debug(s"$requester config request : $lcr")
      configRepository.getLineConfig(lcr) match {
        case Some(lineConfig) => {
          requester ! lineConfig
        }
        case None => log.error(s"unable to find any config for nb ${lcr.number}")
      }

    case RequestConfig(requester, lcr: LineConfigQueryById) =>
      log.debug(s"$requester config request : $lcr")
      configRepository.getLineConfig(lcr) match {
        case Some(lineConfig) => requester ! lineConfig
        case None => log.error(s"unable to find any config for id ${lcr.id}")
      }

    case RequestConfigForNb(requester, GetConfig(objectType), phoneNb) =>
      log.debug(s"$requester config request : $objectType")
      objectType match {
        case TypeAgent => requester ! configRepository.getAgents().foreach(requester ! _)
        case TypeQueue => configRepository.getQueues.foreach(requester ! _)
        case TypeQueueMember => configRepository.getAgentQueueMembers.foreach(requester ! _)
        case TypeLine => phoneNb match {
          case Some(nb) => self ! RequestConfig(requester, LineConfigQueryByNb(nb))
          case None => log.error("Unable to get line for unknown phoneNb")
        }
        case unknown =>
      }

    case RequestConfig(requester, GetList(objectType)) =>
      log.debug(s"$requester config list request : $objectType")
      objectType match {
        case TypeQueue => requester ! QueueList(configRepository.getQueues)
        case TypeAgent => requester ! AgentList(configRepository.getAgents)
        case TypeAgentGroup => requester ! AgentGroupList(agentGroupFactory.all())
        case TypeQueueMember => requester ! AgentQueueMemberList(configRepository.getAgentQueueMembers)
        case TypeMeetme => requester ! MeetmeList(configRepository.getMeetmeList)
        case unknown =>
      }

    case RequestConfig(requester, GetAgents(groupId, queueId, penalty)) =>
      requester ! AgentList(configRepository.getAgents(groupId, queueId, penalty))

    case RequestConfig(requester, GetAgentsNotInQueue(groupId, queueId)) =>
      requester ! AgentList(configRepository.getAgentsNotInQueue(groupId, queueId))

    case RequestConfig(requester, OutboundQueueNumberQuery(queueIds)) =>
      configRepository.getOutboundQueueNumber(queueIds).foreach(requester ! OutboundQueueNumber(_))

    case RequestStatus(requester, id, TypeAgent) => configRepository.getAgentState(id).foreach(requester ! _)

    case RequestStatus(requester, id, TypeUser) => configRepository.getUserStatus(id).foreach(requester ! _)

    case RequestStatus(requester, id, TypePhone) => configRepository.getPhoneStatus(id).foreach(requester ! _)

    case agentState: AgentState =>
      log.debug(s"Receiving AgentStatus: $agentState ${agentState.cause}")
      configRepository.onAgentState(agentState)

    case agentQueueMember: AgentQueueMember if(!configRepository.queueMemberExists(agentQueueMember)) =>
      configRepository.updateOrAddQueueMembers(agentQueueMember)
      eventBus.publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeQueueMember),agentQueueMember))

    case RequestConfig(requester, GetAgentDirectory) =>
      val dir = AgentDirectory(configRepository.getAgentDirectory)
      log.debug(s"Agent directory: $dir")
      requester ! dir

    case RequestConfig(requester, GetAgentStates) => configRepository.getAgentStates.foreach(requester ! _)

    case RequestConfig(requester, GetAgentStatistics) => configRepository.getAgentStatistics.foreach(requester ! _)

    case RequestConfig(requester, GetQueueCalls(queueId)) => requester ! configRepository.getQueueCalls(queueId)

    case RequestConfig(requester, GetAgentOnPhone(phoneNumber)) => configRepository.getAgentLoggedOnPhoneNumber(phoneNumber).foreach(
      requester ! AgentOnPhone(_, phoneNumber))

    case RequestConfig(requester, GetQueuesForAgent(agentId)) => requester ! QueuesForAgent(configRepository.getQueuesForAgent(agentId), agentId)

    case ConfigChangeRequest(requester, SetAgentQueue(agentId, queueId, penalty)) =>
      agentQueueMemberFactory.setAgentQueue(agentId, queueId, penalty).foreach(self ! _)

    case ConfigChangeRequest(requester, RemoveAgentFromQueue(agentId, queueId)) =>
      agentQueueMemberFactory.removeAgentFromQueue(agentId, queueId)

    case meetmeUpdate: MeetmeUpdate => configRepository.onMeetmeUpdate(meetmeUpdate.getMeetmeList.toList)
      eventBus.publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeMeetme),MeetmeList(meetmeUpdate.getMeetmeList.toList)))

    case agStat: AgentStatistic => configRepository.updateAgentStatistic(agStat)

    case EnterQueue(queue, uniqueId, queueCall) => configRepository.onQueueCallReceived(queue, uniqueId, queueCall)
      configRepository.getQueueCalls(queue).foreach(eventBus.publish)

    case LeaveQueue(queue, uniqueId) => configRepository.onQueueCallFinished(queue, uniqueId)
      configRepository.getQueueCalls(queue).foreach(eventBus.publish)

    case RefreshAgent(agentId) => configRepository.loadAgent(agentId)
      configRepository.getAgent(agentId).foreach(eventBus.publish)

    case unknown => log.debug(s"Unknown message received: $unknown from: $sender")
  }
  def receive = configInitializerReceive orElse configDispatcherReceive


}
