package services.config

import models._
import org.json.JSONObject
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{AgentIds, DirectoryResult, PhoneConfigUpdate, PhoneIdsList, PhoneStatusUpdate, QueueConfigUpdate, QueueIds, QueueMemberConfigUpdate, QueueMemberIds, QueueMemberRemoved, UserConfigUpdate, UserIdsList, UserStatusUpdate}
import org.xivo.cti.model.{DirectoryEntry, Meetme, PhoneHintStatus}
import play.api.Logger
import play.api.libs.concurrent.Akka
import play.api.libs.functional.syntax._
import play.api.libs.json._
import services.agent.{AgentStatistic, Statistic}
import services.config
import services.config.ConfigDispatcher.{LineConfigQueryById, LineConfigQueryByNb}
import xivo.events.AgentState
import xivo.events.AgentState.{AgentLoggedOut, AgentLogin}
import xivo.models.Agent._
import xivo.models._
import xivo.services.TokenRetriever
import xivo.xucami.models.QueueCall

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.collection.mutable.Buffer

import play.api.Play.current

case class RichPhone(val phoneNumber: String, val currentStatus: PhoneHintStatus)
case class AgentDirectoryEntry(agent: Agent, state: AgentState)
object AgentDirectoryEntry {
  implicit val agentDirectoryEntryWrites: Writes[AgentDirectoryEntry] = (
    (__ \ "agent").write[Agent] and
      (__ \ "agentState").write[AgentState])(unlift(AgentDirectoryEntry.unapply))
}

trait AgentRepository {
  this: ConfigRepository with QueueMemberRepository  =>

  private[config] var agents:Map[Agent.Id, Agent] = Map()
  private[config] var agentStatistics:Map[Agent.Id,AgentStatistic] = Map()

  def addAgent(agent: Agent) = agents = agents + (agent.id -> agent)

  def loadAgent(id: Agent.Id) = {
    agentFactory.getById(id).map(addAgent(_))
  }

  def getAgent(agentId: Agent.Id): Option[Agent] =  agents.get(agentId)

  def getAgent(number: Agent.Number): Option[Agent] = agents.values.toList.filter(agent => agent.number == number).headOption

  def getAgents(): List[Agent] = agents.values.toList


  def getAgents(groupId: Long, queueId: Long, penalty : Int) : List[Agent] = {
    for {
      ag <- agents.values.toList.filter (_.groupId == groupId)
      qm <- agentQueueMembers.filter(_ == AgentQueueMember(ag.id,queueId, penalty))
    } yield(ag)
  }

  def getAgentsNotInQueue(groupId: Long, queueId: Long):List[Agent] = {
    for {
      agent <- agents.values.toList.filter (_.groupId == groupId)
      if (agentNotInQueue(agent, queueId))
    } yield(agent)
  }

  def updateAgentStatistic(agStat: AgentStatistic)= {
    def merge(stats1:List[Statistic], stats2: List[Statistic]): List[Statistic] = {
      (Map[String, Statistic]() /: (stats1 ::: stats2)) {
        case(map, Statistic(name,value)) =>  map.updated(name,Statistic(name,value))
      }.values.toList
    }
    agentStatistics.get(agStat.agentId) match {
      case Some(repoAgStat) =>
        merge(repoAgStat.statistics, agStat.statistics)
        agentStatistics = agentStatistics + (agStat.agentId -> AgentStatistic(agStat.agentId, merge(repoAgStat.statistics, agStat.statistics)))
      case None => agentStatistics = agentStatistics + (agStat.agentId -> agStat)
    }
  }

  def getAgentStatistics:List[AgentStatistic] = agentStatistics.values.toList

  def groupIdForAgent(id: Id): Option[Long] = agents.get(id).map(_.groupId)
}

trait QueueMemberRepository {
  this: config.ConfigRepository =>

  private[config] var agentQueueMembers:List[AgentQueueMember] = List()

  def getAgentQueueMembers(): List[AgentQueueMember] = agentQueueMembers

  def loadAgentQueueMembers() =  agentQueueMembers = agentQueueMemberFactory.all()

  def agentNotInQueue(agent: Agent, queueId: Long ) = !agentQueueMembers.exists{ case AgentQueueMember(agentId, qId, _) => agentId == agent.id && qId == queueId }

  def queueMemberExists(agqm : AgentQueueMember) = agentQueueMembers.contains(agqm)

  private def filterQueueMember(agentId : Agent.Id, queueId : Long) = agentQueueMembers.filter(agqm => agqm.agentId != agentId || queueId != agqm.queueId)

  def updateOrAddQueueMembers(agentQueueMember : AgentQueueMember) =
    agentQueueMembers = agentQueueMember :: filterQueueMember(agentQueueMember.agentId,agentQueueMember.queueId)

  def removeQueueMember(qmRemoved: QueueMemberRemoved) = {
    for {
      agent <- getAgent(qmRemoved.getAgentNumber)
      queueConfig <- getQueue(qmRemoved.getQueueName)
    } agentQueueMembers = filterQueueMember(agent.id, queueConfig.getId.toLong)
  }

  def getAgentQueueMember(qMconfig: QueueMemberConfigUpdate): Option[AgentQueueMember] =
    for {
      agent <- getAgent(qMconfig.getAgentNumber)
      queueConfig <- getQueue(qMconfig.getQueueName)
    } yield AgentQueueMember(agent.id, queueConfig.getId.toLong, qMconfig.getPenalty)

  def getAgentQueueMember(qMRemoved: QueueMemberRemoved): Option[AgentQueueMember] =
    for {
      agent <- getAgent(qMRemoved.getAgentNumber)
      queueConfig <- getQueue(qMRemoved.getQueueName)
    } yield AgentQueueMember(agent.id, queueConfig.getId.toLong, -1)

  def getQueuesForAgent(agentId: Agent.Id): List[Long] = agentQueueMembers.filter(_.agentId == agentId).map(_.queueId)

}

trait QueueRepository {
  this: config.ConfigRepository =>
  val OutBoundQueuePrefix = "out"
  protected[config] var queues: Map[Long, QueueConfigUpdate] = Map()

  private def isAnOutboundQueue(qc:QueueConfigUpdate) = qc.getName.startsWith(OutBoundQueuePrefix)

  def getQueues(): List[QueueConfigUpdate] = queues.values.toList

  def getQueue(queueId : Long):Option[QueueConfigUpdate] = queues.get(queueId)

  def getQueue(name : String):Option[QueueConfigUpdate] = queues.values.filter(qcf => qcf.getName == name).headOption

  def updateQueueConfig(qConfig: QueueConfigUpdate) = queues += (qConfig.getId().toLong -> qConfig)

  def getOutboundQueueNumber(queueIds: List[Int]):Option[String] = queues.filterKeys(Set(queueIds:_*)).values.filter(isAnOutboundQueue(_)).map(_.getNumber).headOption
}

trait LineRepository {
  this: ConfigRepository =>
  var userLines: Map[Long, Line] = Map()
  var agentLines : Map[Agent.Id, Line] = Map()
  var linePhoneNbs: Map[Line.Id, String] = Map()
  var linesUser: Map[Line.Id, Long] = Map()

  def getLineForPhoneNb(phoneNb: String) = linePhoneNbs.filter(_._2 == phoneNb).keys.headOption

  def getLineForUser(userId : Long):Option[Line]= userLines.get(userId)
  def getLineForAgent(agentId :Agent.Id):Option[Line]= agentLines.get(agentId)
  def getLineConfig(lcr: LineConfigQueryByNb): Option[LineConfig] =
    getLineForPhoneNb(lcr.number) map (id => LineConfig(id.toString, lcr.number, lineFactory.get(id.toInt)))

  def getLineConfig(lcr: LineConfigQueryById): Option[LineConfig] =
    linePhoneNbs.get(lcr.id) map (nb => LineConfig(lcr.id.toString, nb, lineFactory.get(lcr.id)))

  protected def updateAgentLine(agentId: Agent.Id, line: Line) = agentLines = agentLines + (agentId -> line)
  protected[config] def updateUserLine(userId: Long, line: Line) = userLines = userLines + (userId -> line)
  protected[config] def updateLinePhoneNb(lineId: Line.Id, phoneNb: String) = {
    log.debug(s"updating phone $phoneNb with id $lineId")
    linePhoneNbs.filter(_._2 == phoneNb).keys.headOption.map {
      id => {
        linePhoneNbs = linePhoneNbs -id
        log.warn(s"phone $phoneNb already exists with id $lineId : removing")
      }
    }

    linePhoneNbs = linePhoneNbs  + (lineId -> phoneNb)
  }
  protected[config] def updateLineUser(lineId: Line.Id, userId: Long) = linesUser = linesUser + (lineId -> userId)

  def loadUserLine(userId: Long, lineId: Long) = lineFactory.get(lineId.toInt).map(line => updateUserLine(userId, line))
  def loadAgentLine(agentId: Long, lineId: Long) = lineFactory.get(lineId.toInt).map(line => updateAgentLine(agentId, line))

}

trait UserRepository {
  this: ConfigRepository =>
  protected[config] var userAgents: Map[Long, Agent.Id] = Map()
  protected[config] var users: Map[Long, XivoUser] = Map()

  def updateUser(user: XivoUser) = users = users + (user.id -> user)

  def getUser(id: Long): Option[XivoUser] = users.get(id)
  def getUser(username: String): Option[XivoUser] = users.values.find(_.username == Some(username))
  def getUser(username: String, password: String): Option[XivoUser] = users.filter{case(id,user) => (user.username == Some(username) && user.password == Some(password))}.values.toList.headOption

  protected def updateUserAgent(userId:Long, agentId: Agent.Id)= userAgents = userAgents + (userId -> agentId)
  protected def getUserIdForAgent(agentId: Agent.Id) = userAgents.filter(_._2==agentId).keys.headOption

}
object ConfigRepository {
  var repo = new ConfigRepository
}

class ConfigRepository(val messageFactory: MessageFactory = new MessageFactory(),
                       val lineFactory: LineFactory = Line,
                       val agentFactory: AgentFactory = Agent,
                       val agentQueueMemberFactory : AgentQueueMemberFactory = AgentQueueMember)
                        extends AgentRepository  with QueueMemberRepository
                        with QueueRepository with LineRepository with UserRepository {

  type QueueCalls = Map[String, QueueCall]

  val LoggerName = getClass.getName
  val log = Logger(LoggerName)

  var richPhones: Map[String, PhoneHintStatus] = Map()
  var agentStatuses: Map[Agent.Id, AgentState] = Map()
  var userStatuses: Map[Int, UserStatusUpdate] = Map()
  var meetmes: List[Meetme] = List()
  var queueCalls: Map[String, QueueCalls] = Map()

  def onMeetmeUpdate(meetmes: List[Meetme]) = this.meetmes = meetmes

  def onPhoneIds(list: PhoneIdsList): List[JSONObject] =
  buildRequestsFromids(list.getIds, messageFactory.createGetPhoneConfig) ::: buildRequestsFromids(list.getIds, messageFactory.createGetPhoneStatus)

  def onUserIds(list: UserIdsList): List[JSONObject] =
  buildRequestsFromids(list.getIds, messageFactory.createGetUserConfig)

  def onQueueIds(list: QueueIds): List[JSONObject] =
  buildRequestsFromids(list.getIds, messageFactory.createGetQueueConfig)

  def requestStatusOnAgentIds(list: AgentIds): List[JSONObject] =
  buildRequestsFromids(list.getIds, messageFactory.createGetAgentStatus)

  def onQueueMemberIds(qmIds: QueueMemberIds): List[JSONObject] =
  buildRequestsFromids(qmIds.getMemberIds, messageFactory.createGetQueueMemberConfig)

  private def buildRequestsFromids[T](jids: java.util.List[T], factory: (String => JSONObject)): List[JSONObject] = {
    val ids = jids.asScala.toList
    for (id <- ids) yield factory(id.toString);
  }

  def onPhoneConfigUpdate(phoneConfig: PhoneConfigUpdate) = {
    Logger(LoggerName).debug("Phone config updated for line [" + phoneConfig.getId() + "]")
    updateLinePhoneNb(phoneConfig.getId().toLong, phoneConfig.getNumber())
    updateLineUser (phoneConfig.getId().toLong,  phoneConfig.getUserId().toLong)
  }
  def onUserConfigUpdate(userConfig: UserConfigUpdate) = {
    if (userConfig.getAgentId != 0) {
      updateUserAgent(userConfig.getUserId, userConfig.getAgentId)
    }
  }

  def getAgentUserStatus(userStatus: UserStatusUpdate): Option[AgentUserStatus] = {
    userStatuses = userStatuses + (userStatus.getUserId() -> userStatus)
    userAgents.get(userStatus.getUserId).map(agentId => AgentUserStatus(agentId, userStatus.getStatus))
  }

  def getUserStatus(userId: Int): Option[UserStatusUpdate] = userStatuses.get(userId)

  def onPhoneStatusUpdate(phoneStatus: PhoneStatusUpdate) = {
    Logger(LoggerName).debug("Phone Status updated for line [" + phoneStatus.getLineId() + "]:<" + phoneStatus.getHintStatus() + ">")
    if (phoneStatus.getHintStatus() != "") {
      linePhoneNbs.get(phoneStatus.getLineId().toLong) match {
        case Some(phoneNumber) =>
          richPhones = richPhones + (phoneNumber -> PhoneHintStatus.getHintStatus(Integer.valueOf(phoneStatus.getHintStatus())))
        case None => Logger(LoggerName).debug("Status not updated, no line in configuration")
      }
    }
  }

  def getMeetmeList: List[Meetme] = meetmes

  def addStatuses(directoryResult: DirectoryResult): RichDirectoryResult = {
    val result = new RichDirectoryResult(directoryResult.getHeaders().toList)
    val entries: Buffer[DirectoryEntry] = directoryResult.getEntries()
    entries.foreach(entry => {
      result.add(enrich(entry))
    })
    Logger(LoggerName).debug("Status added " + result.getEntries)
    result
  }

  def getUserPhoneStatus(phoneStatus: PhoneStatusUpdate): Option[UserPhoneStatus] = {
    for {
      userid <- linesUser.get(phoneStatus.getLineId().toLong)
      user <- users.get(userid)
      if (phoneStatus.getHintStatus() != "")
    } yield UserPhoneStatus(user.username.getOrElse(""), PhoneHintStatus.getHintStatus(Integer.valueOf(phoneStatus.getHintStatus())))
  }

  def getPhoneStatus(phoneId: Int): Option[PhoneStatusUpdate] = {
    for {
       phoneNumber <- linePhoneNbs.get(phoneId)
       hint <- richPhones.get(phoneNumber)
    } yield {
      val ps = new PhoneStatusUpdate
      ps.setLineId(Integer.valueOf(phoneId))
      ps.setHintStatus(hint.getHintStatus.toString)
      ps
    }
  }

  def getAllUserPhoneStatuses: List[UserPhoneStatus] = {
    (for {
      (lineId, userId) <- linesUser
      phoneNumber <- linePhoneNbs.get(lineId.toLong)
      user <- users.get(userId)
      hint <- richPhones.get(phoneNumber)
    } yield (UserPhoneStatus(user.username.getOrElse(""), hint))).toList
  }

  def getAgentQueueMemberRequest(agentNumber: String): List[JSONObject] = {
    (for {
      agent <- agents.values.toList.filter(agent => agent.number == agentNumber)
      agqm <- agentQueueMembers.filter(_.agentId == agent.id)
      queue <- queues.values.filter(_.getId == agqm.queueId)
    } yield (messageFactory.createGetQueueMemberConfig(s"Agent/${agent.number},${queue.getName}"))).toList
  }

  def onAgentState(agentState: AgentState): Unit = {
    agentStatuses += (agentState.agentId -> agentState)
    agentState match {
      case agentLogin: AgentLogin =>
        getLineForPhoneNb(agentLogin.phoneNb) map {lineId =>
          lineFactory.get(lineId) map { line =>
            updateAgentLine(agentLogin.id, line)
            getUserIdForAgent(agentLogin.id) map (userId => updateUserLine(userId, line))
          }
        }
      case _ =>
    }
  }


  def getAgentLoggedOnPhoneNumber(phoneNumber : String): Option[Agent.Id] = {
    agentStatuses.filter { case (id, state) =>
      state match {
        case state : AgentLoggedOut  => false
        case _ if(phoneNumber != "") => (state.phoneNb == phoneNumber)
        case _ => false
      }}.keys.headOption
  }

  def getAgentDirectory: List[AgentDirectoryEntry] = {
    (for {
      agent <- agents.values.toList
      agentState <- agentStatuses.get(agent.id)
    } yield ((AgentDirectoryEntry(agent, agentState)))).toList
  }


  def getAgentStates: List[AgentState] = agentStatuses.values.toList

  def getAgentState(agentId: Agent.Id): Option[AgentState] = agentStatuses.get(agentId)

  private def enrich(entry: DirectoryEntry): RichEntry = {
    val phoneStatusesForEntry = (for (field <- entry.getFields()) yield richPhones.get(field)).flatten.toList
    RichEntry(phoneStatusesForEntry.headOption.getOrElse(PhoneHintStatus.UNEXISTING), entry.getFields().toBuffer)
  }

  def onQueueCallReceived(queue: String, uniqueId: String, queueCall: QueueCall): Unit = {
    queueCalls.get(queue) match {
      case None => queueCalls = queueCalls + (queue -> Map(uniqueId -> queueCall))
      case Some(calls) => queueCalls = queueCalls + (queue -> (calls + (uniqueId -> queueCall)))
    }

  }

  def onQueueCallFinished(queue: String, uniqueId: String) = {
    queueCalls.get(queue) match {
      case None => log.warn(s"Received a call finished notification on the queue $queue which is not resistered")
      case Some(calls) => queueCalls = queueCalls + (queue -> (calls - uniqueId))
    }
  }

  def getQueueCalls(queueId: Long): QueueCallList = queues.get(queueId) match {
    case Some(queue) => queueCalls.get(queue.getName) match {
      case Some(calls) => QueueCallList(queueId, calls.values.toList)
      case None => QueueCallList(queueId, List())
    }
    case None => QueueCallList(queueId, List())
  }

  def getQueueCalls(queueName: String): Option[QueueCallList] = queues.values.find(q => q.getName.equals(queueName))
    .map(queue => getQueueCalls(queue.getId.toInt))

  def agentFromUsername(username: String): Option[Agent] = getUser(username) match {
    case None => None
    case Some(user) => userAgents.get(user.id) match {
      case None => None
      case Some(agentId) => agents.get(agentId)
    }
  }


  def interfaceFromUsername(username: String): Option[String] = getUser(username).flatMap(user =>
    userLines.get(user.id).map(_.interface))

  def phoneNumberForUser(username: String): Option[String] = getUser(username)
    .flatMap(u => getLineForUser(u.id))
    .flatMap(l => linePhoneNbs.get(l.id))

  def userNameFromPhoneNb(nb: String): Option[String] = linePhoneNbs.find(t => t._2 == nb)
      .flatMap(t => linesUser.get(t._1))
      .flatMap(userId => users.get(userId))
      .map(_.username.getOrElse(""))

}
