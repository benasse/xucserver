package services.request

import play.api.libs.json._
import services.BrowserMessage
import services.config.ConfigDispatcher._
import PhoneRequest._

class RequestDecoder {

  val decoders: Map[String, JsValue => JsResult[XucRequest]] = Map(
    "addAgentsInGroup" -> AddAgentInGroup.validate,
    "addAgentsNotInQueueFromGroupTo" -> AddAgentsNotInQueueFromGroupTo.validate,
    "addFavorite" -> AddFavorite.validate,
    "agentLogin" -> AgentLoginRequest.validate,
    "answer" -> Answer.validate,
    "attendedTransfer" -> AttendedTransfer.validate,
    "busyFwd" -> BusyForward.validate,
    "cancelTransfer" -> CancelTransfer.validate,
    "completeTransfer" -> CompleteTransfer.validate,
    "conference" -> Conference.validate,
    "dial" -> Dial.validate,
    "directoryLookUp" -> DirectoryLookUp.validate,
    "getAgentDirectory" -> GetAgentDirectory.validate,
    "getAgentCallHistory" -> GetAgentCallHistory.validate,
    "getAgentStates" -> GetAgentStates.validate,
    "getCallbackLists" -> GetCallbackLists.validate,
    "getConfig" -> GetConfig.validate,
    "getFavorites" -> GetFavorites.validate,
    "getList" -> GetList.validate,
    "getUserCallHistory" -> GetUserCallHistory.validate,
    "hangup" -> Hangup.validate,
    "hold" -> Hold.validate,
    "inviteConferenceRoom" -> InviteConferenceRoom.validate,
    "listenAgent" -> AgentListen.validate,
    "monitorPause" -> MonitorPause.validate,
    "monitorUnpause" -> MonitorUnpause.validate,
    "moveAgentsInGroup" -> MoveAgentInGroup.validate,
    "naFwd" -> NaForward.validate,
    "releaseCallback" -> ReleaseCallback.validate,
    "removeAgentGroupFromQueueGroup" -> RemoveAgentGroupFromQueueGroup.validate,
    "removeFavorite" -> RemoveFavorite.validate,
    "setAgentGroup" -> SetAgentGroup.validate,
    "setAgentQueue" -> SetAgentQueue.validate,
    "startCallback" -> StartCallback.validate,
    "subscribeToAgentEvents" -> SubscribeToAgentEvents.validate,
    "subscribeToAgentStats" -> SubscribeToAgentStats.validate,
    "subscribeToQueueCalls" -> SubscribeToQueueCalls.validate,
    "subscribeToQueueStats" -> SubscribeToQueueStats.validate,
    "takeCallback" -> TakeCallback.validate,
    "uncFwd" -> UncForward.validate,
    "updateCallbackTicket" -> UpdateCallbackTicket.validate,
    "unSubscribeToQueueCalls" -> UnSubscribeToQueueCalls.validate
  )

  def decodeCmd(message: JsValue): XucRequest = {
    message \ XucRequest.Cmd match {
      case JsString(cmd) if decoders.contains(cmd) =>
        decoders(cmd)(message) match {
          case s: JsSuccess[XucRequest] => s.get
          case e: JsError => InvalidRequest(s"${XucRequest.errors.validation} $e", message.toString())
        }
      case s: JsString => BrowserMessage(play.libs.Json.parse(message.toString()))
      case _ => InvalidRequest(XucRequest.errors.invalidNoCmd, message.toString())
    }
  }

  def decode(message: JsValue): XucRequest = {

    message \ XucRequest.ClassProp match {
      case JsString(XucRequest.PingClass) => Ping
      case JsString(XucRequest.WebClass) => decodeCmd(message)
      case _ => InvalidRequest(XucRequest.errors.invalid, message.toString())
    }
  }

}
