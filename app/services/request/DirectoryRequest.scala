package services.request

import play.api.libs.json.{JsSuccess, Json, JsValue}

class DirectoryRequest extends XucRequest

case class DirectoryLookUp(term: String) extends DirectoryRequest
object DirectoryLookUp {
  def validate(json: JsValue) = json.validate[DirectoryLookUp]
  implicit val DirectoryLookUpRead = Json.reads[DirectoryLookUp]
}

case object GetFavorites extends DirectoryRequest {
  def validate(json: JsValue) = JsSuccess(GetFavorites)
}

case class AddFavorite(contactId: String, source: String) extends DirectoryRequest
object AddFavorite {
  def validate(json: JsValue) = json.validate[AddFavorite]
  implicit val SetFavoriteRead = Json.reads[AddFavorite]
}

case class RemoveFavorite(contactId: String, source: String) extends DirectoryRequest
object RemoveFavorite {
  def validate(json: JsValue) = json.validate[RemoveFavorite]
  implicit val RemoveFavoriteRead = Json.reads[RemoveFavorite]
}
