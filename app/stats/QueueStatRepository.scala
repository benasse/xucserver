package stats

import akka.actor.ActorRef
import akka.actor.ActorContext
import akka.actor.Props

class QueueStatRepository {
  protected[stats] var repo:Map[Int,ActorRef] = Map()
  
  def getQueueStat(queueId: Int): Option[ActorRef] = repo.get(queueId)
  def getAllStats : List[ActorRef] = repo.map(_._2).toList

  def createQueueStat(queueId:Int, context : ActorContext):ActorRef = {
    val actorRef = context.actorOf(Props(QueueStatManager(queueId)),s"${queueId}_QueueStatManager")
    repo += (queueId -> actorRef)
    actorRef
  }
}