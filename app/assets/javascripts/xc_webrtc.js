xc_webrtc = function(){
    'use strict';

    var registerExp = 60;

    var debugEvent = true;
    var debugHandler = true;

    var sipStack;
    var wsIp;
    var wsPort;
    var wsProto;
    var conf;
    var registerSession;
    var username;
    var callSession;
    var onHold = false;
    var audioRemote;
    var callConfig;
    var iceServers;


    function init(name, ssl, websocketPort, remoteAudio, ip){
        remoteAudio = typeof remoteAudio !== 'undefined' ?  remoteAudio : "audio_remote";
        if (ip) { wsIp = ip; }
        username = name;
        wsProto = ssl ? 'wss://' : 'ws://';
        wsPort = websocketPort;
        var audioElem = document.getElementById(remoteAudio);
        console.log("Remote audio element: ", audioRemote);
        callConfig = {
            /*jshint camelcase: false */
            audio_remote: audioElem,
            events_listener: { events: '*', listener: sessionEventListener },
        };
        Cti.setHandler(Cti.MessageType.LINECONFIG, processLineCfg);
        Cti.getConfig('line');
    }

    function disableICE() {
        iceServers = [];
        iceServers.toString = function() {
            return "workaround bug https://code.google.com/p/sipml5/issues/detail?id=187";
        };
        console.log("Disable ICE");
    }

    function setIceUrls(urls) {
        iceServers = urls;
        console.log("Set ICE urls: ", urls);
    }

    function processLineCfg(lineCfg) {
        conf = initConf(lineCfg, username);
        startStack( conf );
    }

    function initConf(lineCfg, name) {
        wsIp = typeof wsIp !== 'undefined' ? wsIp : lineCfg.xivoIp;
        return {
            sip: {
                authorizationUser : lineCfg.name,
                realm: 'xivo',
                domain: lineCfg.xivoIp,
                password : lineCfg.password,
                wsServer : wsProto + wsIp + ':' + wsPort + '/ws',
                displayName: name,
                registerExpires: registerExp,
            }
        };
    }

    function startStack(conf) {
        var readyCallback = function(){
            createSipStack(conf);
        };
        var errorCallback = function(e){
            console.error('Failed to initialize the engine: ' + e.message);
        };
        SIPml.init(readyCallback, errorCallback);
    }

    function createSipStack(conf) {
        sipStack = new SIPml.Stack({
            /*jshint camelcase: false */
            realm: conf.sip.realm,
            impi: conf.sip.authorizationUser,
            impu: 'sip:' + conf.sip.authorizationUser + '@' + conf.sip.domain,
            password: conf.sip.password,
            display_name: conf.sip.displayName,
            websocket_proxy_url: conf.sip.wsServer,
            enable_rtcweb_breaker: false,
            events_listener: { events: '*', listener: generalEventListener },
            ice_servers: iceServers,
        });
        sipStack.start();
    }

    function register() {
        registerSession = sipStack.newSession('register', {
            expires: conf.sip.registerExpires,
            /*jshint camelcase: false */
            events_listener: { events: '*', listener: registerEventListener },
        });
        registerSession.register();
    }

    function topic(id) {
        return {
            clear : function(){
                try{
                    SHOTGUN.remove('xc_webrtc/*');
                }catch(e){}
            },
            publish : function(val){
                SHOTGUN.fire('xc_webrtc/'+id,[val]);
            },
            subscribe : function(handler){
                SHOTGUN.listen('xc_webrtc/'+id,handler);
            },
            unsubscribe : function(handler){
                SHOTGUN.remove('xc_webrtc/'+id,handler);
            }
        };
    }

    function setHandler(eventName, handler) {
        topic(eventName).subscribe(handler);
        if (debugHandler) {
            console.log("subscribing : [" + eventName + "] to " + handler);
        }
    }

    function unsetHandler(eventName, handler) {
        topic(eventName).unsubscribe(handler);
        if (debugHandler) {
            console.log("unsubscribing : [" + eventName + "] to " + handler);
        }
    }

    function generalEventListener(e) {
        if (debugEvent){ console.log("RE<<< ", e); }
        processGeneralEvent(e);
    }

    function registerEventListener(e) {
        if (debugEvent){ console.log("RE<<< ", e); }
        processRegisterEvent(e);
    }
    function sessionEventListener(e) {
        if (debugEvent){ console.log("RE<<< ", e); }
        processSessionEvent(e);
    }

    function publishEvent(id, event, data) {
        try{
            topic(id).publish(createEvent(event, data));
        }catch(error){
            if (Cti.debugMsg){
                console.error(id,event,error);
            }
        }
    }

    function createEvent(eventType, data) {
        if (typeof data === 'undefined' || data === null) {
            return {'type': eventType};
        }
        else {
            return {'type': eventType, 'data': data};
        }
    }

    function processGeneralEvent(e) {
        switch(e.type) {
            case 'started': {
                publishEvent(xc_webrtc.MessageType.GENERAL, xc_webrtc.General.STARTED);
                console.log('Started, registering');
                register();
                break;
            }
            case 'failed_to_start': {
                publishEvent(xc_webrtc.MessageType.GENERAL, xc_webrtc.General.FAILED, {'reason': e.description});
                break;
            }
            case 'i_new_call': {
                publishEvent(xc_webrtc.MessageType.INCOMING, xc_webrtc.Incoming.SETUP, getCaller(e));
                console.log('New incoming call: ', e);
                callSession = e.newSession;
                break;
            }
        }
    }

    function processRegisterEvent(e) {
        switch(e.type) {
            case 'connected': {
                publishEvent(xc_webrtc.MessageType.REGISTRATION, xc_webrtc.Registration.REGISTERED);
                console.log("Registered");
                break;
            }
            case 'terminated': {
                publishEvent(xc_webrtc.MessageType.REGISTRATION, xc_webrtc.Registration.UNREGISTERED, {'reason': e.description});
                register();
                console.log("Unregistered, retrying");
                break;
            }
        }
    }

    function processSessionEvent(e) {
        switch(e.type) {
            case 'connected': {
                if (isOutgoing(e)) {
                    publishEvent(xc_webrtc.MessageType.OUTGOING, xc_webrtc.Outgoing.CONNECTED, getCallee(e));
                } else {
                    publishEvent(xc_webrtc.MessageType.INCOMING, xc_webrtc.Outgoing.CONNECTED, getCaller(e));
                }
                console.log('Connected');
                break;
            }
            case 'i_ao_request':
            {
                var iSipResponseCode = e.getSipResponseCode();
                if (iSipResponseCode === 180 || iSipResponseCode === 183) {
                    if (isOutgoing(e)) {
                        publishEvent(xc_webrtc.MessageType.OUTGOING, xc_webrtc.Outgoing.RINGING, getCallee(e));
                    } else {
                        publishEvent(xc_webrtc.MessageType.INCOMING, xc_webrtc.Outgoing.RINGING, getCaller(e));
                    }
                    console.log('Ringing');
                }
                break;
            }
            case 'm_local_hold_ok': {
                onHold = true;
                if (isOutgoing(e)) {
                    publishEvent(xc_webrtc.MessageType.OUTGOING, xc_webrtc.Outgoing.HOLD);
                } else {
                    publishEvent(xc_webrtc.MessageType.INCOMING, xc_webrtc.Outgoing.HOLD);
                }
                console.log('Holded');
                break;
            }
            case 'm_local_resume_ok': {
                onHold = false;
                if (isOutgoing(e)) {
                    publishEvent(xc_webrtc.MessageType.OUTGOING, xc_webrtc.Outgoing.RESUME);
                } else {
                    publishEvent(xc_webrtc.MessageType.INCOMING, xc_webrtc.Outgoing.RESUME);
                }
                console.log('Resumed');
                break;
            }
            case 'terminating':
            case 'terminated': {
                if (isOutgoing(e)) {
                    publishEvent(xc_webrtc.MessageType.OUTGOING, xc_webrtc.Outgoing.TERMINATED, {"reason": e.description});
                } else {
                    publishEvent(xc_webrtc.MessageType.INCOMING, xc_webrtc.Outgoing.TERMINATED, {"reason": e.description});
                }
                console.log('terminated');
                break;
            }
        }
    }

    function isOutgoing(e) {
        return e.o_event.o_session.o_uri_from.s_display_name === conf.sip.displayName;
    }

    function getCallee(e) {
        return {'callee': e.o_event.o_session.o_uri_to.s_user_name};
    }

    function getCaller(e) {
        return {'caller': e.o_event.o_session.o_uri_from.s_user_name};
    }

    function getParticipantsData(e) {
        return {'caller': e.o_session.o_uri_from.s_display_name,
                'callee': e.o_session.o_uri_to.s_user_name};
    }

    function dial(destination) {
        console.log('Dial: ', destination);
        callSession = sipStack.newSession('call-audio', callConfig);
        if (callSession.call(destination) !== 0) {
            publishEvent(xc_webrtc.MessageType.OUTGOING, xc_webrtc.Outgoing.FAILED);
            console.log('call Failed');
            return;
        }
        else {
            publishEvent(xc_webrtc.MessageType.OUTGOING, xc_webrtc.Outgoing.ESTABLISHING);
            console.log('call Establishing');
        }
    }

    function answer() {
        console.log('Answer');
        callSession.accept(callConfig);
        publishEvent(xc_webrtc.MessageType.INCOMING, xc_webrtc.Incoming.CONNECTED);
    }

    function dtmf(key) {
        console.log('Sending DTMF: ', key);
        return callSession.dtmf(key);
    }

    function hold() {
        if (onHold) {
            deactivateHold();
        } else {
            activateHold();
        }
    }

    function activateHold() {
        console.log('Hold');
        return callSession.hold();
    }

    function deactivateHold() {
        console.log('Resume');
        return callSession.resume();
    }

    return{
        init: init,
        dial: dial,
        answer: answer,
        dtmf: dtmf,
        hold: hold,
        setHandler: setHandler,
        disableICE: disableICE,
        setIceUrls: setIceUrls,
    };
}();

xc_webrtc.MessageType = {
    GENERAL: "general",
    OUTGOING: "outgoing",
    INCOMING: "incoming",
    REGISTRATION: "registration",
};

xc_webrtc.General = {
    STARTED: "Started",
    FAILED: "Failed",
};

xc_webrtc.Incoming = {
    SETUP: "Setup",
    RINGING: "Ringing",
    CONNECTED: "Connected",
    TERMINATED: "Terminated",
    HOLD: "Hold",
    RESUME: "Resume",
};

xc_webrtc.Outgoing = {
    ESTABLISHING: "Establishing",
    RINGING: "Ringing",
    CONNECTED: "Connected",
    TERMINATED: "Terminated",
    FAILED: "Failed",
    HOLD: "Hold",
    RESUME: "Resume",
};

xc_webrtc.Registration = {
    REGISTERED: "Registered",
    UNREGISTERED: "Unregistered",
};
