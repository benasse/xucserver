var DirectoryDisplay = {

    init : function(tableName) {
        this.tableName = tableName;
    },

    prepareTable : function() {
        $(this.tableName).empty();
        $(this.tableName).append('<table class="table"/>' );
    },

    displayHeaders : function(headers) {
        var tableElement = this.tableName + " table";
        $(tableElement).append( '<tr>');
        $(tableElement+ ' tr').append( '<th></th>' );
        for(var i=0 ; i < headers.length; i++){
           $(tableElement+ ' tr').append( '<th>' + headers[i] + '</th>' );
        }
        $(tableElement).append( '</tr>');
        this.length = headers.length;
    },
    displayEntries : function(entries) {
        var tableElement = this.tableName + " table";
        for(var i=0; i < entries.length;i++){
            var entry = '<tr><td bgcolor="'+Cti.PhoneStatusColors[entries[i].status]+'"><span class="glyphicon glyphicon-earphone glyph-white"></span></td>';
            for ( var j = 0; j < entries[i].entry.length; j++) {
                var currentField = entries[i].entry[j];
                entry = entry + '<td >' +  this.formatField(currentField) + '</td>';
            }
            for(; j < this.length; j++) {
                entry += '<td/>';
            }
            entry = entry + '</tr>';
            $(tableElement).append(entry);
        }
    },

    formatField : function(field) {
        if ($.isNumeric(field)) {
            return '<a href="#">'+field+'</a>';
        }
        else {
            return field;
        }
    }
};
