package controllers

import controllers.helpers.PrettyController
import play.api.mvc.{Action, Controller}

object XucDefault extends Controller {

  def index = Action {
    Ok(PrettyController.prettify(views.html.default.index("Xuc")))
  }
}