package controllers.xuc

import controllers.xuc.WsApi._
import play.api.Play.current
import play.api.libs.json.Json
import play.api.libs.ws.WS
import play.api.mvc.{AnyContentAsJson, Request, RequestHeader, Result}
import xivo.xuc.XucConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ForwardRequester {

  val WsTimeout = 2000
  val Xucapi = "xuc/api/1.0"
  val headers = List(("Content-Type", "application/json"), ("Accept", "application/json"), ("Peer", "xuc"))


  def isFromXuc(implicit request: RequestHeader) = request.headers.get("Peer") == Some("xuc")

  def requestBodyAsJson[A](implicit request:Request[A])  =  request.body match {
    case AnyContentAsJson(json) => json
    case _ => Json.obj()
  }

  def forwardRequest[A](domain:String, username: String, action:String)(implicit request: Request[A]):Future[Result] = {
    log.debug(s"forwarding request $request ${request.headers} ${request.body.getClass} ${XucConfig.xucPeers}")

    def buildWs(peer: String) = {
      WS.url(s"http://$peer/$Xucapi/$action/$domain/$username/")
        .withHeaders(headers:_*)
        .withRequestTimeout(WsTimeout)
        .post(requestBodyAsJson)
    }

    if(!isFromXuc) {
      log.debug(s"request $request ${request.headers}")
      val forwardWSs = XucConfig.xucPeers.map(peer => buildWs(peer))
      Future.sequence(forwardWSs).map {
        success => log.info(s"request forwarded on user not found ")
        Accepted(Json.obj("message"->"request forwarded on user not found","peers" -> XucConfig.xucPeers))
      }.recover{
        case ex => log.error(s"unable to forward request $ex")
          BadRequest(s"unable to forward request $ex")
      }
    }
    else {
      log.debug("request already coming from another xuc")
      Future(NotFound("From xuc user not found"))
    }
  }

}
