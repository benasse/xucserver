package models

import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.libs.json._

sealed trait XivoDird

/*
 * compatible with XiVO auth swagger specification v0.1
 */

case class DirSearchResult(columnHeaders: List[String], columnTypes: List[String], results: List[ResultItem])
object DirSearchResult {

  def parse(json: JsValue): DirSearchResult = {
    val columnHeaders = (json \ "column_headers").validate[List[String]].get
    val columnTypes = (json \ "column_types").validate[List[String]].get
    val results = ResultItem.read(columnTypes, (json \ "results"))
    DirSearchResult(columnHeaders, columnTypes, results)
  }

}

case class ResultItem(item: DefaultDisplayViewItem, relations: Relations, source: String)
object ResultItem {
  def read(columnTypes: List[String], json: JsValue): List[ResultItem] = {
    val list = json.validate[List[JsValue]].get
    val result = list.map(item => {
      val columnValues = DefaultDisplayViewItem.read(columnTypes, (item \ "column_values"))
      val relations = (item \ "relations").validate[Relations].get
      val source = (item \ "source").validate[String].get
      ResultItem(columnValues, relations, source)
    })
    result
  }
}

case class DefaultDisplayViewItem(name: String, number: Option[String], mobile: Option[String],
                                  externalNumber: Option[String], email: Option[String], favorite: Boolean)
object DefaultDisplayViewItem {
  def read(columnTypes: List[String], json: JsValue): DefaultDisplayViewItem = {
    val names = List(
      json(columnTypes.indexOf("name")).validate[Option[String]].get,
      json(columnTypes.indexOf("name", columnTypes.indexOf("name") +1)).validate[Option[String]].get)
    val name = names.filter(i => i.nonEmpty).map(i => i.get).mkString(" ")
    val number = json(columnTypes.indexOf("number")).validate[Option[String]].get
    val mobile = if (columnTypes.contains("mobile")) {
      json(columnTypes.indexOf("mobile")).validate[Option[String]].get
    }
    else {
      json(columnTypes.indexOf("callable")).validate[Option[String]].get
    }
    val externalNumber = json(columnTypes.indexOf("number", columnTypes.indexOf("number") +1)).validate[Option[String]].get
    val email = json(columnTypes.indexOf("other")).validate[Option[String]].get
    val favorites = json(columnTypes.indexOf("favorite")).validate[Boolean].get
    DefaultDisplayViewItem(name, number, mobile, externalNumber, email, favorites)
  }

}

case class Relations(agentId: Option[Long], endpointId: Option[Long], userId: Option[Long], xivoId: Option[String],
                      sourceEntryId: Option[String])
object Relations {
  implicit val relationReads: Reads[Relations] = (
      (JsPath \ "agent_id").readNullable[Long] and
      (JsPath \ "endpoint_id").readNullable[Long] and
      (JsPath \ "user_id").readNullable[Long] and
      (JsPath \ "xivo_id").readNullable[String] and
      (JsPath \ "source_entry_id").readNullable[String]
    )(Relations.apply _)

}
