package models

import org.xivo.cti.model.PhoneHintStatus
import play.api.libs.json._
import play.api.libs.functional.syntax._
import scala.collection.mutable.Buffer

case class RichEntry(status: PhoneHintStatus, fields: RichEntry.Fields, contactId: Option[String] = None,
                     source: Option[String] = None, favorite: Option[Boolean] = None)
object RichEntry {
  type Fields = Buffer[Any]
  implicit val fieldsWrites = new Writes[Fields] {
    def writes(fields: Fields): JsValue = {
      val jsvalues = fields.map {
        case s: String => JsString(s)
        case b: Boolean => JsBoolean(b)
        case _ => JsNull
      }
      JsArray(jsvalues)
    }
  }

  implicit val richEntryWrites = (
    (__ \ "status").write[Int].contramap { (a: PhoneHintStatus) => a.getHintStatus() } and
    (__ \ "entry").write[Fields] and
    (__ \ "contact_id").writeNullable[String] and
    (__ \ "source").writeNullable[String] and
    (__ \ "favorite").writeNullable[Boolean]
    )(unlift(RichEntry.unapply))

}


class RichDirectoryEntries(val headers: List[String]) {
  var entries : List[RichEntry] = List()
  def getEntries(): List[RichEntry] = entries
  def add(entry: RichEntry) = {
    entries = entries :+ entry
  }
}

object RichDirectoryEntries {
  implicit val rdrWrites = new Writes[RichDirectoryEntries] {
    def writes(rdr: RichDirectoryEntries): JsValue =
      JsObject(
          Seq("headers" -> Json.toJson(rdr.headers),
              "entries" -> Json.toJson(rdr.entries))
          )
  }

  def toJson(rdResult : RichDirectoryEntries) = Json.toJson(rdResult)
  def removeFavorite(headers: List[String]): List[String] = {
    headers.filter(s => s != "Favorite")
  }

}

class RichDirectoryResult(headers: List[String]) extends RichDirectoryEntries(headers)
class RichFavorites(headers: List[String]) extends RichDirectoryEntries(headers)
